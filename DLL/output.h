#pragma once

#include <Windows.h>

template <typename... Args>
void print_error(const char* fmt, Args... args) {
    size_t strsize = snprintf(nullptr, 0, fmt, args...) + 1;
    
    char* outstr = (char*)malloc(strsize);
    snprintf(outstr, strsize, fmt, args...);
    OutputDebugStringA(outstr);
    free(outstr);
}

template <typename... Args>
void print_warning(const char* fmt, Args... args) {
    size_t strsize = snprintf(nullptr, 0, fmt, args...) + 1;

    char* outstr = (char*)malloc(strsize);
    snprintf(outstr, strsize, fmt, args...);
    OutputDebugStringA(outstr);
    free(outstr);
}

template <typename... Args>
void print_info(const char* fmt, Args... args) {
    size_t strsize = snprintf(nullptr, 0, fmt, args...) + 1;

    char* outstr = (char*)malloc(strsize);
    snprintf(outstr, strsize, fmt, args...);
    OutputDebugStringA(outstr);
    free(outstr);
}