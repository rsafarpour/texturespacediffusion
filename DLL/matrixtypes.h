#pragma once

#include <cmath>
#include "vectortypes.h"

struct float3x3 {
    float _00; float _01; float _02;
    float _10; float _11; float _12;
    float _20; float _21; float _22;
};

struct float4x4 {
    float _00; float _01; float _02; float _03;
    float _10; float _11; float _12; float _13;
    float _20; float _21; float _22; float _23;
    float _30; float _31; float _32; float _33;
};


inline float3x3 construct(const float3* a, const float3* b, const float3* c) {
    return {
        a->x, b->x, c->x,
        a->y, b->y, c->y,
        a->z, b->z, c->z
    };
}

inline float3x3 transpose(const float3x3* m) {
    return {
        m->_00, m->_01, m->_02,
        m->_10, m->_11, m->_12,
        m->_20, m->_21, m->_22
    };
}

inline float3 mul(const float3x3* m, const float3* v) {
    return {
        m->_00*v->x + m->_01*v->y + m->_02*v->z,
        m->_10*v->x + m->_11*v->y + m->_12*v->z,
        m->_20*v->x + m->_21*v->y + m->_22*v->z
    };
}

inline float4x4 transposed(const float4x4* m) {
    return {
        m->_00, m->_10, m->_20, m->_30,
        m->_01, m->_11, m->_21, m->_31,
        m->_02, m->_12, m->_22, m->_32,
        m->_03, m->_13, m->_23, m->_33
    };
}

inline float4 mul(const float4x4* m, const float4* v) {
    return {
        m->_00*v->x + m->_01*v->y + m->_02*v->z + m->_03*v->w,
        m->_10*v->x + m->_11*v->y + m->_12*v->z + m->_13*v->w,
        m->_20*v->x + m->_21*v->y + m->_22*v->z + m->_23*v->w,
        m->_30*v->x + m->_31*v->y + m->_32*v->z + m->_33*v->w
    };
}

inline float4x4 mul(const float4x4* m0, const float4x4* m1) {
    float4 m0row0 = { m0->_00, m0->_01, m0->_02, m0->_03 };
    float4 m0row1 = { m0->_10, m0->_11, m0->_12, m0->_13 };
    float4 m0row2 = { m0->_20, m0->_21, m0->_22, m0->_23 };
    float4 m0row3 = { m0->_30, m0->_31, m0->_32, m0->_33 };

    float4 m1col0 = { m1->_00, m1->_10, m1->_20, m1->_30 };
    float4 m1col1 = { m1->_01, m1->_11, m1->_21, m1->_31 };
    float4 m1col2 = { m1->_02, m1->_12, m1->_22, m1->_32 };
    float4 m1col3 = { m1->_03, m1->_13, m1->_23, m1->_33 };

    return {
        dot(m0row0, m1col0), dot(m0row0, m1col1), dot(m0row0, m1col2), dot(m0row0, m1col3),
        dot(m0row1, m1col0), dot(m0row1, m1col1), dot(m0row1, m1col2), dot(m0row1, m1col3),
        dot(m0row2, m1col0), dot(m0row2, m1col1), dot(m0row2, m1col2), dot(m0row2, m1col3),
        dot(m0row3, m1col0), dot(m0row3, m1col1), dot(m0row3, m1col2), dot(m0row3, m1col3)
    };
}

inline float det(const float3x3& m) {
    float f0 =  m._00;
    float f1 = -m._01;
    float f2 =  m._02;

    float minor0 = m._11*m._22 - m._12*m._21;
    float minor1 = m._10*m._22 - m._20*m._12;
    float minor2 = m._10*m._21 - m._20*m._11;

    return f0 * minor0 + f1 * minor1 + f2 * minor2;
}

inline float det(const float4x4& m) {
    float f0 =  m._00;
    float f1 = -m._01;
    float f2 =  m._02;
    float f3 = -m._03;

    float minor0 = det(float3x3{m._11, m._12, m._13, m._21, m._22, m._23, m._31, m._32, m._33});
    float minor1 = det(float3x3{m._10, m._12, m._13, m._20, m._22, m._23, m._30, m._32, m._33});
    float minor2 = det(float3x3{m._10, m._11, m._13, m._20, m._21, m._23, m._30, m._31, m._33});
    float minor3 = det(float3x3{m._10, m._11, m._12, m._20, m._21, m._22, m._30, m._31, m._32});

    return f0*minor0 + f1*minor1 + f2*minor2 + f3*minor3;
}

inline void transpose(float4x4& m) {
    std::swap(m._01, m._10);
    std::swap(m._02, m._20);
    std::swap(m._03, m._30);
    std::swap(m._12, m._21);
    std::swap(m._13, m._31);
    std::swap(m._23, m._32);
}

inline void invert_transpose(float4x4& m) {
    float d = det(m);

    float minor00 = det(float3x3{m._11, m._12, m._13, m._21, m._22, m._23, m._31, m._32, m._33});
    float minor01 = det(float3x3{m._10, m._12, m._13, m._20, m._22, m._23, m._30, m._32, m._33});
    float minor02 = det(float3x3{m._10, m._11, m._13, m._20, m._21, m._23, m._30, m._31, m._33});
    float minor03 = det(float3x3{m._10, m._11, m._12, m._20, m._21, m._22, m._30, m._31, m._32});

    float minor10 = det(float3x3{m._01, m._02, m._03, m._21, m._22, m._23, m._31, m._32, m._33});
    float minor11 = det(float3x3{m._00, m._02, m._03, m._20, m._22, m._23, m._30, m._32, m._33});
    float minor12 = det(float3x3{m._00, m._01, m._03, m._20, m._21, m._23, m._30, m._31, m._33});
    float minor13 = det(float3x3{m._00, m._01, m._02, m._20, m._21, m._22, m._30, m._31, m._32});

    float minor20 = det(float3x3{m._01, m._02, m._03, m._11, m._12, m._13, m._31, m._32, m._33});
    float minor21 = det(float3x3{m._00, m._02, m._03, m._10, m._12, m._13, m._30, m._32, m._33});
    float minor22 = det(float3x3{m._00, m._01, m._03, m._10, m._11, m._13, m._30, m._31, m._33});
    float minor23 = det(float3x3{m._00, m._01, m._02, m._10, m._11, m._12, m._30, m._31, m._32});

    float minor30 = det(float3x3{m._01, m._02, m._03, m._11, m._12, m._13, m._21, m._22, m._23});
    float minor31 = det(float3x3{m._00, m._02, m._03, m._10, m._12, m._13, m._20, m._22, m._23});
    float minor32 = det(float3x3{m._00, m._01, m._03, m._10, m._11, m._13, m._20, m._21, m._23});
    float minor33 = det(float3x3{m._00, m._01, m._02, m._10, m._11, m._12, m._20, m._21, m._22});

    m._00 =  minor00 / d;
    m._01 = -minor01 / d;
    m._02 =  minor02 / d;
    m._03 = -minor03 / d;

    m._10 = -minor10 / d;
    m._11 =  minor11 / d;
    m._12 = -minor12 / d;
    m._13 =  minor13 / d;

    m._20 =  minor20 / d;
    m._21 = -minor21 / d;
    m._22 =  minor22 / d;
    m._23 = -minor23 / d;

    m._30 = -minor30 / d;
    m._31 =  minor31 / d;
    m._32 = -minor32 / d;
    m._33 =  minor33 / d;
}

inline void invert(float4x4& m) {
    invert_transpose(m);
    transpose(m);
}

using rad_t = float;
using deg_t = float;
inline float4x4 ProjectionMatrixRad(rad_t fov_y, float aspect_ratio, float n, float f) {
    float w = std::tan(fov_y / 2.0f);
    float h = std::tan(fov_y / 2.0f);
    float a = aspect_ratio;
    return {
        1.0f / w, 0.0f,        0.0f,               0.0f,
        0.0f,     a * 1.0f/h,  0.0f,               0.0f,
        0.0f,     0.0f,       -(n + f) / (f - n), -(2.0f * n*f) / (f - n),
        0.0f,     0.0f,       -1.0f,               0.0f
    };
}

inline float4x4 ViewMatrix(float3 position, float3 look_at, float3 up) {
    float4x4 translation = {
        1.0f, 0.0f, 0.0f, -position.x,
        0.0f, 1.0f, 0.0f, -position.y,
        0.0f, 0.0f, 1.0f, -position.z,
        0.0f, 0.0f, 0.0f,        1.0f
    };

    // Create view basis transformation
    // => equivalent to rotation to view
    float3 z_axis = normalised(position - look_at);
    float3 x_axis = cross(normalised(up), z_axis);
    float3 y_axis = cross(z_axis, x_axis);
    float4x4 rotation = {
        x_axis.x, x_axis.y, x_axis.z, 0.0f,
        y_axis.x, y_axis.y, y_axis.z, 0.0f,
        z_axis.x, z_axis.y, z_axis.z, 0.0f,
        0.0f,         0.0f,     0.0f, 1.0f
    };

    // Return combined transformation in column major 
    // format to accomodate DirectX conventions
    float4x4 tmp = mul(&rotation, &translation);
    return transposed(&tmp);
}

inline float4x4 ProjectionMatrixDeg(deg_t fov_y, float aspect_ratio, float n, float f) {
    return ProjectionMatrixRad(rad_t{fov_y/180.0f * 3.14159f}, aspect_ratio, n, f);
}
