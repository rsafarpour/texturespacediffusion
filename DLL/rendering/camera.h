#pragma once

#include "../matrixtypes.h"
#include "reflist.h"

using radians = float;

struct dx11renderer;

struct camera {
    ref      buffer_ref;
    float3   position;
    float3   up;
    float3   look_at;
    union { float4x4 view; char gpu_data; };
    float4x4 projection;
};

const unsigned camera_gpu_size = 2*sizeof(float4x4);

camera create_camera(dx11renderer* r);
void destroy_camera(dx11renderer* r, camera* cam);
void point_at(camera* c, float3 p, float3 up);
void place_at(camera* c, float3 p);
void place_at(camera* c, float radius, float elevation, float azimuth);
void perspective(camera* c, float ratio, radians fovy, float near, float far);