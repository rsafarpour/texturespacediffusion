#pragma once
#pragma comment(lib, "d3dcompiler.lib")

#include <string>
#include "dx11render.h"
#include "vertex_desc.h"

pipeline_state compile_from_file(vertex_desc format, const char* vs_fname, const char* ps_fname, SAMPLER_TYPE sampler);
