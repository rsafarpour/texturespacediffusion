#include "mesh_asm.h"

mesh_asm mesh_assemble(obj_mesh* mesh) {
    mesh_asm as;
    unsigned vertex_floats = 0;
    unsigned offset_normals = 0;
    unsigned offset_texcoord = 0;
    switch (mesh->layout) {
    case obj_layout::POSITIONS:
        as.vertex_layout = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        vertex_floats = 3;
        break;
    case obj_layout::POSITIONS_NORMALS:
        as.vertex_layout = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        offset_normals = 3;
        vertex_floats  = 6;
        break;
    case obj_layout::POSITIONS_TEXCOORDS:
        as.vertex_layout = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        offset_texcoord = 3;
        vertex_floats   = 5;
        break;
    case obj_layout::POSITIONS_TEXCOORDS_NORMALS:
        as.vertex_layout = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0}
        };
        offset_normals  = 3;
        offset_texcoord = 6;
        vertex_floats   = 8;
        break;
    }

    as.nvertices = mesh->positions.size();
    as.nindices = mesh->indices.size();
    as.vertex_data.resize(as.nvertices*vertex_floats);
    as.index_data.resize(as.nindices);

    unsigned stride = vertex_floats;
    for (size_t i = 0; i < as.nvertices; ++i) {
        as.vertex_data[i*stride + 0] = mesh->positions[i].x;
        as.vertex_data[i*stride + 1] = mesh->positions[i].y;
        as.vertex_data[i*stride + 2] = mesh->positions[i].z;
    }
    
    if (has_normals(mesh->layout)) {
        for (size_t i = 0; i < as.nvertices; ++i) {
            as.vertex_data[offset_normals + i*stride + 0] = mesh->normals[i].x;
            as.vertex_data[offset_normals + i*stride + 1] = mesh->normals[i].y;
            as.vertex_data[offset_normals + i*stride + 2] = mesh->normals[i].z;
        }
    }

    if (has_texcoords(mesh->layout)) {
        for (size_t i = 0; i < as.nvertices; ++i) {
            as.vertex_data[offset_texcoord + i * stride + 0] = mesh->texcoords[i].x;
            as.vertex_data[offset_texcoord + i * stride + 1] = mesh->texcoords[i].y;
        }
    }

    as.index_data = mesh->indices;
    return as;
}
