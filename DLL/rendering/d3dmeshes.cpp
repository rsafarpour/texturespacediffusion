#include "d3dmeshes.h"

std::vector<ref> submit_meshes(dx11renderer* renderer, obj_data* data) {
    std::vector<ref> mesh_refs;
    for (auto& mesh : data->meshes) {
        mesh_asm as = mesh_assemble(&mesh);
        mesh_refs.push_back(upload_mesh(renderer, &as));
    }

    return mesh_refs;
}