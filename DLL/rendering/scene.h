#pragma once

#include <vector>

#include "reflist.h"
#include "d3dmaterials.h"
#include "camera.h"
#include "point_light.h"

struct sparams_t {
    float bump_height;
};

struct scene_t {
    ref                          ambient_buffer;
    float                        profile[24];
    ref                          profile_buffer;
    sparams_t                    params;
    ref                          param_buffer;
    std::vector<ref>             mesh_refs;
    std::vector<material_params> mparams;
    camera                       cam;
    point_light                  light;
    float                        cam_elevation;
    float                        cam_azimuth;
    float                        light_elevation;
    float                        light_azimuth;
    float3                       ambient_radiance;
    float3                       pointlight_radiance;
};

void create_scene(dx11renderer* r, int2 backbuffer_dims, scene_t* scene);
void destroy_scene(dx11renderer* r, scene_t* scene);

