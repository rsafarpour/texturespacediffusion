#include "pipeline_state.h"
#include "../loaders/hlsl/hlsl_loader.h"

#include <d3dcompiler.h>

#define ENABLE_SHADERDEBUG

ID3DBlob* compile_vs(const char* name, const std::string* code) {

    UINT flags =
#ifdef ENABLE_SHADERDEBUG
        D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
        0;
#endif


    ID3DBlob* blob;
    ID3DBlob* err_msg;
    if (FAILED(D3DCompile(
        code->c_str(),
        code->size(),
        name,
        nullptr,
        D3D_COMPILE_STANDARD_FILE_INCLUDE,
        "main",
        "vs_5_0",
        flags,
        0,
        &blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*)err_msg->GetBufferPointer());
        err_msg->Release();
        return {};
    }

    return blob;
}

ID3DBlob* compile_ps(const char* name, const std::string* code) {

    UINT flags =
#ifdef ENABLE_SHADERDEBUG
        D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
        0;
#endif


    ID3DBlob* blob;
    ID3DBlob* err_msg;
    if (FAILED(D3DCompile(
        code->c_str(),
        code->size(),
        name,
        nullptr,
        D3D_COMPILE_STANDARD_FILE_INCLUDE,
        "main",
        "ps_5_0",
        flags,
        0,
        &blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*)err_msg->GetBufferPointer());
        err_msg->Release();
        return {};
    }

    return blob;
}

pipeline_state compile_from_file(vertex_desc format, const char* vs_fname, const char* ps_fname, SAMPLER_TYPE sampler_type) {
    std::string vs_code, ps_code;
    hlsl_load(vs_fname, &vs_code);
    hlsl_load(ps_fname, &ps_code);

    pipeline_state state;
    state.vs = compile_vs(vs_fname, &vs_code);
    state.ps = compile_ps(ps_fname, &ps_code);

    state.input_layout;
    state.input_layout.resize(format.data_formats.size());
    state.sampler_type = sampler_type;
    for (size_t i = 0; i < format.data_formats.size(); ++i) {
        state.input_layout[i].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        state.input_layout[i].Format            = format.data_formats[i];
        state.input_layout[i].InputSlot         = 0;
        state.input_layout[i].InputSlotClass    = D3D11_INPUT_PER_VERTEX_DATA;
        state.input_layout[i].SemanticIndex     = 0;
        state.input_layout[i].SemanticName      = format.semantics[i];
    }
    
    return state;
}
