#pragma once

#include <inttypes.h>
#include <limits>
#include <stack>
struct ref {
    uint32_t handle;
    uint32_t generation;
};

constexpr ref invalid_ref = { ~0, ~0 };

template <typename T>
class reflist {

    template <typename T>
    friend void swap(reflist<T>& lhs, reflist<T>& rhs);
public:
    class iterator;

    reflist();
    reflist(const reflist& other);
    reflist(reflist&& other);
    ~reflist();

    reflist& operator=(const reflist& other);
    reflist& operator=(reflist&& other);

    ref  make();
    void release(ref r);
    T*   get(ref r);

    iterator begin();
    iterator end();

private:
    struct link {
        uint32_t position;
        uint32_t generation;
        uint32_t backref;
    };

    T*                   objects;
    link*                redir;
    unsigned             allocated;
    unsigned             populated;
    std::stack<unsigned> free;
};

//-------------------------------------------------------------------------------------------------

template <typename T>
class reflist<T>::iterator {
    friend class reflist<T>::iterator;
public:
    iterator(reflist<T>* list, uint32_t i);
    iterator(const reflist<T>::iterator& other);

    bool operator==(const reflist<T>::iterator& rhs);
    bool operator!=(const reflist<T>::iterator& rhs);
    reflist<T>::iterator& operator=(const reflist<T>::iterator& rhs);
    reflist<T>::iterator& operator++();
    reflist<T>::iterator& operator++(int);
    reflist<T>::iterator& operator--();
    reflist<T>::iterator& operator--(int);
    reflist<T>::iterator& operator+=(int rhs);
    reflist<T>::iterator& operator-=(int rhs);
    T* operator->();
    T& operator*();

private:
    reflist<T>* list_;
    uint32_t    pos_;
};

//-------------------------------------------------------------------------------------------------

template <typename T>
void swap(reflist<T>& lhs, reflist<T>& rhs) {
    std::swap(lhs.allocated, rhs.allocated);
    std::swap(lhs.objects,   rhs.objects);
    std::swap(lhs.populated, rhs.populated);
    std::swap(lhs.redir,     rhs.redir);
}

template <typename T>
reflist<T>::reflist() {
    this->objects = (T*)malloc(sizeof(T));
    this->redir = (link*)malloc(sizeof(link));
    this->allocated = 1;
    this->populated = 0;
}

template <typename T>
reflist<T>::reflist(const reflist& other) {
    uint32_t objbytes = sizeof(T)*other.allocated;
    uint32_t linkbytes = sizeof(reflist<T>::link)*other.allocated;
    this->objects = (T*)malloc(objbytes);
    this->redir = (reflist<T>::link*)malloc(linkbytes);
    this->allocated = other.allocated;
    this->populated = other.populated;
    memcpy(this->objects, other.objects, objbytes);
    memcpy(this->redir, other.redir, linkbytes);
}

template <typename T>
reflist<T>::reflist(reflist&& other)
  : objects(nullptr),
    redir(nullptr),
    allocated(0),
    populated(0) {
    std::swap(this->objects,   other.objects);
    std::swap(this->redir,     other.redir);
    std::swap(this->allocated, other.allocated);
    std::swap(this->populated, other.populated);
}

template <typename T>
typename reflist<T>& reflist<T>::operator=(const reflist& other) {
    reflist tmp(other);
    swap(*this, tmp);
    return *this;
}

template <typename T>
typename reflist<T>& reflist<T>::operator=(reflist&& other) {
    reflist tmp(std::move(other));
    swap(*this, tmp);
    return *this;
}

template <typename T>
reflist<T>::~reflist() {
    if (this->objects != nullptr) { ::free(this->objects); }
    if (this->redir   != nullptr) { ::free(this->redir); }
}

template <typename T>
ref reflist<T>::make() {
    
    ref r;
    if (populated < allocated) {
        if (!free.empty()) {
            r.handle = free.top();
            free.pop();
            r.generation = ++this->redir[r.handle].generation;
            this->redir[populated].backref = r.handle;
            this->redir[r.handle].position = populated;

            populated++;
            return r;
        }
    }
    else {
        if (this->allocated < invalid_ref.handle) {
            this->objects = (T*)realloc(this->objects, 2*this->allocated*sizeof(T));
            this->redir = (link*)realloc(this->redir, 2*this->allocated*sizeof(link));
            if (this->objects != nullptr &&
                this->redir   != nullptr) {
                this->allocated <<= 1;
            }
        }
        else {
            return invalid_ref;
        }
    }
   
    r.handle = this->redir[populated].position = populated;
    r.handle = this->redir[populated].backref = populated;
    r.generation = this->redir[r.handle].generation = 0;
    populated++;
    return r;
}

template <typename T>
void reflist<T>::release(ref r) {
    if (r.handle < this->allocated && this->redir[r.handle].generation == r.generation) {

        unsigned index   = this->redir[r.handle].position;
        uint32_t backref = this->redir[populated - 1].backref;
        
        std::swap(this->objects[index], this->objects[populated - 1]);


        this->redir[backref].position = index;
        this->redir[index].backref = backref;

        populated--;

        this->free.push(index);
    }
}

template <typename T>
T* reflist<T>::get(ref r) {
    if (r.handle < this->allocated && this->redir[r.handle].generation == r.generation) {
        unsigned index = this->redir[r.handle].position;
        return &this->objects[index];
    }

    return nullptr;
}

//-------------------------------------------------------------------------------------------------

template <typename T>
typename reflist<T>::iterator reflist<T>::begin() {
    return reflist<T>::iterator(this, 0);
}

template <typename T>
typename reflist<T>::iterator reflist<T>::end() {
    return reflist<T>::iterator(this, this->populated);
}

template <typename T>
reflist<T>::iterator::iterator(reflist<T>* list, uint32_t i)
  : list_(list),
    pos_(i)
{}

template <typename T>
reflist<T>::iterator::iterator(const reflist<T>::iterator& other)
  : list_(other.list_),
    pos_(other.pos_)
{}

template <typename T>
bool reflist<T>::iterator::operator==(const reflist<T>::iterator& rhs) {
    return (this->pos_ == rhs.pos_);
}

template <typename T>
bool reflist<T>::iterator::operator!=(const reflist<T>::iterator& rhs) {
    return (this->pos_ != rhs.pos_);
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator=(const reflist<T>::iterator& rhs) {
    this->pos = rhs.pos;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator++() {
    this->pos_++;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator++(int) {
    this->pos_++;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator--() {
    this->pos_--;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator--(int) {
    this->pos_--;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator+=(int rhs) {
    this->pos_ += rhs;
    return *this;
}

template <typename T>
typename reflist<T>::iterator& reflist<T>::iterator::operator-=(int rhs) {
    this->pos_ -= rhs;
    return *this;
}

template <typename T>
T* reflist<T>::iterator::operator->() {
    return &this->list_[this->pos_];
}

template <typename T>
T& reflist<T>::iterator::operator*() {
    return this->list_->objects[this->pos_];
}
