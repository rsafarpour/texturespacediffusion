#pragma once

#include "dx11profile.h"
#include "pipeline_state.h"
#include "passes/ss_prepass.h"
#include "passes/ss_finalpass.h"
#include "passes/ss_maskpass.h"
#include "passes/ss_shadowpass.h"
#include "vertex_desc.h"
#include "scene.h"

struct ssparams_t {
    float bias;
    float scale;
    float transmscale;
};

struct sspace_method {
    ssparams_t     params = { 11.0f, 800.0f, 1.0f };
    ref            params_buffer;
    ref            invproj_buffer;
    pipeline_state prepass_pipeline;
    pipeline_state maskpass_pipeline;
    pipeline_state finalpass_pipeline;
    pipeline_state shadowpass_pipeline;
    ref            pre_ppl;
    ref            mask_ppl;
    ref            final_ppl;
    ref            shadow_ppl;
    ss_prepass     pp;
    ss_maskpass    mp;
    ss_finalpass   fp;
    ss_shadowpass  sp;
    int2           backbuffer_dims;
    gpu_profiler   p;
    gpu_capture    begin;
    gpu_capture    end;
    ms_t           avg_frametime;
    unsigned long  n_samples;
};

void create_sspacemethod(dx11renderer* r, int2 backbuffer_dims, scene_t* scene, const vertex_desc* desc, sspace_method* method);
void destroy_sspacemethod(dx11renderer* r, sspace_method* method);
void render_sspacemethod(dx11renderer* r, sspace_method* method);
