#pragma once

#include "dx11profile.h"
#include "pipeline_state.h"
#include "passes/ts_blurpass.h"
#include "passes/ts_diffusepass.h"
#include "passes/ts_finalpass.h"
#include "passes/ts_shadowpass.h"
#include "passes/ts_stretchpass.h"
#include "vertex_desc.h"
#include "scene.h"

struct scales_t {
    float blur_scale;        // Scales TS blurs
    float attenuation_scale; // Scales attenuation of translucency
};

struct tspace_method {
    scales_t       scales = { 1.0, 1.0 };
    ref            scales_ref;
    pipeline_state shadow_pipeline;
    pipeline_state stretch_pipeline;
    pipeline_state diffusepass_pipeline;
    pipeline_state blurH_pipeline;
    pipeline_state blurV_pipeline;
    pipeline_state finalpass_pipeline;
    ref            shadow_ppl;
    ref            stretch_ppl;
    ref            diffuse_ppl;
    ref            blurH_ppl;
    ref            blurV_ppl;
    ref            finalpass_ppl;
    ref            rt_tmp1;
    ref            rt_tmp2;
    ts_shadowpass  shp;
    ts_stretchpass sp;
    ts_diffusepass dp;
    ts_blurpass    bp1;
    ts_blurpass    bp2;
    ts_blurpass    bp3;
    ts_blurpass    bp4;
    ts_blurpass    bp5;
    ts_finalpass   fp;
    int2           backbuffer_dims;
    gpu_profiler   p;
    gpu_capture    begin;
    gpu_capture    end;
    ms_t           avg_frametime;
    unsigned long  n_samples;
};

void create_tspacemethod(dx11renderer* r, int2 backbuffer_dims, scene_t* scene, const vertex_desc* desc, tspace_method* method);
void destroy_tspacemethod(dx11renderer* r, tspace_method* method);
void render_tspacemethod(dx11renderer* r, tspace_method* method);
