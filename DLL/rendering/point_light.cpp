#include "point_light.h"
#include "dx11render.h"

point_light create_pointlight(dx11renderer* r) {

    point_light l = {};
    l.buffer_ref = create_uniformbuffer(r, point_light_gpu_size);

    return l;
}

void destroy_pointlight(dx11renderer* r, point_light* l) {
    destroy_uniformbuffer(r, l->buffer_ref);
}

void set_radiance(point_light* l, float3 L_o) {
    l->radiance.x = L_o.x;
    l->radiance.y = L_o.y;
    l->radiance.z = L_o.z;
}

void set_position(point_light* l, float3 p) {
    l->position.x = p.x;
    l->position.y = p.y;
    l->position.z = p.z;

    l->view = ViewMatrix(
        l->position,
        { 0.0f, 0.0f, 0.0f },
        { 0.0f, 1.0f, 0.0f });
}

void set_position(point_light* l, float radius, float elevation, float azimuth) {

    float3 p = {
        radius * cos(azimuth)*cos(elevation),
        radius * sin(elevation),
        radius * sin(azimuth)*cos(elevation)
    };

    set_position(l, p);
}

void set_perspective(point_light* l, float fovy, float aspect_ratio, float near_plane, float far_plane) {
    float4x4 projection_transposed = ProjectionMatrixRad(fovy, aspect_ratio, near_plane, far_plane);
    l->projection = transposed(&projection_transposed);
}