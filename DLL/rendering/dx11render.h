#pragma once


#include <d3d11.h>
#include <vector>
#include "../vectortypes.h"

#include "../matrixtypes.h"
#include "../stb_image.h"
#include "mesh_asm.h"
#include "reflist.h"

struct camera;
struct point_light;

enum class SAMPLER_TYPE {
    NONE,
    LINEAR
};

struct dx11uniformbuffer {
    ID3D11Buffer* buffer;
    unsigned      bytes;
};

struct pipeline_state {
    ID3DBlob*                             vs;
    ID3DBlob*                             ps;
    std::vector<D3D11_INPUT_ELEMENT_DESC> input_layout;
    SAMPLER_TYPE                          sampler_type;
};

struct ppl_obj {
    ID3D11VertexShader* vs;
    ID3D11PixelShader*  ps;
    ID3D11InputLayout*  layout;
    ID3D11SamplerState* sampler_state;
};

struct dx11mesh {
    ID3D11Buffer* vertex_buffer;
    ID3D11Buffer* index_buffer;
    unsigned      num_indices;
    unsigned      vertex_stride;
};

struct dx11texture2d {
    ID3D11Texture2D*          resource;
    ID3D11ShaderResourceView* view;
};

struct dx11target {
    ID3D11Texture2D*          resource;
    ID3D11RenderTargetView*   rtv;
    ID3D11ShaderResourceView* srv;
};

struct dx11depthstencil {
    ID3D11Texture2D*          resource;
    ID3D11DepthStencilView*   dsv;
    ID3D11ShaderResourceView* srv;
};

struct dx11uav {
    ID3D11Texture2D*           resource;
    ID3D11UnorderedAccessView* uav;
    ID3D11ShaderResourceView*  srv;
};

struct dx11renderer {
    int2                       backbuffer_dims;
    IDXGIFactory*              dxgi;
    IDXGIAdapter*              adapt;
    IDXGISwapChain*            sc;
    ID3D11Device*              dev;
    ID3D11DeviceContext*       ctx;
    D3D_FEATURE_LEVEL          feature_level;
    ID3D11Texture2D*           ds_texture;
    ID3D11DepthStencilState*   dsstate_depthon;
    ID3D11DepthStencilState*   dsstate_depthoff;
    ID3D11DepthStencilState*   dsstate_genmask;
    ID3D11DepthStencilState*   dsstate_drawmask;
    ID3D11DepthStencilView*    dsv;
    ID3D11SamplerState*        bilinear_sampler;
    ID3D11SamplerState*        pcf_sampler;

    ID3D11BlendState*          blend_state;
    ID3D11RasterizerState*     rasterizer_state;
    ID3D11RenderTargetView*    rtv;

    reflist<dx11texture2d>     textures2d;
    reflist<dx11mesh>          meshes;
    reflist<ppl_obj>           pipelines;
    reflist<dx11target>        targets;
    reflist<dx11uav>           uavs;
    reflist<dx11depthstencil>  depthstencils;
    reflist<dx11uniformbuffer> uniforms;
};

dx11renderer create_renderer(HWND output_window, int width, int height);
ref          create_target(dx11renderer* renderer, int2 dims, DXGI_FORMAT format);
ref          create_depth(dx11renderer* renderer, int2 dims);
ref          create_uniformbuffer(dx11renderer* renderer, unsigned bytes);

void         clear_backbuffer(dx11renderer* renderer);
void         clear_target(dx11renderer* renderer, ref target_ref);
void         clear_target(dx11renderer* renderer, ref target_ref, float4 color);
void         clear_depth(dx11renderer* renderer, ref depth_ref);

void         depthtest_on(dx11renderer* renderer);
void         depthtest_off(dx11renderer* renderer);

void         depthtest_genmask(dx11renderer* renderer);
void         depthtest_masktest(dx11renderer* renderer);

void         destroy_renderer(dx11renderer* renderer);
void         destroy_target(dx11renderer* renderer, ref target);
void         destroy_depthstencil(dx11renderer* renderer, ref depthstencil);
void         destroy_pipelinestate(dx11renderer* renderer, ref state_ref);
void         destroy_mesh(dx11renderer* renderer, ref mesh_ref);
void         destroy_texture(dx11renderer* renderer, ref tex_ref);
void         destroy_uniformbuffer(dx11renderer* renderer, ref buffer_ref);

void         draw_mesh(dx11renderer* renderer, ref r);
void         draw_nulls(dx11renderer* renderer, unsigned vertices);

void         present_target(dx11renderer* renderer);

void         set_backbuffer(dx11renderer* renderer);
void         set_backbuffer_ds(dx11renderer* renderer);
void         set_depthstencil(dx11renderer* renderer, ref depthstencil_ref);
void         set_camera(dx11renderer* renderer, camera* cam, unsigned slot);
void         set_light(dx11renderer* renderer, point_light* l, unsigned slot);
void         set_pipeline(dx11renderer* renderer, ref program_ref);
void         set_textures(dx11renderer* renderer, unsigned start_slot, std::vector<ref> tex_refs);
void         set_texrt(dx11renderer* renderer, unsigned slot, ref target_ref);
void         set_texds(dx11renderer* renderer, unsigned slot, ref ds_ref);
void         set_target(dx11renderer* renderer, ref target_ref);
void         set_target(dx11renderer* renderer, ref target_ref, ref depthstencil_ref);
void         set_targets(dx11renderer* renderer, ref target1_ref, ref target2_ref, ref depthstencil_ref);
void         set_uniformbuffer(dx11renderer* renderer, ref buffer_ref, unsigned slot);
void         set_psuniformbuffer(dx11renderer* renderer, ref buffer_ref, unsigned slot);
void         set_viewport(dx11renderer* renderer, int2 dims);

void         update_camera(dx11renderer* renderer, camera* cam);
void         update_light(dx11renderer* renderer, point_light* light);

void         unset_target(dx11renderer* renderer);
void         unset_textures(dx11renderer* renderer, unsigned start_slot, unsigned n);
void         upload_bufferdata(dx11renderer* renderer, ref buffer_ref, void* data, unsigned bytes);
ref          upload_mesh(dx11renderer* renderer, const mesh_asm* mesh);
ref          upload_pipelinestate(dx11renderer* renderer, pipeline_state* state);
ref          upload_texalbedo(dx11renderer* renderer, stbi_uc* image, int2 dims);
ref          upload_texheight(dx11renderer* renderer, float* image, int2 dims);

