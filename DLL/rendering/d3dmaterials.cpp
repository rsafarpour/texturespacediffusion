#include "d3dmaterials.h"

std::vector<material_params> gen_materials(dx11renderer* renderer, obj_data* data) {
    std::vector<material_params> mparams;
    for (auto& material : data->materials) {
        material_params p;
        for (auto& attr : material.attrs) {
            int x, y, n_channels;
            std::string filename;
            if (attr.id == mtl_attrid::BUMP_MAP) {
                filename = "./_data/lpshead/"; // TODO: change this
                filename += attr.value.str;
                stbi_ldr_to_hdr_gamma(1.0f);
                float* img = stbi_loadf(filename.c_str(), &x, &y, &n_channels, 1);
                stbi_ldr_to_hdr_gamma(2.2f);
                p.ref_normalmap = upload_texheight(renderer, img, { x, y });
                stbi_image_free(img);
            }
            else if (attr.id == mtl_attrid::KD_MAP) {
                filename = "./_data/lpshead/"; // TODO: change this
                filename += attr.value.str;
                stbi_uc* img = stbi_load(filename.c_str(), &x, &y, &n_channels, 4);
                p.ref_albedomap = upload_texalbedo(renderer, img, { x, y });
                stbi_image_free(img);
            }
        }

        mparams.push_back(p);
    }

    return mparams;
}