#pragma once

#include <vector>
#include <d3d11.h>
#include "../loaders/obj_files/obj_output.h"

struct mesh_asm {
    std::vector<float>                    vertex_data;
    std::vector<unsigned>                 index_data;
    std::vector<D3D11_INPUT_ELEMENT_DESC> vertex_layout;
    size_t                                nvertices;
    size_t                                nindices;
};

mesh_asm mesh_assemble(obj_mesh* mesh);
