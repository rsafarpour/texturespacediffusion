#pragma once

#include "../dx11render.h"

struct ts_diffusepass {
    point_light* light;
    ref          tex_output;
    ref          mesh;
    ref          ppl;
    ref          lightmap_ds;
    ref          lightmap_uvdepth;
    ref          lightmap_normals;
};

ts_diffusepass create_diffusepass(dx11renderer* r, ref ppl, point_light* light, ref mesh, ref lightmap_ds, ref shadow_uvdepth, ref shadow_normals, int2 dims);
void         destroy_diffusepass(dx11renderer* r, const ts_diffusepass* dp);
void         exec_diffusepass(dx11renderer* r, const ts_diffusepass* dp);