#pragma once

#include "../dx11render.h"
#include "../camera.h"
#include "../scene.h"

struct sspace_method;
struct ss_finalpass {
    ref    tex_depth;
    ref    tex_diffuse;
    ref    tex_specular;
    ref    ppl;
};

ss_finalpass create_finalpass(dx11renderer* r, sspace_method* method, scene_t* scene, ref mask);
void         destroy_finalpass(dx11renderer* r, const ss_finalpass* fp);
void         exec_finalpass(dx11renderer* r, const ss_finalpass* fp);