#include "ss_prepass.h"

ss_prepass create_prepass(dx11renderer* r, int2 backbuffer_dims, ref mesh, ref ppl, camera* cam, point_light* light, ref tex_shadowmap) {
    
    ss_prepass pp = {};
    pp.light         = light;
    pp.mesh          = mesh;
    pp.ppl           = ppl;
    pp.camera        = cam;
    pp.tex_shadowmap = tex_shadowmap;

    pp.rt_diffuse     = create_target(r, backbuffer_dims, DXGI_FORMAT_R32G32B32A32_FLOAT);
    pp.rt_specular    = create_target(r, backbuffer_dims, DXGI_FORMAT_R32G32B32A32_FLOAT);
    pp.ds_mask        = create_depth(r, backbuffer_dims);

    return pp;
}

void destroy_prepass(dx11renderer* r, ss_prepass* pp) {
    destroy_depthstencil(r, pp->ds_mask);
    destroy_target(r, pp->rt_specular);
    destroy_target(r, pp->rt_diffuse);
}

void exec_prepass(dx11renderer* r, ss_prepass* pp) {

    clear_depth(r, pp->ds_mask);
    clear_target(r, pp->rt_diffuse);
    clear_target(r, pp->rt_specular);

    set_targets(r, pp->rt_diffuse, pp->rt_specular, pp->ds_mask);
    set_texds(r, 5, pp->tex_shadowmap);
    set_pipeline(r, pp->ppl);

    draw_mesh(r, pp->mesh);

    unset_target(r);
    unset_textures(r, 5, 1);
}