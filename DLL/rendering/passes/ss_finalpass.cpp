#include "ss_finalpass.h"
#include "../sspace_method.h"

ss_finalpass create_finalpass(dx11renderer* r, sspace_method* method, scene_t* scene, ref depth) {
    ss_finalpass fp;
    fp.tex_depth    = depth;
    fp.tex_diffuse  = method->pp.rt_diffuse;
    fp.tex_specular = method->pp.rt_specular;
    fp.ppl          = method->final_ppl;
    return fp;
}

void destroy_finalpass(dx11renderer* r, const ss_finalpass* fp) {
}

void exec_finalpass(dx11renderer* r, const ss_finalpass* fp) {

    set_backbuffer(r);
    depthtest_masktest(r);

    set_pipeline(r, fp->ppl);
    set_texds(r, 2, fp->tex_depth);
    set_texrt(r, 3, fp->tex_diffuse);
    set_texrt(r, 4, fp->tex_specular);

    draw_nulls(r, 4);
    present_target(r);

    depthtest_on(r);
    unset_textures(r, 2, 3);
}