#pragma once

#include "../camera.h"
#include "../dx11render.h"
#include "../point_light.h"

struct ts_shadowpass {
    point_light* light;
    ref          depth_stencil;
    ref          rt_shadowtexdepth;
    ref          rt_shadownormals;
    ref          mesh;
    ref          ppl;
};

ts_shadowpass create_tsshadowpass(dx11renderer* r, point_light* l, ref mesh, ref ppl, int2 map_dims);
void          exec_tsshadowpass(dx11renderer* r, ts_shadowpass* shp);
void          destroy_tsshadowpass(dx11renderer* r, ts_shadowpass* shp);
