#pragma once

#include "../camera.h"
#include "../reflist.h"
#include "../dx11render.h"

struct ss_maskpass {
    camera*      camera;
    ref          mesh;
    ref          ppl;
    ref          ds_mask;
};

ss_maskpass create_maskpass(dx11renderer* r, int2 backbuffer_dims, ref mesh, ref ppl, camera* cam);
void        destroy_maskpass(dx11renderer* r, ss_maskpass* mp);
void        exec_maskpass(dx11renderer* r, ss_maskpass* mp);
