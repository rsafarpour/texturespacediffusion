#pragma once

#include "../dx11render.h"

struct ts_finalpass {
    point_light* light;
    camera*      render_view;
    ref          blurs[6];
    ref          mesh;
    ref          ppl;
    ref          profile_buffer;
    ref          lightmap_ds;
    ref          lightmap;
};

ts_finalpass create_finalpass(dx11renderer* r, camera* render_view, ref mesh, ref ppl, ref profile_buffer, ref lightmap_ds, ref lightmap, point_light* light, std::vector<ref> blurs);
void         destroy_finalpass(dx11renderer* r, const ts_finalpass* fp);
void         exec_finalpass(dx11renderer* r, const ts_finalpass* fp);