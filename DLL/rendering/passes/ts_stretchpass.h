#pragma once

#include "../dx11render.h"

struct ts_stretchpass {
    ref tex_output;
    ref mesh;
    ref ppl;
};

ts_stretchpass create_stretchpass(dx11renderer* r, ref ppl, ref mesh, int2 dims);
void           destroy_stretchpass(dx11renderer* r, const ts_stretchpass* sp);
void           exec_stretchpass(dx11renderer* r, const ts_stretchpass* sp);