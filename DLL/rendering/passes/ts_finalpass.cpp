#include "ts_finalpass.h"
#include "../point_light.h"

ts_finalpass create_finalpass(dx11renderer* r, camera* render_view, ref mesh, ref ppl, ref profile_buffer, ref lightmap_ds, ref lightmap, point_light* light, std::vector<ref> blurs) {
    if (blurs.size() == 6) {
        ts_finalpass fp;
        fp.render_view    = render_view;
        fp.ppl            = ppl;
        fp.mesh           = mesh;
        fp.profile_buffer = profile_buffer;
        fp.blurs[0]       = blurs[0];
        fp.blurs[1]       = blurs[1];
        fp.blurs[2]       = blurs[2];
        fp.blurs[3]       = blurs[3];
        fp.blurs[4]       = blurs[4];
        fp.blurs[5]       = blurs[5];
        fp.lightmap       = lightmap;
        fp.lightmap_ds    = lightmap_ds;

        fp.light    = light;
        return fp;
    }
    return {};
}

void destroy_finalpass(dx11renderer* r, const ts_finalpass* fp) {
}

void exec_finalpass(dx11renderer* r, const ts_finalpass* fp) {
    set_backbuffer(r);

    set_texrt(r,  4, fp->lightmap);
    set_texds(r,  5, fp->lightmap_ds);
    set_texrt(r,  6, fp->blurs[0]);
    set_texrt(r,  7, fp->blurs[1]);
    set_texrt(r,  8, fp->blurs[2]);
    set_texrt(r,  9, fp->blurs[3]);
    set_texrt(r, 10, fp->blurs[4]);
    set_texrt(r, 11, fp->blurs[5]);

    set_pipeline(r, fp->ppl);
    draw_mesh(r, fp->mesh);

    unset_textures(r, 3, 11);
}