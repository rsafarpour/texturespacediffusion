#include "ss_maskpass.h"

ss_maskpass create_maskpass(dx11renderer* r, int2 backbuffer_dims, ref mesh, ref ppl, camera* cam) {
    ss_maskpass mp;
    mp.camera  = cam;
    mp.ds_mask = create_depth(r, backbuffer_dims);
    mp.mesh    = mesh;
    mp.ppl     = ppl;

    return mp;
}

void destroy_maskpass(dx11renderer* r, ss_maskpass* mp) {
    destroy_depthstencil(r, mp->ds_mask);
}

void exec_maskpass(dx11renderer* r, ss_maskpass* mp) {

    // Setup pipeline to fill stencil only
    clear_backbuffer(r);
    set_backbuffer_ds(r);
    set_pipeline(r, mp->ppl);
    depthtest_genmask(r);

    draw_mesh(r, mp->mesh);
}