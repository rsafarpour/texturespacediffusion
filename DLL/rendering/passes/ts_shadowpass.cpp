#include "ts_shadowpass.h"

ts_shadowpass create_tsshadowpass(dx11renderer* r, point_light* l, ref mesh, ref ppl, int2 map_dims) {
    ts_shadowpass shp;
    shp.light = l;
    shp.mesh = mesh;
    shp.depth_stencil = create_depth(r, map_dims);
    shp.rt_shadowtexdepth = create_target(r, map_dims, DXGI_FORMAT_R16G16B16A16_FLOAT);
    shp.rt_shadownormals = create_target(r, map_dims, DXGI_FORMAT_R16G16B16A16_FLOAT);
    shp.ppl = ppl;

    return shp;
}

void exec_tsshadowpass(dx11renderer* r, ts_shadowpass* shp) {

    // First step: Update light view to the latest light position
    float3 p = {
        shp->light->position.x,
        shp->light->position.y,
        shp->light->position.z
    };
    
    set_uniformbuffer(r, shp->light->buffer_ref, 1);
    
    // Render normal texture and UV-depth texture
    set_targets(r, shp->rt_shadowtexdepth, shp->rt_shadownormals, shp->depth_stencil);
    set_pipeline(r, shp->ppl);
    clear_depth(r, shp->depth_stencil);
    clear_target(r, shp->rt_shadowtexdepth, {0.0f, 0.0f, 0.0f, 0.0f});
    clear_target(r, shp->rt_shadownormals, {0.0f, 0.0f, 0.0f, 0.0f});
    
    draw_mesh(r, shp->mesh);
}

void destroy_tsshadowpass(dx11renderer* r, ts_shadowpass* shp) {
    destroy_target(r, shp->rt_shadownormals);
    destroy_target(r, shp->rt_shadowtexdepth);
    destroy_depthstencil(r, shp->depth_stencil);
}