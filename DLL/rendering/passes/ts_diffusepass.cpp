#include "ts_diffusepass.h"
#include "../point_light.h"

ts_diffusepass create_diffusepass(dx11renderer* r, ref ppl, point_light* light, ref mesh, ref lightmap_ds, ref shadow_uvdepth, ref shadow_normals, int2 dims) {
    ts_diffusepass dp;
    dp.mesh             = mesh;
    dp.ppl              = ppl;
    dp.tex_output       = create_target(r, dims, DXGI_FORMAT_R32G32B32A32_FLOAT);
    dp.light            = light;
    dp.lightmap_uvdepth = shadow_uvdepth;
    dp.lightmap_normals = shadow_normals;
    dp.lightmap_ds      = lightmap_ds;

    return dp;
}

void destroy_diffusepass(dx11renderer* r, const ts_diffusepass* dp) {
    destroy_target(r, dp->tex_output);
}

void exec_diffusepass(dx11renderer* r, const ts_diffusepass* dp) {
    clear_target(r, dp->tex_output);
    set_target(r, dp->tex_output);

    set_texrt(r, 4, dp->lightmap_uvdepth);
    set_texrt(r, 5, dp->lightmap_normals);
    set_texds(r, 6, dp->lightmap_ds);

    set_pipeline(r, dp->ppl);
    draw_mesh(r, dp->mesh);

    unset_target(r);
}