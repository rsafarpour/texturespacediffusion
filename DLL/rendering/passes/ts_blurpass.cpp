#include "ts_blurpass.h"

ts_blurpass create_blurpass(dx11renderer* r, float variance, ref rt_tmp1, ref rt_tmp2, ref input, ref stretch, int2 dims, ref pplH, ref pplV) {
    ts_blurpass bp;
    bp.variance_buffer = create_uniformbuffer(r, sizeof(float));
    upload_bufferdata(r, bp.variance_buffer, &variance, sizeof(float));

    bp.rt_tmp1 = rt_tmp1;
    bp.rt_tmp2 = rt_tmp2;

    bp.tex_irradiance   = input;
    bp.tex_stretch      = stretch;
    bp.tex_convolution  = create_target(r, dims, DXGI_FORMAT_R32G32B32A32_FLOAT);
    bp.pipelineH        = pplH;
    bp.pipelineV        = pplV;
    return bp;
}

void destroy_blurpass(dx11renderer* r, const ts_blurpass* bp) {
    destroy_uniformbuffer(r, bp->variance_buffer);
    destroy_target(r, bp->tex_convolution);
}

void exec_blurpass(dx11renderer* r, const ts_blurpass* bp) {

    // Horizontal blur stretch texture
    set_psuniformbuffer(r, bp->variance_buffer, 4);
    set_texrt(r, 12, bp->tex_stretch);
    set_texrt(r, 13, bp->tex_stretch);
    set_target(r, bp->rt_tmp1);
    clear_target(r, bp->rt_tmp1);
    set_pipeline(r, bp->pipelineH);

    draw_nulls(r, 3);

    // Vertical blur stretch texture
    set_target(r, bp->rt_tmp2);
    clear_target(r, bp->rt_tmp2);
    set_texrt(r, 13, bp->rt_tmp1);

    set_pipeline(r, bp->pipelineV);
    draw_nulls(r, 3);

    // Update stretch texture with new content
    ID3D11Resource* src = r->targets.get(bp->rt_tmp2)->resource;
    ID3D11Resource* dst = r->targets.get(bp->tex_stretch)->resource;
    r->ctx->CopyResource(dst, src);


    // Horizontal blur irradiance texture
    unset_textures(r, 13, 1);
    set_texrt(r, 12, bp->tex_irradiance);
    set_texrt(r, 13, bp->tex_stretch);
    set_target(r, bp->rt_tmp1);
    clear_target(r, bp->rt_tmp1);

    set_pipeline(r, bp->pipelineH);
    draw_nulls(r, 3);

    // Vertical blur irradiance texture
    set_target(r, bp->tex_convolution);
    clear_target(r, bp->tex_convolution);
    
    set_texrt(r, 12, bp->rt_tmp1);
    set_pipeline(r, bp->pipelineV);
    draw_nulls(r, 3);

    unset_target(r);
}
