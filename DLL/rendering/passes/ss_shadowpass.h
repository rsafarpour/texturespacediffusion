#pragma once

#include "../dx11render.h"
#include "../point_light.h"
#include "../reflist.h"

struct ss_shadowpass {
    point_light* light;
    ref ppl;
    ref ds_zbuffer;
    ref mesh;
};

ss_shadowpass create_ssshadowpass(dx11renderer* r, point_light* light, ref mesh, ref ppl);
void          destroy_ssshadowpass(dx11renderer* r, ss_shadowpass* sp);
void          exec_ssshadowpass(dx11renderer* r, ss_shadowpass* sp);
