#include "../../conf.h"
#include "ss_shadowpass.h"

ss_shadowpass create_ssshadowpass(dx11renderer* r, point_light* light, ref mesh, ref ppl) {
    ss_shadowpass sp = {};
    sp.light        = light;
    sp.ppl          = ppl;
    sp.ds_zbuffer   = create_depth(r, { LIGHTMAP_DIMS, LIGHTMAP_DIMS });
    sp.mesh         = mesh;
    return sp;
}

void destroy_ssshadowpass(dx11renderer* r, ss_shadowpass* sp) {
    destroy_depthstencil(r, sp->ds_zbuffer);
}

void exec_ssshadowpass(dx11renderer* r, ss_shadowpass* sp) {
    
    clear_depth(r, sp->ds_zbuffer);
    
    set_viewport(r, { LIGHTMAP_DIMS, LIGHTMAP_DIMS });
    set_pipeline(r, sp->ppl);
    set_depthstencil(r, sp->ds_zbuffer);
    set_uniformbuffer(r, sp->light->buffer_ref, 1);
    
    draw_mesh(r, sp->mesh);
}