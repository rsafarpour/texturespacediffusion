#include "ts_stretchpass.h"

ts_stretchpass create_stretchpass(dx11renderer* r, ref ppl, ref mesh, int2 dims) {
    ts_stretchpass sp;
    sp.mesh = mesh;
    sp.ppl = ppl;
    sp.tex_output = create_target(r, dims, DXGI_FORMAT_R16G16B16A16_FLOAT);

    return sp;
}

void destroy_stretchpass(dx11renderer* r, const ts_stretchpass* sp) {
    destroy_target(r, sp->tex_output);
}

void exec_stretchpass(dx11renderer* r, const ts_stretchpass* sp) {
    clear_target(r, sp->tex_output);
    set_target(r, sp->tex_output);

    set_pipeline(r, sp->ppl);
    draw_mesh(r, sp->mesh);

    unset_target(r);
}