#pragma once

#include "../reflist.h"
#include "../dx11render.h"

struct ts_blurpass {
    ref variance_buffer;
    ref rt_tmp1;
    ref rt_tmp2;
    ref tex_irradiance;
    ref tex_stretch;
    ref tex_convolution;
    ref pipelineH;
    ref pipelineV;
};

struct stretchpass {

};

ts_blurpass create_blurpass(dx11renderer* r, float variance, ref rt_tmp1, ref rt_tmp2, ref input, ref stretch, int2 dims, ref pplH, ref pplV);
void      destroy_blurpass(dx11renderer* r, const ts_blurpass* bp);
void      exec_blurpass(dx11renderer* r, const ts_blurpass* bp);