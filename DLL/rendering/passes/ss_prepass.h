#pragma once

#include "../camera.h"
#include "../point_light.h"
#include "../reflist.h"
#include "../dx11render.h"

struct ss_prepass {
    point_light* light;
    camera*      camera;
    ref          mesh;
    ref          ppl;
    ref          rt_diffuse;
    ref          rt_specular;
    ref          ds_mask;
    ref          tex_shadowmap;
};

ss_prepass create_prepass(dx11renderer* r, int2 backbuffer_dims, ref mesh, ref ppl, camera* cam, point_light* light, ref tex_shadowmap);
void       destroy_prepass(dx11renderer* r, ss_prepass* pp);
void       exec_prepass(dx11renderer* r, ss_prepass* pp);
