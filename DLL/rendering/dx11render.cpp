#include "dx11render.h"
#include "camera.h"
#include "point_light.h"
#include "../output.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

dx11renderer create_renderer(HWND output_window, int width, int height) {
    dx11renderer renderer;

    if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&renderer.dxgi))) {
        MessageBox(NULL, TEXT("Failed initialising DXGI"), TEXT("Error"), MB_OK);
        return {};
    }
    
    if (FAILED(renderer.dxgi->EnumAdapters(0, &renderer.adapt))) {
        MessageBox(NULL, TEXT("Failed enumerating Adapters"), TEXT("Error"), MB_OK);
        return {};
    }

    D3D_FEATURE_LEVEL levels = { D3D_FEATURE_LEVEL_11_0 };
    DXGI_SWAP_CHAIN_DESC sc_desc               = {};
    sc_desc.BufferCount                        = 2;
    sc_desc.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    sc_desc.BufferDesc.Height                  = height;
    sc_desc.BufferDesc.RefreshRate.Denominator = 60;
    sc_desc.BufferDesc.RefreshRate.Numerator   = 1;
    sc_desc.BufferDesc.Scaling                 = DXGI_MODE_SCALING_UNSPECIFIED;
    sc_desc.BufferDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
    sc_desc.BufferDesc.Width                   = width;
    sc_desc.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sc_desc.Flags                              = 0;
    sc_desc.OutputWindow                       = output_window;
    sc_desc.SampleDesc.Count                   = 1;
    sc_desc.SampleDesc.Quality                 = 0;
    sc_desc.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;
    sc_desc.Windowed                           = true;


    if (FAILED(D3D11CreateDeviceAndSwapChain(
        renderer.adapt,
        D3D_DRIVER_TYPE_UNKNOWN,
        nullptr,
        D3D11_CREATE_DEVICE_DEBUG,
        &levels,
        1,
        D3D11_SDK_VERSION,
        &sc_desc,
        &renderer.sc,
        &renderer.dev,
        &renderer.feature_level,
        &renderer.ctx))) {
        MessageBox(NULL, TEXT("Failed creating swap chain"), TEXT("Error"), MB_OK);
        return {};
    }
    
    renderer.backbuffer_dims = { width, height };

    D3D11_DEPTH_STENCIL_DESC dsstate_offdesc = {};
    dsstate_offdesc.DepthEnable             = false;
    dsstate_offdesc.StencilEnable           = false;
    dsstate_offdesc.DepthFunc               = D3D11_COMPARISON_ALWAYS;
    dsstate_offdesc.DepthWriteMask          = D3D11_DEPTH_WRITE_MASK_ALL;
    dsstate_offdesc.BackFace.StencilFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_offdesc.BackFace.StencilPassOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_offdesc.BackFace.StencilFunc    = D3D11_COMPARISON_ALWAYS;
    dsstate_offdesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsstate_offdesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsstate_offdesc.FrontFace.StencilFunc   = D3D11_COMPARISON_ALWAYS;
    renderer.dev->CreateDepthStencilState(&dsstate_offdesc, &renderer.dsstate_depthoff);

    D3D11_DEPTH_STENCIL_DESC dsstate_ondesc = {};
    dsstate_ondesc.DepthEnable             = true;
    dsstate_ondesc.StencilEnable           = false;
    dsstate_ondesc.DepthFunc               = D3D11_COMPARISON_LESS;
    dsstate_ondesc.DepthWriteMask          = D3D11_DEPTH_WRITE_MASK_ALL;
    dsstate_ondesc.BackFace.StencilFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_ondesc.BackFace.StencilPassOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_ondesc.BackFace.StencilFunc    = D3D11_COMPARISON_ALWAYS;
    dsstate_ondesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsstate_ondesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsstate_ondesc.FrontFace.StencilFunc   = D3D11_COMPARISON_ALWAYS;
    renderer.dev->CreateDepthStencilState(&dsstate_ondesc, &renderer.dsstate_depthon);
    renderer.ctx->OMSetDepthStencilState(renderer.dsstate_depthon, 0);

    D3D11_DEPTH_STENCIL_DESC dsstate_genmaskdesc = {};
    dsstate_genmaskdesc.DepthEnable                   = true;
    dsstate_genmaskdesc.StencilEnable                 = true;
    dsstate_genmaskdesc.DepthFunc                     = D3D11_COMPARISON_LESS;
    dsstate_genmaskdesc.DepthWriteMask                = D3D11_DEPTH_WRITE_MASK_ZERO;
    dsstate_genmaskdesc.StencilWriteMask              = D3D11_DEFAULT_STENCIL_WRITE_MASK;
    dsstate_genmaskdesc.BackFace.StencilDepthFailOp   = D3D11_STENCIL_OP_KEEP;
    dsstate_genmaskdesc.BackFace.StencilFailOp        = D3D11_STENCIL_OP_KEEP;
    dsstate_genmaskdesc.BackFace.StencilPassOp        = D3D11_STENCIL_OP_KEEP;
    dsstate_genmaskdesc.BackFace.StencilFunc          = D3D11_COMPARISON_ALWAYS;
    dsstate_genmaskdesc.FrontFace.StencilDepthFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_genmaskdesc.FrontFace.StencilFailOp       = D3D11_STENCIL_OP_KEEP;
    dsstate_genmaskdesc.FrontFace.StencilPassOp       = D3D11_STENCIL_OP_REPLACE;
    dsstate_genmaskdesc.FrontFace.StencilFunc         = D3D11_COMPARISON_ALWAYS;
    renderer.dev->CreateDepthStencilState(&dsstate_genmaskdesc, &renderer.dsstate_genmask);

    D3D11_DEPTH_STENCIL_DESC dsstate_drawmaskdesc = {};
    dsstate_drawmaskdesc.DepthEnable                  = true;
    dsstate_drawmaskdesc.StencilEnable                = true;
    dsstate_drawmaskdesc.DepthFunc                    = D3D11_COMPARISON_LESS;
    dsstate_drawmaskdesc.DepthWriteMask               = D3D11_DEPTH_WRITE_MASK_ALL;
    dsstate_drawmaskdesc.StencilWriteMask             = D3D11_DEFAULT_STENCIL_WRITE_MASK;
    dsstate_drawmaskdesc.StencilReadMask              = D3D11_DEFAULT_STENCIL_READ_MASK;
    dsstate_drawmaskdesc.BackFace.StencilDepthFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_drawmaskdesc.BackFace.StencilFailOp       = D3D11_STENCIL_OP_KEEP;
    dsstate_drawmaskdesc.BackFace.StencilPassOp       = D3D11_STENCIL_OP_KEEP;
    dsstate_drawmaskdesc.BackFace.StencilFunc         = D3D11_COMPARISON_NEVER;
    dsstate_drawmaskdesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
    dsstate_drawmaskdesc.FrontFace.StencilFailOp      = D3D11_STENCIL_OP_KEEP;
    dsstate_drawmaskdesc.FrontFace.StencilPassOp      = D3D11_STENCIL_OP_REPLACE;
    dsstate_drawmaskdesc.FrontFace.StencilFunc        = D3D11_COMPARISON_EQUAL;
    renderer.dev->CreateDepthStencilState(&dsstate_drawmaskdesc, &renderer.dsstate_drawmask);
    
    D3D11_TEXTURE2D_DESC dstexture_desc;
    dstexture_desc.ArraySize = 1;
    dstexture_desc.BindFlags = D3D10_BIND_DEPTH_STENCIL;
    dstexture_desc.CPUAccessFlags = 0;
    dstexture_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dstexture_desc.Height = height;
    dstexture_desc.MipLevels = 1;
    dstexture_desc.MiscFlags = 0;
    dstexture_desc.SampleDesc.Count = 1;
    dstexture_desc.SampleDesc.Quality = 0;
    dstexture_desc.Usage = D3D11_USAGE_DEFAULT;
    dstexture_desc.Width = width;

    D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
    dsv_desc.Flags = 0;
    dsv_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsv_desc.Texture2D.MipSlice = 0;


    // FIXED blend & rasterizer state
    D3D11_BLEND_DESC blend_desc = {};
    blend_desc.AlphaToCoverageEnable                 = false;
    blend_desc.IndependentBlendEnable                = false;
    blend_desc.RenderTarget[0].BlendEnable           = false;
    blend_desc.RenderTarget[0].BlendOp               = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].BlendOpAlpha          = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].DestBlend             = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].DestBlendAlpha        = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].SrcBlend              = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].SrcBlendAlpha         = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    D3D11_RASTERIZER_DESC rasterizer_desc = {};
    rasterizer_desc.AntialiasedLineEnable = false;
    rasterizer_desc.CullMode              = D3D11_CULL_BACK;
    rasterizer_desc.DepthClipEnable       = true;
    rasterizer_desc.FillMode              = D3D11_FILL_SOLID;
    rasterizer_desc.FrontCounterClockwise = true;
    rasterizer_desc.MultisampleEnable     = false;
    rasterizer_desc.ScissorEnable         = false;
    rasterizer_desc.DepthBias             = 15'000;
    //rasterizer_desc.SlopeScaledDepthBias  = 1.6;
    //rasterizer_desc.DepthBiasClamp        = 0.007;

    // Create bilinear sampler
    D3D11_SAMPLER_DESC sampler_desc = {};
    sampler_desc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampler_desc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampler_desc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    sampler_desc.Filter         = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampler_desc.MaxLOD         = D3D11_FLOAT32_MAX;

    // Create PCF sampler
    D3D11_SAMPLER_DESC pcf_desc = {};
    pcf_desc.AddressU       = D3D11_TEXTURE_ADDRESS_WRAP;
    pcf_desc.AddressV       = D3D11_TEXTURE_ADDRESS_WRAP;
    pcf_desc.AddressW       = D3D11_TEXTURE_ADDRESS_WRAP;
    pcf_desc.ComparisonFunc = D3D11_COMPARISON_LESS;
    pcf_desc.Filter         = D3D11_FILTER_COMPARISON_ANISOTROPIC;

    ID3D11Texture2D* backbuffer;
    D3D11_RENDER_TARGET_VIEW_DESC rt;
    rt.ViewDimension        = D3D11_RTV_DIMENSION_TEXTURE2D;
    rt.Format               = DXGI_FORMAT_R8G8B8A8_UNORM;
    rt.Texture2D.MipSlice   = 0;

    if(FAILED(renderer.dev->CreateTexture2D(&dstexture_desc, nullptr, &renderer.ds_texture)) ||
        FAILED(renderer.dev->CreateDepthStencilView(renderer.ds_texture, &dsv_desc, &renderer.dsv)) ||
        FAILED(renderer.dev->CreateRasterizerState(&rasterizer_desc, &renderer.rasterizer_state)) ||
        FAILED(renderer.dev->CreateBlendState(&blend_desc, &renderer.blend_state)) ||
        FAILED(renderer.dev->CreateSamplerState(&sampler_desc, &renderer.bilinear_sampler)) ||
        FAILED(renderer.dev->CreateSamplerState(&pcf_desc, &renderer.pcf_sampler)) ||
        FAILED(renderer.sc->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbuffer)) ||
        FAILED(renderer.dev->CreateRenderTargetView(backbuffer, nullptr, &renderer.rtv))) {
        MessageBox(NULL, TEXT("Failed configuring DirectX"), TEXT("Error"), MB_OK);
        return {};
    }
    backbuffer->Release();

    ID3D11SamplerState* samplers[] = { renderer.bilinear_sampler, renderer.pcf_sampler };
    renderer.ctx->PSSetSamplers(0, 2, samplers);

    renderer.ctx->OMSetRenderTargets(1, &renderer.rtv, renderer.dsv);
    renderer.ctx->OMSetBlendState(renderer.blend_state, nullptr, 0xFFFFFFFF);
    renderer.ctx->RSSetState(renderer.rasterizer_state);

    return renderer;
}

ref  create_target(dx11renderer* renderer, int2 dims, DXGI_FORMAT format) {
    D3D11_TEXTURE2D_DESC tex_desc = {};
    tex_desc.ArraySize = 1;
    tex_desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    tex_desc.Format = format;
    tex_desc.Height = dims.y;
    tex_desc.MipLevels = 1;
    tex_desc.SampleDesc.Count = 1;
    tex_desc.Usage = D3D11_USAGE_DEFAULT;
    tex_desc.Width = dims.x;

    ID3D11Texture2D* texture;
    if (FAILED(renderer->dev->CreateTexture2D(&tex_desc, nullptr, &texture))) {
        return invalid_ref;
    }



    D3D11_RENDER_TARGET_VIEW_DESC rtv_desc = {};
    rtv_desc.Format             = format;
    rtv_desc.Texture2D.MipSlice = 0;
    rtv_desc.ViewDimension      = D3D11_RTV_DIMENSION_TEXTURE2D;

    ID3D11RenderTargetView* rtv;
    if (FAILED(renderer->dev->CreateRenderTargetView(texture, &rtv_desc, &rtv))) {
        texture->Release();
        return invalid_ref;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc = {};
    srv_desc.Format              = format;
    srv_desc.Texture2D.MipLevels = 1;
    srv_desc.ViewDimension       = D3D11_SRV_DIMENSION_TEXTURE2D;

    ID3D11ShaderResourceView* srv;
    if (FAILED(renderer->dev->CreateShaderResourceView(texture, &srv_desc, &srv))) {
        texture->Release();
        rtv->Release();
        return invalid_ref;
    }

    ref r = renderer->targets.make();
    renderer->targets.get(r)->resource = texture;
    renderer->targets.get(r)->rtv = rtv;
    renderer->targets.get(r)->srv = srv;
    return r;
}

ref create_uav(dx11renderer* renderer, int2 dims) {
    D3D11_TEXTURE2D_DESC tex_desc = {};
    tex_desc.ArraySize        = 1;
    tex_desc.BindFlags        = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
    tex_desc.Format           = DXGI_FORMAT_D24_UNORM_S8_UINT;
    tex_desc.Height           = dims.y;
    tex_desc.MipLevels        = 1;
    tex_desc.SampleDesc.Count = 1;
    tex_desc.Usage            = D3D11_USAGE_DEFAULT;
    tex_desc.Width            = dims.x;

    ID3D11Texture2D* texture;
    if (FAILED(renderer->dev->CreateTexture2D(&tex_desc, nullptr, &texture))) {
        return invalid_ref;
    }




    D3D11_UNORDERED_ACCESS_VIEW_DESC uav_desc = {};
    uav_desc.Format             = DXGI_FORMAT_R8G8B8A8_UNORM;
    uav_desc.ViewDimension      = D3D11_UAV_DIMENSION_TEXTURE2D;

    ID3D11UnorderedAccessView* uav;
    if (FAILED(renderer->dev->CreateUnorderedAccessView(texture, &uav_desc, &uav))) {
        texture->Release();
        return invalid_ref;
    }




    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc = {};
    srv_desc.Format              = DXGI_FORMAT_R8G8B8A8_UNORM;
    srv_desc.Texture2D.MipLevels = 1;
    srv_desc.ViewDimension       = D3D_SRV_DIMENSION_TEXTURE2D;

    ID3D11ShaderResourceView* srv;
    if (FAILED(renderer->dev->CreateShaderResourceView(texture, &srv_desc, &srv))) {
        texture->Release();
        uav->Release();
        return invalid_ref;
    }

    ref r = renderer->uavs.make();
    renderer->uavs.get(r)->resource = texture;
    renderer->uavs.get(r)->uav = uav;
    renderer->uavs.get(r)->srv = srv;

    return r;
}


ref create_depth(dx11renderer* renderer, int2 dims) {
    D3D11_TEXTURE2D_DESC tex_desc = {};
    tex_desc.ArraySize        = 1;
    tex_desc.BindFlags        = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
    tex_desc.Format           = DXGI_FORMAT_R24G8_TYPELESS;
    tex_desc.Height           = dims.y;
    tex_desc.MipLevels        = 1;
    tex_desc.SampleDesc.Count = 1;
    tex_desc.Usage            = D3D11_USAGE_DEFAULT;
    tex_desc.Width            = dims.x;

    ID3D11Texture2D* texture;
    if (FAILED(renderer->dev->CreateTexture2D(&tex_desc, nullptr, &texture))) {
        return invalid_ref;
    }



    D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc = {};
    dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsv_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

    ID3D11DepthStencilView* dsv;
    if (FAILED(renderer->dev->CreateDepthStencilView(texture, &dsv_desc, &dsv))) {
        texture->Release();
        return invalid_ref;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc = {};
    srv_desc.Format              = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
    srv_desc.Texture2D.MipLevels = 1;
    srv_desc.ViewDimension       = D3D_SRV_DIMENSION_TEXTURE2D;

    ID3D11ShaderResourceView* srv;
    if (FAILED(renderer->dev->CreateShaderResourceView(texture, &srv_desc, &srv))) {
        texture->Release();
        dsv->Release();
        return invalid_ref;
    }

    ref r = renderer->depthstencils.make();
    renderer->depthstencils.get(r)->resource = texture;
    renderer->depthstencils.get(r)->dsv = dsv;
    renderer->depthstencils.get(r)->srv = srv;
    return r;
}

ref create_uniformbuffer(dx11renderer* renderer, unsigned bytes) {
    
    // Guarantee buffer size to be multiple of 16
    unsigned mul16 = bytes + 15;
    mul16 &= mul16 & ~15;

    D3D11_BUFFER_DESC buffer_desc = {};
    buffer_desc.BindFlags           = D3D11_BIND_CONSTANT_BUFFER;
    buffer_desc.ByteWidth           = mul16;
    buffer_desc.StructureByteStride = 0;
    buffer_desc.Usage               = D3D11_USAGE_DYNAMIC;
    buffer_desc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;

    ID3D11Buffer* buffer;
    if (FAILED(renderer->dev->CreateBuffer(&buffer_desc, nullptr, &buffer))) {
        print_error("Failed creating view matrix\n");
        return invalid_ref;
    }

    ref r = renderer->uniforms.make();
    renderer->uniforms.get(r)->buffer = buffer;
    renderer->uniforms.get(r)->bytes = bytes;
    return r;
}

void clear_backbuffer(dx11renderer* renderer) {
    FLOAT clear_color[] = { 0.0f, 0.0f, 0.0f, 0.0 };
    renderer->ctx->ClearRenderTargetView(renderer->rtv, clear_color);
    renderer->ctx->ClearDepthStencilView(renderer->dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void clear_target(dx11renderer* renderer, ref target_ref) {
    clear_target(renderer, target_ref, { 0.0f, 0.0f, 0.0f, 0.0f });
}

void clear_target(dx11renderer* renderer, ref target_ref, float4 color) {
    dx11target* target = renderer->targets.get(target_ref);
    if (target != nullptr) {
        float clear_color[] = { color.x, color.y, color.z, color.w };
        renderer->ctx->ClearRenderTargetView(target->rtv, clear_color);
    }
}

void clear_depth(dx11renderer* renderer, ref depth_ref) {
    dx11depthstencil* ds = renderer->depthstencils.get(depth_ref);
    if (ds != nullptr) {
        renderer->ctx->ClearDepthStencilView(ds->dsv, D3D11_CLEAR_DEPTH, 1.0, 0);
    }
}

void depthtest_on(dx11renderer* renderer) {
    renderer->ctx->OMSetDepthStencilState(renderer->dsstate_depthon, 0x0);
}

void depthtest_off(dx11renderer* renderer) {
    renderer->ctx->OMSetDepthStencilState(renderer->dsstate_depthoff, 0x0);
}

void depthtest_genmask(dx11renderer* renderer) {
    renderer->ctx->OMSetDepthStencilState(renderer->dsstate_genmask, 1);
}

void depthtest_masktest(dx11renderer* renderer) {
    renderer->ctx->OMSetDepthStencilState(renderer->dsstate_drawmask, 1);
}

void destroy_renderer(dx11renderer* renderer) {
    if (renderer->dsstate_depthoff != nullptr) { renderer->dsstate_depthoff->Release(); }
    if (renderer->dsstate_depthon  != nullptr) { renderer->dsstate_depthon->Release();  }
    if (renderer->dsstate_genmask  != nullptr) { renderer->dsstate_genmask->Release();  }
    if (renderer->dsstate_drawmask != nullptr) { renderer->dsstate_drawmask->Release(); }
    if (renderer->blend_state      != nullptr) { renderer->blend_state->Release();      }
    if (renderer->rasterizer_state != nullptr) { renderer->rasterizer_state->Release(); }
    if (renderer->ds_texture       != nullptr) { renderer->ds_texture->Release();       }
    if (renderer->dsv              != nullptr) { renderer->dsv->Release();              }
    if (renderer->rtv              != nullptr) { renderer->rtv->Release();              }
    if (renderer->bilinear_sampler != nullptr) { renderer->bilinear_sampler->Release(); }
    if (renderer->pcf_sampler      != nullptr) { renderer->pcf_sampler->Release();      }

    renderer->dev->Release();
    renderer->ctx->Release();
    renderer->sc->Release();
    renderer->adapt->Release();
    renderer->dxgi->Release();

    for (auto state : renderer->pipelines) {
        if (state.layout != nullptr) { state.layout->Release(); }
        if (state.vs != nullptr) { state.vs->Release(); }
        if (state.ps != nullptr) { state.ps->Release(); }
    }

    for (auto tex2d : renderer->textures2d) {
        if (tex2d.resource != nullptr) { tex2d.resource->Release(); }
        if (tex2d.view != nullptr) { tex2d.view->Release(); }
    }
}

void destroy_target(dx11renderer* renderer, ref target) {
    dx11target* rt = renderer->targets.get(target);
    if (rt != nullptr) {
        rt->resource->Release();
        rt->rtv->Release();
        rt->srv->Release();
        renderer->targets.release(target);
    }
}

void destroy_depthstencil(dx11renderer* renderer, ref depthstencil) {
    dx11depthstencil* ds = renderer->depthstencils.get(depthstencil);
    if (ds != nullptr) {
        ds->dsv->Release();
        ds->srv->Release();
        ds->resource->Release();
        renderer->depthstencils.release(depthstencil);
    }
}

void destroy_pipelinestate(dx11renderer* renderer, ref state_ref) {
    ppl_obj* pipeline = renderer->pipelines.get(state_ref);
    if (pipeline != nullptr) {
        pipeline->layout->Release();
        pipeline->ps->Release();
        pipeline->vs->Release();
        renderer->pipelines.release(state_ref);
    }
}

void destroy_mesh(dx11renderer* renderer, ref mesh_ref) {
    dx11mesh* mesh = renderer->meshes.get(mesh_ref);
    if (mesh != nullptr) {
        mesh->index_buffer->Release();
        mesh->vertex_buffer->Release();
        renderer->meshes.release(mesh_ref);
    }
}

void destroy_texture(dx11renderer* renderer, ref tex_ref) {
    dx11texture2d* texture = renderer->textures2d.get(tex_ref);
    if (texture != nullptr) {
        texture->resource->Release();
        texture->view->Release();
        renderer->textures2d.release(tex_ref);
    }
}

void destroy_uniformbuffer(dx11renderer* renderer, ref buffer_ref) {
    dx11uniformbuffer* uniform = renderer->uniforms.get(buffer_ref);
    if (uniform != nullptr) {
        uniform->buffer->Release();
        renderer->uniforms.release(buffer_ref);
    }
}

void draw_mesh(dx11renderer* renderer, ref r) {
    dx11mesh* m = renderer->meshes.get(r);
    if (m != nullptr) {
        ID3D11Buffer* vbuffer[] = { m->vertex_buffer };
        ID3D11Buffer* ibuffer = m->index_buffer;
        UINT stride[] = { m->vertex_stride };
        UINT offsets = 0;
        renderer->ctx->IASetVertexBuffers(0, 1, vbuffer, stride, &offsets);
        renderer->ctx->IASetIndexBuffer(ibuffer, DXGI_FORMAT_R32_UINT, 0);
        renderer->ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        renderer->ctx->DrawIndexed(m->num_indices, 0, 0);
    }
}

void draw_nulls(dx11renderer* renderer, unsigned vertices) {
    renderer->ctx->IASetVertexBuffers(0, 0, nullptr, 0, 0);
    renderer->ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    renderer->ctx->Draw(vertices, 0);
}

void present_target(dx11renderer* renderer) {
    renderer->sc->Present(0, 0);
}

void set_backbuffer(dx11renderer* renderer) {
    renderer->ctx->OMSetRenderTargets(1, &renderer->rtv, renderer->dsv);
}

void set_backbuffer_ds(dx11renderer* renderer) {
    ID3D11RenderTargetView* rtv[] = { nullptr };
    renderer->ctx->OMSetRenderTargets(1, rtv, renderer->dsv);
}

void set_depthstencil(dx11renderer* renderer, ref depthstencil_ref) {
    dx11depthstencil* ds = renderer->depthstencils.get(depthstencil_ref);
    if (ds != nullptr) {
        ID3D11RenderTargetView* rtv[] = {nullptr};
        renderer->ctx->OMSetRenderTargets(1, rtv, ds->dsv);
    }
}

void set_camera(dx11renderer* renderer, camera* cam, unsigned slot) {
    upload_bufferdata(renderer, cam->buffer_ref, &cam->gpu_data, camera_gpu_size);

    dx11uniformbuffer* uniform = renderer->uniforms.get(cam->buffer_ref);
    if (uniform != nullptr) {
        renderer->ctx->VSSetConstantBuffers(slot, 1, &uniform->buffer);
    }
}

void set_light(dx11renderer* renderer, point_light* l, unsigned slot) {
    upload_bufferdata(renderer, l->buffer_ref, &l->gpu_data, point_light_gpu_size);

    dx11uniformbuffer* uniform = renderer->uniforms.get(l->buffer_ref);
    if (uniform != nullptr) {
        renderer->ctx->VSSetConstantBuffers(slot, 1, &uniform->buffer);
        renderer->ctx->PSSetConstantBuffers(slot, 1, &uniform->buffer);
    }
}

void set_pipeline(dx11renderer* renderer, ref program_ref) {
    ppl_obj* pipeline = renderer->pipelines.get(program_ref);
    if (pipeline != nullptr) {
        renderer->ctx->VSSetShader(pipeline->vs, nullptr, 0);
        renderer->ctx->PSSetShader(pipeline->ps, nullptr, 0);
        renderer->ctx->IASetInputLayout(pipeline->layout);
        renderer->ctx->PSSetSamplers(0, 1, &pipeline->sampler_state);
    }
}

void set_textures(dx11renderer* renderer, unsigned start_slot, std::vector<ref> tex_refs) {
    std::vector<ID3D11ShaderResourceView*> textures;

    for (ref r : tex_refs) {
        dx11texture2d* texture = renderer->textures2d.get(r);
        if (texture != nullptr) {
            textures.push_back(texture->view);
        }
        else {
            return;
        }

    }

    renderer->ctx->PSSetShaderResources(start_slot, textures.size(), textures.data());
}

void set_texrt(dx11renderer* renderer, unsigned slot, ref target_ref) {
    dx11target* target = renderer->targets.get(target_ref);
    if (target != nullptr) {
        renderer->ctx->PSSetShaderResources(slot, 1, &target->srv);
    }
}

void set_texds(dx11renderer* renderer, unsigned slot, ref ds_ref) {
    dx11depthstencil* ds = renderer->depthstencils.get(ds_ref);
    if (ds != nullptr) {
        renderer->ctx->PSSetShaderResources(slot, 1, &ds->srv);
    }
}


void set_target(dx11renderer* renderer, ref target_ref) {
    dx11target* target = renderer->targets.get(target_ref);
    if (target != nullptr) {
        renderer->ctx->OMSetRenderTargets(1, &target->rtv, nullptr);
    }
}

void set_target(dx11renderer* renderer, ref target_ref, ref depthstencil_ref) {
    dx11target* target = renderer->targets.get(target_ref);
    dx11depthstencil* depthstencil = renderer->depthstencils.get(depthstencil_ref);
    if ((target && depthstencil) != false) {
        renderer->ctx->OMSetRenderTargets(1, &target->rtv, depthstencil->dsv);
    }
}

void set_targets(dx11renderer* renderer, ref target1_ref, ref target2_ref, ref depthstencil_ref) {
    dx11target* target1 = renderer->targets.get(target1_ref);
    dx11target* target2 = renderer->targets.get(target2_ref);
    dx11depthstencil* depthstencil = renderer->depthstencils.get(depthstencil_ref);
    if ((target1 && target2 && depthstencil) != false) {
        ID3D11RenderTargetView* target_list[] = { target1->rtv, target2->rtv };
        renderer->ctx->OMSetRenderTargets(2, target_list, depthstencil->dsv);
    }
}


void set_uniformbuffer(dx11renderer* renderer, ref buffer_ref, unsigned slot) {
    dx11uniformbuffer* uniform = renderer->uniforms.get(buffer_ref);
    if (uniform != nullptr) {
        renderer->ctx->VSSetConstantBuffers(slot, 1, &uniform->buffer);
    }
}

void set_psuniformbuffer(dx11renderer* renderer, ref buffer_ref, unsigned slot) {
    dx11uniformbuffer* uniform = renderer->uniforms.get(buffer_ref);
    if (uniform != nullptr) {
        renderer->ctx->PSSetConstantBuffers(slot, 1, &uniform->buffer);
    }
}

void set_viewport(dx11renderer* renderer, int2 dims) {
    D3D11_VIEWPORT vp = {};
    vp.TopLeftX       = 0.0f;
    vp.TopLeftY       = 0.0f;
    vp.Height         = (float)dims.y;
    vp.Width          = (float)dims.x;
    vp.MinDepth       = 0.0f;
    vp.MaxDepth       = 1.0f;

    renderer->ctx->RSSetViewports(1, &vp);
}

void update_camera(dx11renderer* renderer, camera* cam) {
    upload_bufferdata(renderer, cam->buffer_ref, &cam->gpu_data, camera_gpu_size);
}

void update_light(dx11renderer* renderer, point_light* light) {
    upload_bufferdata(renderer, light->buffer_ref, &light->gpu_data, point_light_gpu_size);
}


void unset_target(dx11renderer* renderer) {
    ID3D11RenderTargetView* rtv = nullptr;
    renderer->ctx->OMSetRenderTargets(1, &rtv, nullptr);
}

void unset_textures(dx11renderer* renderer, unsigned start_slot, unsigned n) {
    std::vector<ID3D11ShaderResourceView*> nulls(n);
    renderer->ctx->PSSetShaderResources(start_slot, n, nulls.data());
}

void upload_bufferdata(dx11renderer* renderer, ref buffer_ref, void* data, unsigned bytes) {
    dx11uniformbuffer* uniform = renderer->uniforms.get(buffer_ref);
    if(uniform != nullptr && bytes <= uniform->bytes) {

        D3D11_MAPPED_SUBRESOURCE mapped_data = {};
        if (SUCCEEDED(renderer->ctx->Map(uniform->buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_data))) {
            memcpy(mapped_data.pData, data, bytes);
            renderer->ctx->Unmap(uniform->buffer, 0);
        }
    }
}

ref upload_mesh(dx11renderer* renderer, const mesh_asm* mesh) {
    
    ID3D11Buffer* vbuffer = nullptr;
    D3D11_BUFFER_DESC vbuffer_desc = {};
    vbuffer_desc.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
    vbuffer_desc.ByteWidth           = mesh->vertex_data.size()*sizeof(float);
    vbuffer_desc.StructureByteStride = 0;
    vbuffer_desc.Usage               = D3D11_USAGE_DEFAULT;
    
    D3D11_SUBRESOURCE_DATA vsub_data = {};
    vsub_data.pSysMem = mesh->vertex_data.data();
    
    
    
    ID3D11Buffer* ibuffer = nullptr;
    D3D11_BUFFER_DESC ibuffer_desc = {};
    ibuffer_desc.BindFlags           = D3D11_BIND_INDEX_BUFFER;
    ibuffer_desc.ByteWidth           = mesh->index_data.size()*sizeof(unsigned);
    ibuffer_desc.StructureByteStride = 0;
    ibuffer_desc.Usage               = D3D11_USAGE_DEFAULT;
    
    D3D11_SUBRESOURCE_DATA isub_data = {};
    isub_data.pSysMem = mesh->index_data.data();

    ref r = renderer->meshes.make();
    dx11mesh* dx_mesh = renderer->meshes.get(r);
    dx_mesh->vertex_stride = mesh->vertex_data.size()/mesh->nvertices * sizeof(float);
    dx_mesh->num_indices = mesh->nindices;

    if (FAILED(renderer->dev->CreateBuffer(&vbuffer_desc, &vsub_data, &dx_mesh->vertex_buffer)) ||
        FAILED(renderer->dev->CreateBuffer(&ibuffer_desc, &isub_data, &dx_mesh->index_buffer))) {
        renderer->meshes.release(r);
        return invalid_ref;
    }

    return r;
}

ref upload_pipelinestate(dx11renderer* renderer, pipeline_state* state) {
    ID3D11VertexShader* vs = nullptr;
    ID3D11PixelShader*  ps = nullptr;
    if (state == nullptr ||
        FAILED(renderer->dev->CreateVertexShader(state->vs->GetBufferPointer(), state->vs->GetBufferSize(), nullptr, &vs)) ||
        FAILED(renderer->dev->CreatePixelShader(state->ps->GetBufferPointer(), state->ps->GetBufferSize(), nullptr, &ps))) {
        return  invalid_ref;
    }

    ID3D11InputLayout* input_layout;
    if (FAILED(renderer->dev->CreateInputLayout(
        state->input_layout.data(),
        state->input_layout.size(),
        state->vs->GetBufferPointer(),
        state->vs->GetBufferSize(),
        &input_layout))) {
        return  invalid_ref;
    }

    ref r = renderer->pipelines.make();
    *renderer->pipelines.get(r) = {
        vs,
        ps,
        input_layout,
        state->sampler_type == SAMPLER_TYPE::LINEAR ? renderer->bilinear_sampler : nullptr
    };

    return r;
}

ref upload_texalbedo(dx11renderer* renderer, stbi_uc* image, int2 dims) {

    D3D11_TEXTURE2D_DESC desc = {};
    desc.ArraySize          = 1;
    desc.BindFlags          = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
    desc.Format             = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    desc.Height             = dims.y;
    desc.MipLevels          = 1;
    desc.SampleDesc.Count   = 1;
    desc.Usage              = D3D11_USAGE_DEFAULT;
    desc.Width              = dims.x;
    desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

    ID3D11Texture2D* resource;
    if (FAILED(renderer->dev->CreateTexture2D(&desc, nullptr, &resource))) {
        return invalid_ref;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC view_desc = {};
    view_desc.Format              = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    view_desc.Texture2D.MipLevels = 1;
    view_desc.ViewDimension       = D3D11_SRV_DIMENSION_TEXTURE2D;

    ID3D11ShaderResourceView* view;
    if (FAILED(renderer->dev->CreateShaderResourceView(resource, &view_desc, &view))) {
        resource->Release();
        return invalid_ref;
    }

    renderer->ctx->UpdateSubresource(resource, 0, nullptr, image, dims.x * 4 * sizeof(stbi_uc), 1);
    renderer->ctx->GenerateMips(view);

    ref r = renderer->textures2d.make();
    renderer->textures2d.get(r)->resource = resource;
    renderer->textures2d.get(r)->view = view;

    return r;
}

ref upload_texheight(dx11renderer* renderer, float* image, int2 dims) {

    D3D11_TEXTURE2D_DESC desc = {};
    desc.ArraySize          = 1;
    desc.BindFlags          = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
    desc.Format             = DXGI_FORMAT_R32_FLOAT;
    desc.Height             = dims.y;
    desc.MipLevels          = 0;
    desc.SampleDesc.Count   = 1;
    desc.Usage              = D3D11_USAGE_DEFAULT;
    desc.Width              = dims.x;
    desc.MiscFlags          = D3D11_RESOURCE_MISC_GENERATE_MIPS;

    ID3D11Texture2D* resource;
    if (FAILED(renderer->dev->CreateTexture2D(&desc, nullptr, &resource))) {
        return invalid_ref;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC view_desc = {};
    view_desc.Format              = DXGI_FORMAT_R32_FLOAT;
    view_desc.Texture2D.MipLevels = -1;
    view_desc.ViewDimension       = D3D11_SRV_DIMENSION_TEXTURE2D;

    ID3D11ShaderResourceView* view;
    if (FAILED(renderer->dev->CreateShaderResourceView(resource, &view_desc, &view))) {
        resource->Release();
        return invalid_ref;
    }

    renderer->ctx->UpdateSubresource(resource, 0, nullptr, image, dims.x*sizeof(float), 1);
    renderer->ctx->GenerateMips(view);

    ref r = renderer->textures2d.make();
    renderer->textures2d.get(r)->resource = resource;
    renderer->textures2d.get(r)->view = view;

    return r;
}
