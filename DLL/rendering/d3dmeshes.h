#pragma once

#include <vector>
#include "../loaders/obj_loading.h"
#include "reflist.h"
#include "dx11render.h"


std::vector<ref> submit_meshes(dx11renderer* renderer, obj_data* data);
