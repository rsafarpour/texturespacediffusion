#pragma once

#include <dxgi.h>
#include <vector>

struct vertex_desc {
    std::vector<DXGI_FORMAT> data_formats;
    std::vector<const char*> semantics;
};
