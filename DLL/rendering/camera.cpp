#include "camera.h"
#include "dx11render.h"

camera create_camera(dx11renderer* r) {
    return {
        create_uniformbuffer(r, 2 * sizeof(float4x4)),
        { 0.0f, 0.0f,  0.0f },
        { 0.0f, 1.0f,  0.0f },
        { 0.0f, 0.0f, -1.0f },
        {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
        },
        {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
        }
    };
}

void destroy_camera(dx11renderer* r, camera* cam) {
    destroy_uniformbuffer(r, cam->buffer_ref);
}

void point_at(camera* c, float3 p, float3 up) {
    c->up = up;
    c->look_at = normalised(p - c->position);

    float4x4 view_transposed = c->view = ViewMatrix(c->position, c->look_at, c->up);
}

void place_at(camera* c, float3 p) {
    c->position = p;

    c->view = ViewMatrix(c->position, c->look_at, c->up);
}

void place_at(camera* c, float radius, float elevation, float azimuth) {
    
    float3 p = {
        radius * cos(azimuth)*cos(elevation),
        radius * sin(elevation),
        radius * sin(azimuth)*cos(elevation)
    };

    place_at(c, p);
}

void perspective(camera* c, float ratio, radians fovy, float near_plane, float far_plane) {
    float4x4 projection_transposed = ProjectionMatrixRad(fovy, ratio, near_plane, far_plane);
    c->projection = transposed(&projection_transposed);
}