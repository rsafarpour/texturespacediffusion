#pragma once

#include "dx11render.h"  

using ms_t = float;
using timestamp_t = UINT64;

struct gpu_profiler {
    UINT64               freq;
    char                 current;
    ID3D11Device*        dev;
    ID3D11DeviceContext* ctx;
    ID3D11Query*         disjoint_queries[2];
};

struct gpu_capture {
    char         current;
    ID3D11Query* timestamp_queries[2];
};

gpu_profiler create_gprofiler(dx11renderer* r);
void         destroy_gprofiler(gpu_profiler* p);

void        gprofile_start(gpu_profiler* p);
void        gprofile_end(gpu_profiler* p);
gpu_capture gprofile_createcapture(gpu_profiler* p);
void        gprofile_capture(gpu_profiler* p, gpu_capture* c);
void        gprofile_destroycapture(gpu_capture* c);
bool        gprofile_validateresults(gpu_profiler* p);
timestamp_t gprofile_resolve(gpu_profiler* p, gpu_capture* c);
ms_t        gprofile_time(gpu_profiler* p, gpu_capture* start, gpu_capture* end);

