#include "scene.h"

#include "../conf.h"

constexpr float PI = 3.141592653f;
constexpr float shadowmap_ratio = float(LIGHTMAP_DIMS) / float(LIGHTMAP_DIMS);

void create_scene(dx11renderer* r, int2 backbuffer_dims, scene_t* scene) {

    // Setting profile values
    // ----------------- R channel ------------------ G channel ------------------ B channel ------------------ Variance---------
    scene->profile[ 0] = 0.233f; scene->profile[ 1] = 0.455f; scene->profile[ 2] = 0.649f; scene->profile[ 3] = 0.0001f;
    scene->profile[ 4] = 0.100f; scene->profile[ 5] = 0.336f; scene->profile[ 6] = 0.344f; scene->profile[ 7] = 0.0484f - 0.0001f;
    scene->profile[ 8] = 0.118f; scene->profile[ 9] = 0.198f; scene->profile[10] = 0.000f; scene->profile[11] = 0.1870f - 0.0484f;
    scene->profile[12] = 0.113f; scene->profile[13] = 0.007f; scene->profile[14] = 0.007f; scene->profile[15] = 0.5670f - 0.1870f;
    scene->profile[16] = 0.358f; scene->profile[17] = 0.004f; scene->profile[18] = 0.000f; scene->profile[19] = 1.9900f - 0.5670f;
    scene->profile[20] = 0.078f; scene->profile[21] = 0.000f; scene->profile[22] = 0.000f; scene->profile[23] = 7.4100f - 1.9900f;

    // Initialize shader uniforms
    scene->cam_elevation       = 0.0f;
    scene->cam_azimuth         = PI / 2.0f;
    scene->light_elevation     = 0.7535f;
    scene->light_azimuth       = PI / 2.0;
    scene->ambient_radiance    = { 0.11f, 0.11f, 0.11f };
    scene->pointlight_radiance = { 1.7f, 1.7f, 1.7f };
    scene->params              = { 45.0f };
    
    // Create and bind shader uniforms
    scene->ambient_buffer = create_uniformbuffer(r, sizeof(float3));
    scene->profile_buffer = create_uniformbuffer(r, sizeof(scene_t::profile));
    scene->param_buffer   = create_uniformbuffer(r, sizeof(sparams_t));
    upload_bufferdata(r, scene->ambient_buffer, &scene->ambient_radiance, sizeof(scene->ambient_radiance));
    upload_bufferdata(r, scene->profile_buffer, scene->profile, sizeof(scene->profile));
    upload_bufferdata(r, scene->param_buffer, &scene->params, sizeof(sparams_t));
    
    scene->cam = create_camera(r);
    place_at(&scene->cam, DISTANCE_CAMERA, scene->cam_elevation, scene->cam_azimuth);
    point_at(&scene->cam, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f });

    float aspect_ratio = float(backbuffer_dims.x) / float(backbuffer_dims.y);
    perspective(&scene->cam, aspect_ratio, PI/4.0, 11.0f, 45.0f);
    
    scene->light = create_pointlight(r);
    set_perspective(&scene->light, radians(75.0f / 180.0f * 3.14159), shadowmap_ratio, 3.5f, 80.0f);
    set_position(&scene->light, DISTANCE_LIGHT, scene->light_elevation, scene->light_azimuth);
    set_radiance(&scene->light, scene->pointlight_radiance);
    
    // Set resources with fixed bind point
    set_camera(r, &scene->cam, 0);
    set_light(r, &scene->light, 1);
    set_textures(r, 0, { scene->mparams.front().ref_albedomap });
    set_textures(r, 1, { scene->mparams.front().ref_normalmap });
    set_psuniformbuffer(r, scene->ambient_buffer, 3);
    set_psuniformbuffer(r, scene->profile_buffer, 2);
    set_psuniformbuffer(r, scene->param_buffer, 5);
}

void destroy_scene(dx11renderer* r, scene_t* scene) {
    
    for (ref mref : scene->mesh_refs) {
        destroy_mesh(r, mref);
    }
    
    destroy_texture(r, scene->mparams.front().ref_albedomap);
    destroy_texture(r, scene->mparams.front().ref_normalmap);

    destroy_pointlight(r, &scene->light);
    destroy_camera(r, &scene->cam);
    destroy_uniformbuffer(r, scene->param_buffer);
    destroy_uniformbuffer(r, scene->profile_buffer);
    destroy_uniformbuffer(r, scene->ambient_buffer);
}
