#pragma once

#include "../matrixtypes.h"
#include "camera.h"
#include "reflist.h"

struct dx11renderer;

struct point_light {
    ref      buffer_ref;
    union { float4x4 view; char gpu_data; };
    float4x4 projection;
    float3   position; float pad0;
    float4   radiance;
};

const unsigned point_light_gpu_size = 2*sizeof(float4x4) + 2*sizeof(float4);

point_light create_pointlight(dx11renderer* r);
void destroy_pointlight(dx11renderer* r, point_light* l);
void set_radiance(point_light* l, float3 L_o);
void set_position(point_light* l, float3 p);
void set_position(point_light* l, float radius, float elevation, float azimuth);
void set_perspective(point_light* l, float fovy, float aspect_ratio, float near_plane, float far_plane);
