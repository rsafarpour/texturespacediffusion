#include "dx11profile.h"

gpu_profiler create_gprofiler(dx11renderer* r) {
    gpu_profiler p = {};

    D3D11_QUERY_DESC disjoint_desc = {};
    disjoint_desc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
    r->dev->CreateQuery(&disjoint_desc, &p.disjoint_queries[0]);
    r->dev->CreateQuery(&disjoint_desc, &p.disjoint_queries[1]);

    p.dev = r->dev;
    p.ctx = r->ctx;
    p.current = 0;
    return p;
}

void destroy_gprofiler(gpu_profiler* p) {
    p->disjoint_queries[0]->Release();
    p->disjoint_queries[1]->Release();
}

void gprofile_start(gpu_profiler* p) {
    p->ctx->Begin(p->disjoint_queries[p->current]);
}

void gprofile_end(gpu_profiler*p) {
    p->ctx->End(p->disjoint_queries[p->current]);
    p->current ^= 0x01;
}

gpu_capture gprofile_createcapture(gpu_profiler* p) {
    gpu_capture cap = {};

    D3D11_QUERY_DESC timestamp_desc = {};
    timestamp_desc.Query = D3D11_QUERY_TIMESTAMP;

    p->dev->CreateQuery(&timestamp_desc, &cap.timestamp_queries[0]);
    p->dev->CreateQuery(&timestamp_desc, &cap.timestamp_queries[1]);
    return cap;
}

void gprofile_capture(gpu_profiler* p, gpu_capture* c) {
    p->ctx->End(c->timestamp_queries[c->current]);
    c->current ^= 0x01;
}

void gprofile_destroycapture(gpu_capture* c) {
    c->timestamp_queries[0]->Release();
    c->timestamp_queries[1]->Release();
}

bool gprofile_validateresults(gpu_profiler* p) {
    while (p->ctx->GetData(p->disjoint_queries[p->current ^ 0x01], nullptr, 0, 0) == S_FALSE) {
        Sleep(1);
    }

    D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjoint_data;
    p->ctx->GetData(p->disjoint_queries[p->current ^ 0x01], &disjoint_data, sizeof(disjoint_data), 0);

    if (!disjoint_data.Disjoint) {
        p->freq = disjoint_data.Frequency;
        return true;
    }

    return false;
}

timestamp_t gprofile_resolve(gpu_profiler* p, gpu_capture* c) {
    timestamp_t timestamp;
    p->ctx->GetData(c->timestamp_queries[c->current ^ 0x01], &timestamp, sizeof(timestamp), 0);

    return timestamp;
}

ms_t gprofile_time(gpu_profiler* p, gpu_capture* start, gpu_capture* end) {
    timestamp_t t0 = gprofile_resolve(p, start);
    timestamp_t t1 = gprofile_resolve(p, end);
    UINT64 time_diff = t1 - t0;

    return float(time_diff)/float(p->freq)*1000.0f;
}
