#pragma once
#include "../loaders/obj_loading.h"
#include "dx11render.h"
#include "reflist.h"
#include <vector>

struct material_params {
    ref ref_albedomap;
    ref ref_normalmap;
};

std::vector<material_params> gen_materials(dx11renderer* renderer, obj_data* data);