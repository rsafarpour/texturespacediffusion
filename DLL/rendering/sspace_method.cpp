#include "../conf.h"
#include "sspace_method.h"
#include "passes/ss_finalpass.h"

struct inv_proj_t {
    float4x4 inv_cameraproj;
    float4x4 inv_lightproj;
};

void create_sspacemethod(dx11renderer* r, int2 backbuffer_dims, scene_t* scene, const vertex_desc* desc, sspace_method* method) {

    method->backbuffer_dims = backbuffer_dims;

    // Create individual passes
    method->shadowpass_pipeline = compile_from_file(*desc, "./shaders/ss_shadowpass.vs",  "./shaders/ss_shadowpass.ps", SAMPLER_TYPE::LINEAR);
    method->prepass_pipeline    = compile_from_file(*desc, "./shaders/ss_prepass.vs",     "./shaders/ss_prepass.ps",    SAMPLER_TYPE::LINEAR);
    method->maskpass_pipeline   = compile_from_file(*desc, "./shaders/ss_maskpass.vs",    "./shaders/ss_maskpass.ps",   SAMPLER_TYPE::LINEAR);
    method->finalpass_pipeline  = compile_from_file(*desc, "./shaders/fullscreen.vs",     "./shaders/ss_finalpass.ps",  SAMPLER_TYPE::LINEAR);
    method->shadow_ppl = upload_pipelinestate(r, &method->shadowpass_pipeline);
    method->pre_ppl    = upload_pipelinestate(r, &method->prepass_pipeline);
    method->mask_ppl   = upload_pipelinestate(r, &method->maskpass_pipeline);
    method->final_ppl  = upload_pipelinestate(r, &method->finalpass_pipeline);
    
   method->sp = create_ssshadowpass(r, &scene->light, scene->mesh_refs.front(), method->shadow_ppl);
    method->pp = create_prepass(r, backbuffer_dims, scene->mesh_refs.front(), method->pre_ppl, &scene->cam, &scene->light, method->sp.ds_zbuffer);
    method->mp = create_maskpass(r, backbuffer_dims, scene->mesh_refs.front(), method->mask_ppl, &scene->cam);
    method->fp = create_finalpass(r, method, scene, method->pp.ds_mask);
    
    // Bind common resources
    method->params_buffer = create_uniformbuffer(r, sizeof(ssparams_t));
    upload_bufferdata(r, method->params_buffer, &method->params, sizeof(ssparams_t));
    set_psuniformbuffer(r, method->params_buffer, 4);


    // Update inverse projections of camera and point light
    // to reconstruct linear depth/light distance
    inv_proj_t inv_proj;
    inv_proj.inv_cameraproj = scene->cam.projection;
    inv_proj.inv_lightproj = scene->light.projection;
    invert(inv_proj.inv_cameraproj);
    invert(inv_proj.inv_lightproj);

    method->invproj_buffer = create_uniformbuffer(r, sizeof(inv_proj_t));
    upload_bufferdata(r, method->invproj_buffer, &inv_proj, sizeof(inv_proj));
    set_psuniformbuffer(r, method->invproj_buffer, 0);

    // Create profiling devices
    method->p             = create_gprofiler(r);
    method->begin         = gprofile_createcapture(&method->p);
    method->end           = gprofile_createcapture(&method->p);
    method->avg_frametime = 0.0f;
    method->n_samples     = 0;
}

void destroy_sspacemethod(dx11renderer* r, sspace_method* method) {
    gprofile_destroycapture(&method->end);
    gprofile_destroycapture(&method->begin);
    destroy_gprofiler(&method->p);

    destroy_uniformbuffer(r, method->invproj_buffer);
    destroy_uniformbuffer(r, method->params_buffer);

    destroy_finalpass(r, &method->fp);
    destroy_maskpass(r, &method->mp);
    destroy_prepass(r, &method->pp);
    destroy_ssshadowpass(r, &method->sp);

    destroy_pipelinestate(r, method->final_ppl);
    destroy_pipelinestate(r, method->mask_ppl);
    destroy_pipelinestate(r, method->pre_ppl);
    destroy_pipelinestate(r, method->shadow_ppl);
}

void render_sspacemethod(dx11renderer* r, sspace_method* method) {

    gprofile_start(&method->p);
    gprofile_capture(&method->p, &method->begin);

    exec_ssshadowpass(r, &method->sp);
    
    set_viewport(r, method->backbuffer_dims);
    exec_prepass(r, &method->pp);
    exec_maskpass(r, &method->mp);
    exec_finalpass(r, &method->fp);

    gprofile_capture(&method->p, &method->end);
    gprofile_end(&method->p);

    if (gprofile_validateresults(&method->p)) {
        ms_t time = gprofile_time(&method->p, &method->begin, &method->end);

        float sum_frametime = method->avg_frametime * float(method->n_samples) + time;
        method->n_samples++;

        method->avg_frametime = sum_frametime/float(method->n_samples);
    }

}