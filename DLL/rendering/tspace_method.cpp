#include "tspace_method.h"
#include "../conf.h"

void create_tspacemethod(dx11renderer* r, int2 backbuffer_dims, scene_t* scene, const vertex_desc* desc, tspace_method* method) {

    method->backbuffer_dims = backbuffer_dims;

    method->shadow_pipeline      = compile_from_file(*desc, "./shaders/ts_shadowpass.vs",     "./shaders/ts_shadowpass.ps",     SAMPLER_TYPE::LINEAR);
    method->stretch_pipeline     = compile_from_file(*desc, "./shaders/ts_stretchtexture.vs", "./shaders/ts_stretchtexture.ps", SAMPLER_TYPE::LINEAR);
    method->diffusepass_pipeline = compile_from_file(*desc, "./shaders/ts_diffusepass.vs",    "./shaders/ts_diffusepass.ps",    SAMPLER_TYPE::LINEAR);
    method->blurH_pipeline       = compile_from_file(*desc, "./shaders/fullscreen.vs",        "./shaders/ts_blurH.ps",          SAMPLER_TYPE::LINEAR);
    method->blurV_pipeline       = compile_from_file(*desc, "./shaders/fullscreen.vs",        "./shaders/ts_blurV.ps",          SAMPLER_TYPE::LINEAR);
    method->finalpass_pipeline   = compile_from_file(*desc, "./shaders/ts_finalpass.vs",      "./shaders/ts_finalpass.ps",      SAMPLER_TYPE::LINEAR);
    method->shadow_ppl    = upload_pipelinestate(r, &method->shadow_pipeline);
    method->stretch_ppl   = upload_pipelinestate(r, &method->stretch_pipeline);
    method->diffuse_ppl   = upload_pipelinestate(r, &method->diffusepass_pipeline);
    method->blurH_ppl     = upload_pipelinestate(r, &method->blurH_pipeline);
    method->blurV_ppl     = upload_pipelinestate(r, &method->blurV_pipeline);
    method->finalpass_ppl = upload_pipelinestate(r, &method->finalpass_pipeline);

    method->rt_tmp1 = create_target(r, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, DXGI_FORMAT_R16G16B16A16_FLOAT);
    method->rt_tmp2 = create_target(r, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, DXGI_FORMAT_R16G16B16A16_FLOAT);
    method->shp = create_tsshadowpass(r, &scene->light, scene->mesh_refs.front(), method->shadow_ppl, { LIGHTMAP_DIMS, LIGHTMAP_DIMS });
    method->sp  = create_stretchpass(r, method->stretch_ppl, scene->mesh_refs.front(), { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS });
    method->dp  = create_diffusepass(r, method->diffuse_ppl, &scene->light, scene->mesh_refs.front(), method->shp.depth_stencil, method->shp.rt_shadowtexdepth, method->shp.rt_shadownormals, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS });
    method->bp1 = create_blurpass(r, scene->profile[ 7], method->rt_tmp1, method->rt_tmp2, method->dp.tex_output, method->sp.tex_output, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, method->blurH_ppl, method->blurV_ppl);
    method->bp2 = create_blurpass(r, scene->profile[11], method->rt_tmp1, method->rt_tmp2, method->bp1.tex_convolution, method->sp.tex_output, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, method->blurH_ppl, method->blurV_ppl);
    method->bp3 = create_blurpass(r, scene->profile[15], method->rt_tmp1, method->rt_tmp2, method->bp2.tex_convolution, method->sp.tex_output, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, method->blurH_ppl, method->blurV_ppl);
    method->bp4 = create_blurpass(r, scene->profile[19], method->rt_tmp1, method->rt_tmp2, method->bp3.tex_convolution, method->sp.tex_output, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, method->blurH_ppl, method->blurV_ppl);
    method->bp5 = create_blurpass(r, scene->profile[24], method->rt_tmp1, method->rt_tmp2, method->bp4.tex_convolution, method->sp.tex_output, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS }, method->blurH_ppl, method->blurV_ppl);
    method->fp  = create_finalpass(r, &scene->cam, scene->mesh_refs.front(), method->finalpass_ppl, scene->profile_buffer, method->shp.depth_stencil, method->shp.rt_shadowtexdepth, &scene->light, {
        method->dp.tex_output,  method->bp1.tex_convolution,  method->bp2.tex_convolution,  method->bp3.tex_convolution,  method->bp4.tex_convolution,  method->bp5.tex_convolution });


    method->scales_ref = create_uniformbuffer(r, sizeof(tspace_method::scales));
    upload_bufferdata(r, method->scales_ref, &method->scales, sizeof(tspace_method::scales));
    set_psuniformbuffer(r, method->scales_ref, 0);

    // Create profiling devices
    method->p             = create_gprofiler(r);
    method->begin         = gprofile_createcapture(&method->p);
    method->end           = gprofile_createcapture(&method->p);
    method->avg_frametime = 0.0f;
    method->n_samples     = 0;
}

void destroy_tspacemethod(dx11renderer* r, tspace_method* method) {

    gprofile_destroycapture(&method->end);
    gprofile_destroycapture(&method->begin);
    destroy_gprofiler(&method->p);

    destroy_uniformbuffer(r, method->scales_ref);

    destroy_finalpass(r, &method->fp);
    destroy_blurpass(r, &method->bp5);
    destroy_blurpass(r, &method->bp4);
    destroy_blurpass(r, &method->bp3);
    destroy_blurpass(r, &method->bp2);
    destroy_blurpass(r, &method->bp1);
    destroy_diffusepass(r, &method->dp);
    destroy_stretchpass(r, &method->sp);
    destroy_tsshadowpass(r, &method->shp);

    destroy_target(r, method->rt_tmp2);
    destroy_target(r, method->rt_tmp1);

    destroy_pipelinestate(r, method->finalpass_ppl);
    destroy_pipelinestate(r, method->blurV_ppl);
    destroy_pipelinestate(r, method->blurH_ppl);
    destroy_pipelinestate(r, method->diffuse_ppl);
    destroy_pipelinestate(r, method->stretch_ppl);
    destroy_pipelinestate(r, method->shadow_ppl);

}

void render_tspacemethod(dx11renderer* r, tspace_method* method) {

    gprofile_start(&method->p);
    gprofile_capture(&method->p, &method->begin);
    clear_backbuffer(r);

    set_viewport(r, { LIGHTMAP_DIMS, LIGHTMAP_DIMS });
    exec_tsshadowpass(r, &method->shp);

    set_viewport(r, { IRRADIANCEMAP_DIMS, IRRADIANCEMAP_DIMS });
    depthtest_off(r);
    exec_stretchpass(r, &method->sp);
    exec_diffusepass(r, &method->dp);
    exec_blurpass(r, &method->bp1);
    exec_blurpass(r, &method->bp2);
    exec_blurpass(r, &method->bp3);
    exec_blurpass(r, &method->bp4);
    exec_blurpass(r, &method->bp5);

    depthtest_on(r);
    set_viewport(r, method->backbuffer_dims);

    exec_finalpass(r, &method->fp);
    present_target(r);
    gprofile_capture(&method->p, &method->end);
    gprofile_end(&method->p);

    if (gprofile_validateresults(&method->p)) {
        ms_t time = gprofile_time(&method->p, &method->begin, &method->end);
        
        float sum_frametime = method->avg_frametime * float(method->n_samples) + time;
        method->n_samples++;

        method->avg_frametime = time;// sum_frametime / float(method->n_samples);
    }

}