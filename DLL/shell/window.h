#pragma once
#include <Windows.h>

using fn_wheel = void(*)();

HWND open_window(LPCWSTR title, int width, int height);
void close_window(HWND window);

void set_wheelfunctions(fn_wheel up, fn_wheel down);