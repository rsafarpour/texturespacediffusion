
float3 lightmap_uvdepth(float4 coordsLS) {
    float3 lmpos = coordsLS.xyz / coordsLS.w;
    float3 uvd = float3((lmpos.xy + 1.0) / 2.0f, lmpos.z);
    uvd.y = 1.0 - uvd.y;

    return uvd;
}

// Returns shadow coefficient, i.e. 0.0 if in full 
// shadow and 1.0 if in full light. Intermediate values 
// are determined by a filter region using PCF
float shadow_sample(Texture2D<float2> lightmap, SamplerComparisonState compare, float4 coordsLS) {
    float3 uvd = lightmap_uvdepth(coordsLS);

    float width, height;
    lightmap.GetDimensions(width, height);

    float2 tex_dims = 1.0f / float2(width, height);

    // Using hardware PCF with a 3x3 area
    float2 taps[9];
    taps[0] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    taps[1] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y);
    taps[2] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    taps[3] = float2(uvd.x, uvd.y - 1.0*tex_dims.y);
    taps[4] = float2(uvd.x, uvd.y);
    taps[5] = float2(uvd.x, uvd.y + 1.0*tex_dims.y);
    taps[6] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    taps[7] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y);
    taps[8] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    
    float pcf_coeff = 0.0;
    pcf_coeff += lightmap.SampleCmp(compare, taps[0], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[1], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[2], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[3], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[4], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[5], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[6], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[7], uvd.z);
    pcf_coeff += lightmap.SampleCmp(compare, taps[8], uvd.z);
    return pcf_coeff/9.0;

    //// Using hardware PCF with a 5x5 area
    //float2 taps[25];
    //taps[ 0] = float2(uvd.x - 2.0f*tex_dims.x, uvd.y - 2.0*tex_dims.y);
    //taps[ 1] = float2(uvd.x - 2.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    //taps[ 2] = float2(uvd.x - 2.0f*tex_dims.x, uvd.y);
    //taps[ 3] = float2(uvd.x - 2.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    //taps[ 4] = float2(uvd.x - 2.0f*tex_dims.x, uvd.y + 2.0*tex_dims.y);
    //taps[ 5] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y - 2.0*tex_dims.y);
    //taps[ 6] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    //taps[ 7] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y);
    //taps[ 8] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    //taps[ 9] = float2(uvd.x - 1.0f*tex_dims.x, uvd.y + 2.0*tex_dims.y);
    //taps[10] = float2(uvd.x, uvd.y - 2.0*tex_dims.y);
    //taps[11] = float2(uvd.x, uvd.y - 1.0*tex_dims.y);
    //taps[12] = float2(uvd.x, uvd.y);
    //taps[13] = float2(uvd.x, uvd.y + 1.0*tex_dims.y);
    //taps[14] = float2(uvd.x, uvd.y + 2.0*tex_dims.y);
    //taps[15] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y - 2.0*tex_dims.y);
    //taps[16] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    //taps[17] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y);
    //taps[18] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    //taps[19] = float2(uvd.x + 1.0f*tex_dims.x, uvd.y + 2.0*tex_dims.y);
    //taps[20] = float2(uvd.x + 2.0f*tex_dims.x, uvd.y - 2.0*tex_dims.y);
    //taps[21] = float2(uvd.x + 2.0f*tex_dims.x, uvd.y - 1.0*tex_dims.y);
    //taps[22] = float2(uvd.x + 2.0f*tex_dims.x, uvd.y);
    //taps[23] = float2(uvd.x + 2.0f*tex_dims.x, uvd.y + 1.0*tex_dims.y);
    //taps[24] = float2(uvd.x + 2.0f*tex_dims.x, uvd.y + 2.0*tex_dims.y);
    //
    //float pcf_coeffs[25];
    //pcf_coeffs[ 0] = lightmap.SampleCmp(compare, taps[ 0], uvd.z).x;
    //pcf_coeffs[ 1] = lightmap.SampleCmp(compare, taps[ 1], uvd.z).x;
    //pcf_coeffs[ 2] = lightmap.SampleCmp(compare, taps[ 2], uvd.z).x;
    //pcf_coeffs[ 3] = lightmap.SampleCmp(compare, taps[ 3], uvd.z).x;
    //pcf_coeffs[ 4] = lightmap.SampleCmp(compare, taps[ 4], uvd.z).x;
    //pcf_coeffs[ 5] = lightmap.SampleCmp(compare, taps[ 5], uvd.z).x;
    //pcf_coeffs[ 6] = lightmap.SampleCmp(compare, taps[ 6], uvd.z).x;
    //pcf_coeffs[ 7] = lightmap.SampleCmp(compare, taps[ 7], uvd.z).x;
    //pcf_coeffs[ 8] = lightmap.SampleCmp(compare, taps[ 8], uvd.z).x;
    //pcf_coeffs[ 9] = lightmap.SampleCmp(compare, taps[ 9], uvd.z).x;
    //pcf_coeffs[10] = lightmap.SampleCmp(compare, taps[10], uvd.z).x;
    //pcf_coeffs[11] = lightmap.SampleCmp(compare, taps[11], uvd.z).x;
    //pcf_coeffs[12] = lightmap.SampleCmp(compare, taps[12], uvd.z).x;
    //pcf_coeffs[13] = lightmap.SampleCmp(compare, taps[13], uvd.z).x;
    //pcf_coeffs[14] = lightmap.SampleCmp(compare, taps[14], uvd.z).x;
    //pcf_coeffs[15] = lightmap.SampleCmp(compare, taps[15], uvd.z).x;
    //pcf_coeffs[16] = lightmap.SampleCmp(compare, taps[16], uvd.z).x;
    //pcf_coeffs[17] = lightmap.SampleCmp(compare, taps[17], uvd.z).x;
    //pcf_coeffs[18] = lightmap.SampleCmp(compare, taps[18], uvd.z).x;
    //pcf_coeffs[19] = lightmap.SampleCmp(compare, taps[19], uvd.z).x;
    //pcf_coeffs[20] = lightmap.SampleCmp(compare, taps[20], uvd.z).x;
    //pcf_coeffs[21] = lightmap.SampleCmp(compare, taps[21], uvd.z).x;
    //pcf_coeffs[22] = lightmap.SampleCmp(compare, taps[22], uvd.z).x;
    //pcf_coeffs[23] = lightmap.SampleCmp(compare, taps[23], uvd.z).x;
    //pcf_coeffs[24] = lightmap.SampleCmp(compare, taps[24], uvd.z).x;
    //
    //float sum = 0.0;
    //sum += pcf_coeffs[ 0];
    //sum += pcf_coeffs[ 1];
    //sum += pcf_coeffs[ 2];
    //sum += pcf_coeffs[ 3];
    //sum += pcf_coeffs[ 4];
    //sum += pcf_coeffs[ 5];
    //sum += pcf_coeffs[ 6];
    //sum += pcf_coeffs[ 7];
    //sum += pcf_coeffs[ 8];
    //sum += pcf_coeffs[ 9];
    //sum += pcf_coeffs[10];
    //sum += pcf_coeffs[11];
    //sum += pcf_coeffs[12];
    //sum += pcf_coeffs[13];
    //sum += pcf_coeffs[14];
    //sum += pcf_coeffs[15];
    //sum += pcf_coeffs[16];
    //sum += pcf_coeffs[17];
    //sum += pcf_coeffs[18];
    //sum += pcf_coeffs[19];
    //sum += pcf_coeffs[20];
    //sum += pcf_coeffs[21];
    //sum += pcf_coeffs[22];
    //sum += pcf_coeffs[23];
    //sum += pcf_coeffs[24];
    //return sum / 25.0;
}

float calc_thickness(Texture2D tsm_normalsdepth, SamplerState linear_sampler, float3 lightposVS, float3 positionVS, float4 positionLS, float3 n, float3 l) {

    // Stores UV in xy- and depth in z-component
    float3 uv_depth   = lightmap_uvdepth(positionLS);
    float4 tsm_value  = tsm_normalsdepth.Sample(linear_sampler, uv_depth.xy);
    float3 tsm_normal = tsm_value.xyz;
    float  tsm_depth  = tsm_value.w;

    float distance_to_light = length(lightposVS - positionVS);
    float thickness_to_light = max(0.0, distance_to_light - tsm_depth);
    float nl = dot(n, l);
    if (nl > 0.0f) {
        thickness_to_light = 50.0f;
    }

    float thickness_corrected = saturate(-nl)*thickness_to_light;
    float backfacing_percentage = saturate(-dot(n, tsm_normal));
    
    float thickness = lerp(thickness_to_light, thickness_corrected, backfacing_percentage);
    return thickness;
}

float3 translucent_radiance(Texture2D tsm, Texture2D E_thickness[6], SamplerState linear_sampler, float4 positionLS, float2 uv, float4 profile[6]) {

    // Stores UV in xy- and depth in z-component
    float3 uv_depth = lightmap_uvdepth(positionLS);

    // Factor 10 converts from cm to mm!
    float2 tsm_tap = tsm.Sample(linear_sampler, uv_depth.xy).xy;
    float thickness = E_thickness[5].Sample(linear_sampler, uv).w*10.0;

    float num = -thickness*thickness;
    float e0 = exp(num/profile[0].w);
    float e1 = exp(num/profile[1].w);
    float e2 = exp(num/profile[2].w);
    float e3 = exp(num/profile[3].w);
    float e4 = exp(num/profile[4].w);
    float e5 = exp(num/profile[5].w);

    float3 L_0 = E_thickness[0].Sample(linear_sampler, tsm_tap).xyz;
    float3 L_1 = E_thickness[1].Sample(linear_sampler, tsm_tap).xyz;
    float3 L_2 = E_thickness[2].Sample(linear_sampler, tsm_tap).xyz;
    float3 L_3 = E_thickness[3].Sample(linear_sampler, tsm_tap).xyz;
    float3 L_4 = E_thickness[4].Sample(linear_sampler, tsm_tap).xyz;
    float3 L_5 = E_thickness[5].Sample(linear_sampler, tsm_tap).xyz;
    float3 weightedL_0 = profile[0].xyz * e0*L_0;
    float3 weightedL_1 = profile[1].xyz * e1*L_1;
    float3 weightedL_2 = profile[2].xyz * e2*L_2;
    float3 weightedL_3 = profile[3].xyz * e3*L_3;
    float3 weightedL_4 = profile[4].xyz * e4*L_4;
    float3 weightedL_5 = profile[5].xyz * e5*L_5;
    float3 total_weight = profile[0].w + profile[1].w + profile[2].w + profile[3].w + profile[4].w + profile[5].w;
    float3 L = (
        weightedL_0 + 
        weightedL_1 + 
        weightedL_2 + 
        weightedL_3 + 
        weightedL_4 + 
        weightedL_5) / total_weight;

    return L;
}