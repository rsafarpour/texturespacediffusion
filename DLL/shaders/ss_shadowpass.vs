cbuffer Light : register(b1) {
    float4x4 light_view;
    float4x4 light_proj;
    float3   light_position;
    float3   light_radiance;
}

struct vs_in {
    float3 position : POSITION;
    float3 normal   : NORMAL;
    float2 uv       : TEXCOORD;
};

float4 main(vs_in i) : SV_POSITION {

    float3 biased_pos = i.position - (0.002*i.normal);
    float4 positionNDC = mul(light_proj, mul(light_view, float4(biased_pos, 1.0)));
    return positionNDC;
}