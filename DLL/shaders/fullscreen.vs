struct vs_out {
    float2 uv       : TEXCOORD;
    float4 position : SV_POSITION;
};

vs_out main(uint id : SV_VertexID) {

    vs_out o;
    o.position = float4(float(id & 0x01)* 4.0 - 1.0, float(id >> 1) * 4.0 - 1.0, 0.0, 1.0);
    o.uv = float2(float(id & 0x01) * 2.0, (1.0 - float(id >> 1)*2.0));
    return o;
}