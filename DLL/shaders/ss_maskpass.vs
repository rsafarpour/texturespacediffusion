cbuffer transforms : register(b0) {
    float4x4 _view;
    float4x4 _proj;
}

struct vs_in {
    float3 position : POSITION;
    float3 normal   : NORMAL;
    float2 uv       : TEXCOORD;
};

struct vs_out {
    float4 positionNDC : SV_POSITION;
};

vs_out main(vs_in i) {
    vs_out o;
    o.positionNDC = mul(_proj, mul(_view, float4(i.position, 1.0)));
    return o;
}