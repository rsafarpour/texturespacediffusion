
#define PI 3.14159f
#define F0 float3(0.028, 0.028, 0.028)

SamplerComparisonState compare : register(s1);

// Diffuse BRDF term using uniform reflection
float3 brdf_lambert(float3 albedo) {
    return albedo/PI;
}

// Specular BRDF term, based on GGX NDF using Beckmann
// shadowing term and Schlick's Fresnel approximation
float3 brdf_spec(float roughness, float3 l, float3 v, float3 n) {
    float3 h = normalize(l + v);

    float nh = clamp(dot(h, n), 0.0, 1.0);
    float vh = clamp(dot(v, h), 0.0, 1.0);
    float nl = abs(dot(l, n));
    float nv = abs(dot(n, v));

    float alpha = roughness * roughness;
    float alpha2 = alpha * alpha;

    float ggx_num = step(0, nh)*alpha2;
    float ggx_sqrt = (1.0 + (nh*nh)*(alpha2 - 1.0));
    float ggx_denom = 3.14159*(ggx_sqrt*ggx_sqrt);

    float3 F_schlick = F0 + (1 - F0)*pow((1 - nl), 5.0);
    float3 D_ggx = ggx_num / ggx_denom;
    float3 G_vcavity = min(1, min(2 * nh*nv / vh, 2 * nh*nl / vh));
    return (F_schlick*D_ggx*G_vcavity) / (4 * nv*nl);
}