cbuffer transforms : register(b0) {
    float4x4 _view;
    float4x4 _proj;
}

cbuffer Light : register(b1) {
    float4x4 light_view;
    float4x4 light_proj;
    float3   light_position;
    float3   light_radiance;
}

struct vs_in {
    float3 position : POSITION;
    float3 normal   : NORMAL;
    float2 uv       : TEXCOORD;
};

struct vs_out {
    float3 normalVS    : NORMAL;
    float3 positionVS  : POSITION_VS;
    float3 lightposVS  : LIGHT_VS;
    float2 uv          : TEXCOORD;
    float4 positionLS  : POSITION_LS;
    float4 positionNDC : SV_POSITION;
};

vs_out main(vs_in i) {
    vs_out o;
    o.positionVS  = mul(_view, float4(i.position, 1.0)).xyz;
    o.lightposVS  = mul(_view, float4(light_position, 1.0)).xyz;
    o.positionLS  = mul(light_proj, mul(light_view, float4(i.position, 1.0)));
    o.normalVS    = mul(_view, float4(i.normal, 0.0)).xyz;
    o.positionNDC = mul(_proj, mul(_view, float4(i.position, 1.0)));
    o.uv          = i.uv;
    return o;
}