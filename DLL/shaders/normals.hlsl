// Reconstructing the tangent frame as described by Schueler 
// in: http://www.thetenthplanet.de/archives/1180
float3x3 cotangent_frame(float3 n, float3 p, float2 uv) {
    float3 dp1 = ddx(p);
    float3 dp2 = ddy(p);
    float2 duv1 = ddx(uv);
    float2 duv2 = ddy(uv);

    float3 dp2perp = cross(dp2, n);
    float3 dp1perp = cross(n, dp1);

    float3 t = (dp2perp*duv1.x) + (dp1perp*duv2.x);
    float3 b = (dp2perp*duv1.y) + (dp1perp*duv2.y);

    float rnorm = rsqrt(max(dot(t, t), dot(b, b)));
    return transpose(float3x3(rnorm*t, rnorm*b, n));
}

// Generating normal from heightmap as described by DICE in:
// http://www.dice.se/wp-content/uploads/2014/12/Chapter5-Andersson-Terrain_Rendering_in_Frostbite.pdf
float3 heightmap_normal(Texture2D<float> heightmap, SamplerState bilinear, float2 uv, float scale) {

    // Query mip maps and highest resolution level dimensions
    uint levels;
    float width, height;
    heightmap.GetDimensions(0, width, height, levels);
    uint max_level = levels - 1;

    // Determine mip map level
    float2 dx = ddx(uv);
    float2 dy = ddy(uv);
    float dF = max(dot(dx, dx), dot(dy, dy));
    float level = max_level - log2(1.0f / sqrt(dF)) - 1.0;

    float2 texture_dims;
    heightmap.GetDimensions(level, texture_dims.x, texture_dims.y, levels);
    float2 texel_dims = 1.0 / texture_dims;

    float l = heightmap.SampleLevel(bilinear, uv - float2(texel_dims.x, 0), level) * scale;
    float r = heightmap.SampleLevel(bilinear, uv + float2(texel_dims.x, 0), level) * scale;
    float t = heightmap.SampleLevel(bilinear, uv - float2(0, texel_dims.y), level) * scale;
    float b = heightmap.SampleLevel(bilinear, uv + float2(0, texel_dims.y), level) * scale;

    return normalize(float3(l - r, t - b, 2.0));
}

float3 final_normal(Texture2D<float> heightmap, SamplerState bilinear, float scale, float2 uv, float3 n, float3 p) {

    float3x3 tbn = cotangent_frame(n, p, uv);
    float3 normalTS = heightmap_normal(heightmap, bilinear, uv, scale);
    return normalize(mul(tbn, normalTS));
}