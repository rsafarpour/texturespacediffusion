#pragma once

#define PAGE_BYTES 4096 

#define IRRADIANCEMAP_DIMS  1*1024
#define LIGHTMAP_DIMS       8*1024

#define DISTANCE_CAMERA 30.0f
#define DISTANCE_LIGHT  20.0f
