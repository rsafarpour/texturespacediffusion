#include "hlsl_loader.h"

void hlsl_load(const char* file_name, std::string* out) {
    fistream fin(file_name);

    out->resize(fin.size);
    read_chunk(&fin, out, (chunksize_t)fin.size);
}