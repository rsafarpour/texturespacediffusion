#pragma once

#include "../filestream.h"
#include <string>

void hlsl_load(const char* file_name, std::string* out);