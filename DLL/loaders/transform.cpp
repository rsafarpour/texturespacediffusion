#include "transform.h"
#include "../matrixtypes.h"

void bounding_box(obj_mesh* mesh, float3* min, float3* max) {
    *min = { FLT_MAX, FLT_MAX, FLT_MAX };
    *max = {-FLT_MAX,-FLT_MAX,-FLT_MAX };

    for (auto& p : mesh->positions) {
        min->x = min->x > p.x ? p.x : min->x;
        min->y = min->y > p.y ? p.y : min->y;
        min->z = min->z > p.z ? p.z : min->z;
        max->x = max->x < p.x ? p.x : max->x;
        max->y = max->y < p.y ? p.y : max->y;
        max->z = max->z < p.z ? p.z : max->z;
    }
}

void scale_mesh(obj_mesh* mesh, float factor) {
    for (auto& p : mesh->positions) {
        p = scale(factor, p);
    }
}

void rotate_mesh(obj_mesh* mesh, float3 axis, float angle_rad) {
    axis = normalised(axis);

    float cos_t = cos(angle_rad);
    float icos_t = 1.0f - cos_t;
    float sin_t = sin(angle_rad);

    float x2 = axis.x*axis.x;
    float y2 = axis.y*axis.y;
    float z2 = axis.z*axis.z;
    float xy = axis.x*axis.y;
    float yz = axis.y*axis.z;
    float xz = axis.x*axis.z;

    float m00 = cos_t + x2*icos_t;
    float m01 = xy*icos_t - axis.z*sin_t;
    float m02 = xz*icos_t + axis.y*sin_t;

    float m10 = xy*icos_t + axis.z*sin_t;
    float m11 = cos_t + y2*icos_t;
    float m12 = yz*icos_t - axis.x*sin_t;

    float m20 = xz*icos_t + axis.y*sin_t;
    float m21 = yz*icos_t + axis.x*sin_t;
    float m22 = cos_t + z2*icos_t;

    float3x3 transform = {
        m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22
    };

    for (auto& p : mesh->positions) {
        p = mul(&transform, &p);
    }

    for (auto& n : mesh->normals) {
        n = mul(&transform, &n);
    }
}

void translate_mesh(obj_mesh* mesh, float3 translation) {
    for (auto& p : mesh->positions) {
        p += translation;
    }
}