#pragma once

#include "obj_files/obj_output.h"

void bounding_box(obj_mesh* mesh, float3* min, float3* max);
void scale_mesh(obj_mesh* mesh, float factor);
void rotate_mesh(obj_mesh* mesh, float3 axis, float angle_rad);
void translate_mesh(obj_mesh* mesh, float3 translation);