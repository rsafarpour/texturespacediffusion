#include "mtl_output.h"

void swap(mtl_attr& a1, mtl_attr& a2) {
    std::swap(a1.id,      a2.id);
    std::swap(a1.value,   a2.value);
    std::swap(a1.options, a2.options);
}

mtl_attr::mtl_attr(mtl_attrid id)
  : id(id),
    value({}) {}

mtl_attr::mtl_attr(mtl_attrid id, float value)
  : id(id) {
    this->value.flt = value;
}

mtl_attr::mtl_attr(mtl_attrid id, float3 value)
    : id(id) {
    this->value.flt3 = value;
}

mtl_attr::mtl_attr(mtl_attrid id, unsigned value)
    : id(id) {
    this->value.uint = value;
}

mtl_attr::mtl_attr(mtl_attrid id, mtl_imfchanid value)
    : id(id) {
    this->value.channel = value;
}

mtl_attr::mtl_attr(mtl_attrid id, const char* str)
    : id(id) {
    this->value.str = strdup(str);
}

mtl_attr::~mtl_attr() {
    if (((unsigned char)this->id & 0x80) != 0 && this->value.str != nullptr) {
        free((char*)this->value.str);
    }
}

mtl_attr::mtl_attr(const mtl_attr& other) {
    this->id = other.id;
    this->options = other.options;
    if (((unsigned char)other.id & 0x80) != 0 && other.value.str != nullptr) {
        this->value.str = strdup(other.value.str);
    }
    else {
        this->value = other.value;
    }
}

mtl_attr::mtl_attr(mtl_attr&& other)
  : id(mtl_attrid::KA)
{
    swap(*this, other);
}

mtl_attr& mtl_attr::operator=(const mtl_attr& other) {
    mtl_attr tmp(other);
    swap(*this, tmp);
    return *this;
}

mtl_attr& mtl_attr::operator=(mtl_attr&& other) {
    mtl_attr tmp(other);
    swap(*this, tmp);
    return *this;
}