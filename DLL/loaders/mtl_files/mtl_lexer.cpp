#include "mtl_lexer.h"
#include "../../conf.h"
#include "../../output.h"
#include "../filestream.h"
#include <cctype>

#define commit_token(token_id) token_stream.push({ token_id, fline }); \
                               state = 0; \
                               if (c == '\n') { fline++; }

#define jump_to_string(str_state) else if (isspace(c)) { state = str_state; continue; }\
                                  else { state = str_state; }

std::queue<mtl_token> mtl_lex(const std::string* file_name)
{
    fistream fin(file_name->c_str());
    std::queue<mtl_token> token_stream;
    std::string token_string;

    size_t fline = 1;
    char chunks[2][PAGE_BYTES];
    size_t size[2];
    fpos_t chunk_pos = 0;
    int state = 0;
    char pr_chunk = 0;
    char rd_chunk = 0;
    size[rd_chunk] = read_chunk(&fin, chunks[rd_chunk], PAGE_BYTES);
    if (size[rd_chunk] < PAGE_BYTES) {
        chunks[rd_chunk][size[rd_chunk]++] = ' ';
    }

    do {
        rd_chunk = (rd_chunk + 1) % 2;
        pr_chunk = (rd_chunk + 1) % 2;
        size[rd_chunk] = read_chunk(&fin, chunks[rd_chunk], PAGE_BYTES);
        if (size[rd_chunk] < PAGE_BYTES) {
            chunks[rd_chunk][size[rd_chunk]++] = ' ';
        }

        chunk_pos = 0;
        char c = -1;
        while (chunk_pos < size[pr_chunk]) {
            char c = chunks[pr_chunk][chunk_pos];
            token_string += c;
            switch (state) {
            case 0:
                token_string.clear();
                token_string += c;
                if (isspace(c)) { if (c == '\n') { fline++; } } // Count lines
                else if (c == '#')   { state = 1; }             // Comment
                else if (c == 'n')   { state = 2; }             // 'newmtl' or string
                else if (c == 'K')   { state = 8; }             // 'K[a|d|s]' or string
                else if (c == 'N')   { state = 12; }            // 'Ns', 'Ni' or string
                else if (c == 'm')   { state = 14; }            // 'map_[K[a|d|s]|d|Ns|bump|aat]' or string
                else if (c == 'T')   { state = 34; }            // 'Tf' or string
                else if (c == 'd')   { state = 36; }            // 'd','decal','disp' or string
                else if (c == 'r')   { state = 41; }            // 'refl' or string
                else if (c == 's')   { state = 46; }            // 'sharpness' or string
                else if (c == '-')   { state = 55; }            // any option starting with '-' or string
                else if (isdigit(c)) { state = 31; }            // float, uint or string
                else if (c == '.')   { state = 32; }            // float or string
                else { state = 33; }                            // string
                break;
            case 1:
                if (c == '\n') { state = 0; fline++; }
                else { state = 1; }
                break;
            case 2:
                if (c == 'e') { state = 3; }
                jump_to_string(33);
                break;
            case 3:
                if (c == 'w') { state = 4; }
                jump_to_string(33);
                break;
            case 4:
                if (c == 'm') { state = 5; }
                jump_to_string(33);
                break;
            case 5:
                if (c == 't') { state = 6; }
                jump_to_string(33);
                break;
            case 6:
                if (c == 'l') { state = 7; }
                jump_to_string(33);
                break;
            case 7:
                if (isspace(c)) { commit_token(mtltoken_type::ID_NEWMTL); }
                jump_to_string(33);
                break;
            case 8:
                if (c == 'a') { state = 9; }
                else if (c == 'd') { state = 10; }
                else if (c == 's') { state = 11; }
                jump_to_string(33);
                break;
            case 9:
                if (isspace(c)) { commit_token(mtltoken_type::ID_KA); }
                jump_to_string(33);
                break;
            case 10:
                if (isspace(c)) { commit_token(mtltoken_type::ID_KD); }
                jump_to_string(33);
                break;
            case 11:
                if (isspace(c)) { commit_token(mtltoken_type::ID_KS); }
                jump_to_string(33);
                break;
            case 12:
                if (c == 's') { state = 13; }
                else if (c == 'i') { state = 45; }
                jump_to_string(33);
                break;
            case 13:
                if (isspace(c)) { commit_token(mtltoken_type::ID_NS); }
                jump_to_string(33);
                break;
            case 14:
                if (c == 'a') { state = 15; }
                jump_to_string(33);
                break;
            case 15:
                if (c == 'p') { state = 16; }
                jump_to_string(33);
                break;
            case 16:
                if (c == '_') { state = 17; }
                else if (c == 'a') { state = 102; }
                jump_to_string(33);
                break;
            case 17:
                if (c == 'K') { state = 18; }
                else if (c == 'd') { state = 20; }
                else if (c == 'N') { state = 22; }
                else if (c == 'b') { state = 24; }
                else if (c == 'a') { state = 28; }
                jump_to_string(33);
                break;
            case 18:
                if (c == 'a') { state = 19; }
                else if (c == 'd') { state = 20; }
                else if (c == 's') { state = 21; }
                jump_to_string(33);
                break;
            case 19:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPKA); }
                jump_to_string(33);
                break;
            case 20:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPKD); }
                jump_to_string(33);
                break;
            case 21:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPKS); }
                jump_to_string(33);
                break;
            case 22:
                if (c == 's') { state = 23; }
                jump_to_string(33);
                break;
            case 23:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPNS); }
                jump_to_string(33);
                break;
            case 24:
                if (c == 'u') { state = 25; }
                jump_to_string(33);
                break;
            case 25:
                if (c == 'm') { state = 26; }
                jump_to_string(33);
                break;
            case 26:
                if (c == 'p') { state = 27; }
                jump_to_string(33);
                break;
            case 27:
                if (isspace(c)) { commit_token(mtltoken_type::ID_BUMP); }
                jump_to_string(33);
                break;
            case 28:
                if (c == 'a') { state = 29; }
                jump_to_string(33);
                break;
            case 29:
                if (c == 't') { state = 30; }
                jump_to_string(33);
                break;
            case 30:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPAAT); }
                jump_to_string(33);
                break;
            case 31:
                if (isdigit(c)) { state = 31; }
                else if (c == '.') { state = 32; }
                else if (isspace(c)) { 
                    commit_token((unsigned)std::strtoul(token_string.c_str(), nullptr, 10));
                }
                jump_to_string(33);
                break;
            case 32:
                if (isdigit(c)) { state = 32; }
                else if (isspace(c)) {
                    commit_token(std::strtof(token_string.c_str(), nullptr));
                }
                jump_to_string(33);
                break;
            case 33:
                if (!isspace(c) && token_string.size() < 1024) { state = 33; }
                else if (isspace(c)) { token_string.pop_back();  commit_token(token_string.c_str()) }
                else { // exceeded 1024 characters for token
                    print_error("A string literal exceeded the limit of 1024 characters in line %i", fline);
                    return {};
                }
                break;
            case 34:
                if (c == 'f') { state = 35; }
                jump_to_string(33);
                break;
            case 35:
                if (isspace(c)) { commit_token(mtltoken_type::ID_TF); }
                jump_to_string(33);
                break;
            case 36:
                if (c == 'e') { state = 37; }
                else if (c == 'i') { state = 99; }
                else if (isspace(c)) { commit_token(mtltoken_type::ID_D); }
                jump_to_string(33);
                break;
            case 37:
                if (c == 'c') { state = 38; }
                jump_to_string(33);
                break;
            case 38:
                if (c == 'a') { state = 39; }
                jump_to_string(33);
                break;
            case 39:
                if (c == 'l') { state = 39; }
                jump_to_string(33);
                break;
            case 40:
                if (isspace(c)) { commit_token(mtltoken_type::ID_DECAL); }
                jump_to_string(33);
                break;
            case 41:
                if (c == 'e') { state = 42; }
                jump_to_string(33);
                break;
            case 42:
                if (c == 'f') { state = 43; }
                jump_to_string(33);
                break;
            case 43:
                if (c == 'l') { state = 44; }
                jump_to_string(33);
                break;
            case 44:
                if (isspace(c)) { commit_token(mtltoken_type::ID_REFL); }
                jump_to_string(33);
                break;
            case 45:
                if (isspace(c)) { commit_token(mtltoken_type::ID_IOR); }
                jump_to_string(33);
                break;
            case 46:
                if ('h') { state = 47; }
                jump_to_string(33);
                break;
            case 47:
                if ('a') { state = 48; }
                jump_to_string(33);
                break;
            case 48:
                if ('r') { state = 49; }
                jump_to_string(33);
                break;
            case 49:
                if ('p') { state = 50; }
                jump_to_string(33);
                break;
            case 50:
                if ('n') { state = 51; }
                jump_to_string(33);
                break;
            case 51:
                if ('e') { state = 52; }
                jump_to_string(33);
                break;
            case 52:
                if ('s') { state = 53; }
                jump_to_string(33);
                break;
            case 53:
                if ('s') { state = 54; }
                jump_to_string(33);
                break;
            case 54:
                if (isspace(c)) { commit_token(mtltoken_type::ID_SHARPNESS); }
                jump_to_string(33);
                break;
            case 55:
                if (c == 'h') { state = 56; }
                else if (c == 'b') { state = 60; }
                else if (c == 'c') { state = 72; }
                else if (c == 'm') { state = 78; }
                else if (c == 'o') { state = 80; }
                else if (c == 's') { state = 81; }
                else if (c == 't') { state = 82; }
                else if (c == 'i') { state = 88; }
                else if (c == 'h') { state = 95; }
                jump_to_string(33);
                break;
            case 56:
                if (c == 'a') { state = 57; }
                jump_to_string(33);
                break;
            case 57:
                if (c == 'l') { state = 58; }
                jump_to_string(33);
                break;
            case 58:
                if (c == 'o') { state = 59; }
                jump_to_string(33);
                break;
            case 59:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_HALO); }
                jump_to_string(33);
                break;
            case 60:
                if (c == 'l') { state = 61; }
                else if (c == 'o') { state = 67; }
                else if (c == 'm') { state = 71; }
                jump_to_string(33);
                break;
            case 61:
                if (c == 'e') { state = 62; }
                jump_to_string(33);
                break;
            case 62:
                if (c == 'n') { state = 63; }
                jump_to_string(33);
                break;
            case 63:
                if (c == 'd') { state = 64; }
                jump_to_string(33);
                break;
            case 64:
                if (c == 'u') { state = 65; }
                else  if (c == 'v') { state = 66; }
                jump_to_string(33);
                break;
            case 65:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_BLENDU); }
                jump_to_string(33);
                break;
            case 66:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_BLENDV); }
                jump_to_string(33);
                break;
            case 67:
                if (c == 'o') { state = 68; }
                jump_to_string(33);
                break;
            case 68:
                if (c == 's') { state = 69; }
                jump_to_string(33);
                break;
            case 69:
                if (c == 't') { state = 70; }
                jump_to_string(33);
                break;
            case 70:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_BOOST); }
                jump_to_string(33);
                break;
            case 71:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_BM); }
                jump_to_string(33);
                break;
            case 72:
                if (c == 'c') { state = 73; }
                if (c == 'l') { state = 74; }
                jump_to_string(33);
                break;
            case 73:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_CC); }
                jump_to_string(33);
                break;
            case 74:
                if (c == 'a') { state = 75; }
                jump_to_string(33);
                break;
            case 75:
                if (c == 'm') { state = 76; }
                jump_to_string(33);
                break;
            case 76:
                if (c == 'p') { state = 77; }
                jump_to_string(33);
                break;
            case 77:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_CLAMP); }
                jump_to_string(33);
                break;
            case 78:
                if (c == 'm') { state = 79; }
                jump_to_string(33);
                break;
            case 79:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_MM); }
                jump_to_string(33);
                break;
            case 80:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_O); }
                jump_to_string(33);
                break;
            case 81:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_S); }
                jump_to_string(33);
                break;
            case 82:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_T); }
                else if (c == 'e') { state = 83; }
                jump_to_string(33);
                break;
            case 83:
                if (c == 'x') { state = 84; }
                jump_to_string(33);
                break;
            case 84:
                if (c == 'r') { state = 85; }
                jump_to_string(33);
                break;
            case 85:
                if (c == 'e') { state = 86; }
                jump_to_string(33);
                break;
            case 86:
                if (c == 's') { state = 87; }
                jump_to_string(33);
                break;
            case 87:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_TEXRES); }
                jump_to_string(33);
                break;
            case 88:
                if (c == 'm') { state = 89; }
                jump_to_string(33);
                break;
            case 89:
                if (c == 'f') { state = 90; }
                jump_to_string(33);
                break;
            case 90:
                if (c == 'c') { state = 91; }
                jump_to_string(33);
                break;
            case 91:
                if (c == 'h') { state = 92; }
                jump_to_string(33);
                break;
            case 92:
                if (c == 'a') { state = 93; }
                jump_to_string(33);
                break;
            case 93:
                if (c == 'n') { state = 94; }
                jump_to_string(33);
                break;
            case 94:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_IMFCHAN); }
                jump_to_string(33);
                break;
            case 95:
                if (c == 'a') { state = 96; }
                jump_to_string(33);
                break;
            case 96:
                if (c == 'l') { state = 97; }
                jump_to_string(33);
                break;
            case 97:
                if (c == 'o') { state = 98; }
                jump_to_string(33);
                break;
            case 98:
                if (isspace(c)) { commit_token(mtltoken_type::OPT_HALO); }
                jump_to_string(33);
                break;
            case 99:
                if (c == 's') { state = 100; }
                jump_to_string(33);
                break;
            case 100:
                if (c == 'p') { state = 101; }
                jump_to_string(33);
                break;
            case 101:
                if (isspace(c)) { commit_token(mtltoken_type::ID_DISP); }
                jump_to_string(33);
                break;
            case 102:
                if (c == 'a') { state = 103; }
                jump_to_string(33);
                break;
            case 103:
                if (c == 't') { state = 104; }
                jump_to_string(33);
                break;
            case 104:
                if (isspace(c)) { commit_token(mtltoken_type::ID_MAPAAT); }
                jump_to_string(33);
            }

            chunk_pos++;
        }
    } while (size[pr_chunk] == PAGE_BYTES);

    token_stream.push({ mtltoken_type::END, fline });
    return token_stream;
}