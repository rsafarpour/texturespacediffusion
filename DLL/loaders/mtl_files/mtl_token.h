#pragma once

#include "../../vectortypes.h"

enum class mtltoken_type {
    ID_NEWMTL,
    ID_KA,
    ID_KD,
    ID_KS,
    ID_NS,
    ID_MAPKA,
    ID_MAPKD,
    ID_MAPKS,
    ID_MAPNS,
    ID_MAPD,
    ID_MAPAAT,
    ID_BUMP,
    ID_DISP,
    ID_D,
    ID_TR,
    ID_ILLUM,
    ID_TF,
    ID_DECAL,
    ID_REFL,
    ID_IOR,
    ID_SHARPNESS,
    VAL_UINT,
    VAL_FLOAT,
    VAL_STR,
    OPT_HALO,
    OPT_BLENDU,
    OPT_BLENDV,
    OPT_BOOST,
    OPT_BM,
    OPT_CC,
    OPT_CLAMP,
    OPT_MM,
    OPT_O,
    OPT_S,
    OPT_T,
    OPT_TEXRES,
    OPT_IMFCHAN,

    END
};

union mtltoken_val {
    float       flt;
    char*       str;
    unsigned    uint;
};

struct mtl_token {
    mtl_token(mtltoken_type ty, size_t line);
    mtl_token(float value, size_t line);
    mtl_token(unsigned value, size_t line);
    mtl_token(const char* value, size_t line);
    mtl_token(const mtl_token& other);
    mtl_token(mtl_token&& other);
    ~mtl_token();

    mtl_token& operator=(const mtl_token& other);
    mtl_token& operator=(mtl_token&& other);

    mtltoken_type type;
    mtltoken_val  value;
    size_t        line;
};