#include "mtl_token.h"

mtl_token::mtl_token(mtltoken_type ty, size_t line) {
    this->type = ty;
    this->line = line;
}

mtl_token::mtl_token(float value, size_t line) {
    this->type = mtltoken_type::VAL_FLOAT;
    this->line = line;
    this->value.flt = value;
}

mtl_token::mtl_token(unsigned value, size_t line) {
    this->type = mtltoken_type::VAL_UINT;
    this->line = line;
    this->value.uint = value;
}

mtl_token::mtl_token(const char* value, size_t line) {
    this->type = mtltoken_type::VAL_STR;
    this->line = line;
    this->value.str = (char*)malloc(strlen(value) + 1);
    strcpy(this->value.str, value);
}

mtl_token::mtl_token(const mtl_token& other) {
    this->type = other.type;
    this->line = other.line;
    if (this->type == mtltoken_type::VAL_STR) {
        this->value.str = (char*)malloc(strlen(other.value.str) + 1);
        strcpy(this->value.str, other.value.str);
    }
    else
    {
        this->value = other.value;
    }
}
mtl_token::mtl_token(mtl_token&& other) 
  : value({})
{
    std::swap(this->type, other.type);
    std::swap(this->line, other.line);
    std::swap(this->value, other.value);
}

mtl_token::~mtl_token() {
    if (this->type == mtltoken_type::VAL_STR &&
        this->value.str != nullptr) {
        free(this->value.str);
    }
}

mtl_token& mtl_token::operator=(const mtl_token& other) { 
    mtl_token tmp(other); 
    std::swap(tmp, *this);
    return *this;
}

mtl_token& mtl_token::operator=(mtl_token&& other) {
    mtl_token tmp(other);
    std::swap(tmp, *this);
    return *this;
}