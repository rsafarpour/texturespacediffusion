#pragma once

#include "../../vectortypes.h"
#include "mtl_token.h"
#include "mtl_output.h"
#include <queue>


void mtl_parse(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib);
