#pragma once

#include "mtl_token.h"
#include <queue>
#include <string>

std::queue<mtl_token> mtl_lex(const std::string* file_name);
