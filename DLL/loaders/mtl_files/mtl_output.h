#pragma once

#include "../../vectortypes.h"
#include <string>
#include <vector>

// The enum value of all mtl attributes with 
// a string value have their MSB set to 1
enum class mtl_attrid : unsigned char {
    KA                  = 0x00,
    KD                  = 0x01,
    KS                  = 0x02,
    NS                  = 0x03,
    DISSOLVE            = 0x04,
    DISPLACEMENT_MAP    = 0x05,
    ILLUM               = 0x06,
    MAPAAT              = 0x07,

    // String value attributes:
    DISSOLVE_MAP        = 0x80,
    BUMP_MAP            = 0x81,
    KA_MAP              = 0x82,
    KD_MAP              = 0x83,
    KS_MAP              = 0x84,
    NS_MAP              = 0x85
};

enum class mtl_imfchanid {
    R,
    G,
    B,
    L,
    M,
    Z
};

enum class mtl_optid {
    BM,
    BLENDU,
    BLENDV,
    CLAMP,
    MM,
    IMFCHAN,
    TEXRES,
    TEX_OFFSET,
    TEX_SCALE,
    TEX_TURBULENCE,
    HALO
};

union mtl_val {
    float         flt;
    float3        flt3;
    unsigned      uint;
    mtl_imfchanid channel;
    const char*   str;
};

struct mtl_attropt {
    mtl_optid id;
    mtl_val   value;
};

struct mtl_attr {
    mtl_attrid               id;
    mtl_val                  value;
    std::vector<mtl_attropt> options;

    mtl_attr(mtl_attrid id);
    mtl_attr(mtl_attrid id, float value);
    mtl_attr(mtl_attrid id, float3 value);
    mtl_attr(mtl_attrid id, unsigned value);
    mtl_attr(mtl_attrid id, mtl_imfchanid value);
    mtl_attr(mtl_attrid id, const char* str);
    ~mtl_attr();

    mtl_attr(const mtl_attr& other);
    mtl_attr(mtl_attr&& other);
    
    mtl_attr& operator=(const mtl_attr& other);
    mtl_attr& operator=(mtl_attr&& other);
};

struct mtl_material {
    std::string           id;
    std::vector<mtl_attr> attrs;
};
