#include "mtl_parse.h"
#include <stack>
#include "../../output.h"
#include <cassert>

enum class mtl_symbol {
    NT_START,
    NT_STATEMENTLIST,
    NT_STATEMENT,
    NT_BUMPMAP_OPTIONLIST,
    NT_KXMAP_OPTIONLIST,
    NT_DFTMAP_OPTIONLIST,
    NT_D_OPTIONLIST,
    NT_OPT_UVWLIST,
    NT_OPT_VWLIST,
    NT_OPT_WLIST,
    
    T_ID_BUMPMAP,
    T_ID_KS,
    T_ID_NS,
    T_ID_D,
    T_ID_DISP,
    T_ID_ILLUM,
    T_ID_KA,
    T_ID_KD,
    T_ID_MAPAAT,
    T_ID_MAPD,
    T_ID_MAPKA,
    T_ID_MAPKD,
    T_ID_MAPKS,
    T_ID_MAPNS,
    T_ID_NEWMTL,

    T_END,
    T_KARED,
    T_KAGREEN,
    T_KABLUE,
    T_KDRED,
    T_KDGREEN,
    T_KDBLUE,
    T_KSRED,
    T_KSGREEN,
    T_KSBLUE,
    T_NSEXP,
    T_MAPKA_FILENAME,
    T_MAPKD_FILENAME,
    T_MAPKS_FILENAME,
    T_MAPNS_FILENAME,
    T_MAPD_FILENAME,
    T_MAPAAT_SWITCH,
    T_BUMPMAP_FILENAME,
    T_DISP_FILENAME,
    T_DISSOLVE_VALUE,
    T_TRANSPARENCY_VALUE,
    T_ILLUM_VALUE,
    T_MTLNAME,

    T_OPTID_BM,
    T_OPTID_BLENDU,
    T_OPTID_BLENDV,
    T_OPTID_CLAMP,
    T_OPTID_IMFCHAN,
    T_OPTID_MM,
    T_OPTID_TEXRES,
    T_OPTID_O,
    T_OPTID_S,
    T_OPTID_T,
    T_OPTID_HALO,

    T_OPT_BMVAL,
    T_OPT_BLENDUSWITCHVAL,
    T_OPT_BLENDVSWITCHVAL,
    T_OPT_CLAMPSWITCHVAL,
    T_OPT_CCSWITCHVAL,
    T_OPT_CHANNELVAL,
    T_OPT_GAINVAL,
    T_OPT_BASEVAL,
    T_OPT_RESVAL,
    T_OPT_UVAL,
    T_OPT_VVAL,
    T_OPT_WVAL
};

bool has_mat(const std::vector<mtl_material>* matlib, const char* id) {
    auto iter = std::find_if(matlib->begin(), matlib->end(), [id](const mtl_material& m) {
        return strcmp(m.id.c_str(), id) == 0; });

    return (iter != matlib->end());
}

bool has_attr(const mtl_material* mat, mtl_attrid id) {
    auto iter = std::find_if(mat->attrs.begin(), mat->attrs.end(), [id](const mtl_attr& a) {
        return (a.id == id); });
    return (iter != mat->attrs.end());
}

bool has_option(const mtl_attr* attr, mtl_optid id) {
    auto iter = std::find_if(attr->options.begin(), attr->options.end(), [id](const mtl_attropt& o) {
        return (o.id == id); });
    return (iter != attr->options.end());
}


bool expand_nt_start(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    terminals->push(mtl_symbol::NT_STATEMENTLIST);
    return true;
}

bool expand_nt_statementlist(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type != mtltoken_type::END) {
        terminals->push(mtl_symbol::NT_STATEMENTLIST);
        terminals->push(mtl_symbol::NT_STATEMENT);
    }
    return true;
}

bool expand_nt_statement(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    switch(lookahead.type)
    {
    case mtltoken_type::ID_BUMP:
        terminals->push(mtl_symbol::T_BUMPMAP_FILENAME);
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_BUMPMAP);
        break;
    case mtltoken_type::ID_D:
        terminals->push(mtl_symbol::T_DISSOLVE_VALUE);
        terminals->push(mtl_symbol::NT_D_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_D);
        break;
    case mtltoken_type::ID_DISP:
        terminals->push(mtl_symbol::T_DISP_FILENAME);
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_DISP);
        break;
    case mtltoken_type::ID_ILLUM:
        terminals->push(mtl_symbol::T_ILLUM_VALUE);
        terminals->push(mtl_symbol::T_ID_ILLUM);
        tokens->pop();
        break;
    case mtltoken_type::ID_KA:
        terminals->push(mtl_symbol::T_KABLUE);
        terminals->push(mtl_symbol::T_KAGREEN);
        terminals->push(mtl_symbol::T_KARED);
        terminals->push(mtl_symbol::T_ID_KA);
        break;
    case mtltoken_type::ID_KD:
        terminals->push(mtl_symbol::T_KDBLUE);
        terminals->push(mtl_symbol::T_KDGREEN);
        terminals->push(mtl_symbol::T_KDRED);
        terminals->push(mtl_symbol::T_ID_KD);
        break;
    case mtltoken_type::ID_KS:
        terminals->push(mtl_symbol::T_KSBLUE);
        terminals->push(mtl_symbol::T_KSGREEN);
        terminals->push(mtl_symbol::T_KSRED);
        terminals->push(mtl_symbol::T_ID_KS);
        break;
    case mtltoken_type::ID_MAPAAT:
        terminals->push(mtl_symbol::T_MAPAAT_SWITCH);
        terminals->push(mtl_symbol::T_ID_MAPAAT);
        break;
    case mtltoken_type::ID_MAPD:
        terminals->push(mtl_symbol::T_MAPD_FILENAME);
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_MAPD);
        break;
    case mtltoken_type::ID_MAPKA:
        terminals->push(mtl_symbol::T_MAPKA_FILENAME);
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_MAPKA);
        break;
    case mtltoken_type::ID_MAPKD:
        terminals->push(mtl_symbol::T_MAPKD_FILENAME);
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_MAPKD);
        break;
    case mtltoken_type::ID_MAPKS:
        terminals->push(mtl_symbol::T_MAPKS_FILENAME);
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_MAPKS);
        break;
    case mtltoken_type::ID_MAPNS:
        terminals->push(mtl_symbol::T_MAPNS_FILENAME);
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_ID_MAPNS);
        break;
    case mtltoken_type::ID_NEWMTL:
        terminals->push(mtl_symbol::T_MTLNAME);
        terminals->push(mtl_symbol::T_ID_NEWMTL);
        break;
    case mtltoken_type::ID_NS:
        terminals->push(mtl_symbol::T_NSEXP);
        terminals->push(mtl_symbol::T_ID_NS);
        break;
    default:
        print_error("Unexpected token, expected a keyword to start a statement in line %i\n", lookahead.line);
        return false;
    }
    return true;
}

bool expand_nt_doptions(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    switch (lookahead.type) {
    case mtltoken_type::OPT_HALO:
        terminals->push(mtl_symbol::T_OPTID_HALO);
        break;
    default:
        print_error("Unrecognized dissolve option in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool expand_nt_bmoptions(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    switch (lookahead.type) {
    case mtltoken_type::OPT_BM:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BMVAL);
        terminals->push(mtl_symbol::T_OPTID_BM);
        break;
    case mtltoken_type::OPT_BLENDU:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDUSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDU);
        break;
    case mtltoken_type::OPT_BLENDV:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDVSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDV);
        break;
    case mtltoken_type::OPT_CLAMP:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CLAMPSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_CLAMP);
        break;
    case mtltoken_type::OPT_IMFCHAN:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CHANNELVAL);
        terminals->push(mtl_symbol::T_OPTID_IMFCHAN);
        break;
    case mtltoken_type::OPT_MM:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_GAINVAL);
        terminals->push(mtl_symbol::T_OPT_BASEVAL);
        terminals->push(mtl_symbol::T_OPTID_MM);
        break;
    case mtltoken_type::OPT_O:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_O);
        break;
    case mtltoken_type::OPT_S:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_S);
        break;
    case mtltoken_type::OPT_T:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_T);
        break;
    case mtltoken_type::OPT_TEXRES:
        terminals->push(mtl_symbol::NT_BUMPMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_RESVAL);
        terminals->push(mtl_symbol::T_OPTID_TEXRES);
        break;
    default:
        break;
    }

    return true;
}

bool expand_nt_kxmapoptions(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    switch (lookahead.type) {
    case mtltoken_type::OPT_BLENDU:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDUSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDU);
        break;
    case mtltoken_type::OPT_BLENDV:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDVSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDV);
        break;
    case mtltoken_type::OPT_CC:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CCSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_CLAMP);
        break;
    case mtltoken_type::OPT_CLAMP:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CLAMPSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_CLAMP);
        break;
    case mtltoken_type::OPT_MM:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_GAINVAL);
        terminals->push(mtl_symbol::T_OPT_BASEVAL);
        terminals->push(mtl_symbol::T_OPTID_MM);
        break;
    case mtltoken_type::OPT_O:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_O);
        break;
    case mtltoken_type::OPT_S:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_S);
        break;
    case mtltoken_type::OPT_T:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_T);
        break;
    case mtltoken_type::OPT_TEXRES:
        terminals->push(mtl_symbol::NT_KXMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_RESVAL);
        terminals->push(mtl_symbol::T_OPTID_TEXRES);
        break;
    default:
        break;

    }

    return true;
}

bool expand_nt_dftoptions(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    switch (lookahead.type) {
    case mtltoken_type::OPT_BLENDU:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDUSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDU);
        break;
    case mtltoken_type::OPT_BLENDV:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_BLENDVSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_BLENDV);
        break;
    case mtltoken_type::OPT_CLAMP:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CLAMPSWITCHVAL);
        terminals->push(mtl_symbol::T_OPTID_CLAMP);
        break;
    case mtltoken_type::OPT_IMFCHAN:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_CHANNELVAL);
        terminals->push(mtl_symbol::T_OPTID_IMFCHAN);
        break;
    case mtltoken_type::OPT_MM:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_GAINVAL);
        terminals->push(mtl_symbol::T_OPT_BASEVAL);
        terminals->push(mtl_symbol::T_OPTID_MM);
        break;
    case mtltoken_type::OPT_O:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_O);
        break;
    case mtltoken_type::OPT_S:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_S);
        break;
    case mtltoken_type::OPT_T:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::NT_OPT_UVWLIST);
        terminals->push(mtl_symbol::T_OPTID_T);
        break;
    case mtltoken_type::OPT_TEXRES:
        terminals->push(mtl_symbol::NT_DFTMAP_OPTIONLIST);
        terminals->push(mtl_symbol::T_OPT_RESVAL);
        terminals->push(mtl_symbol::T_OPTID_TEXRES);
        break;
    default:
        break;
    }

    return true;
}

bool expand_nt_uvwlist(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    terminals->push(mtl_symbol::NT_OPT_VWLIST);
    terminals->push(mtl_symbol::T_OPT_UVAL);

    return true;
}

bool expand_nt_vwlist(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        terminals->push(mtl_symbol::NT_OPT_WLIST);
        terminals->push(mtl_symbol::T_OPT_VVAL);
    }

    return true;
}

bool expand_nt_wlist(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        terminals->push(mtl_symbol::T_OPT_WVAL);
    }

    return true;
}

bool resolve_attrstring(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_STR) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);

        matlib->back().attrs.back().value.str = strdup(lookahead.value.str);
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a string literal as '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attruint(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_UINT) {
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a uint literal as '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_mtlname(std::queue<mtl_token>* tokens, std::stack<mtl_symbol>* terminals, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_STR) {
        if (has_mat(matlib, lookahead.value.str)) {
            print_error("Redefinition of material '%s' in line %i", lookahead.value.str, lookahead.line);
            return false;
        }
        if (matlib->back().id != "") {
            print_warning("Changing name of material '%s' to '%s'\n", matlib->back().id, lookahead.value.str);
        }
        matlib->back().id = lookahead.value.str;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a string literal as material ID in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attrid(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, mtl_attrid aid) {
    mtl_token lookahead = tokens->front();
    if (matlib->size() > 0 && matlib->back().id != "") {
        if (!has_attr(&matlib->back(), aid)) {
            auto& mat = matlib->back();
                mat.attrs.push_back({ aid });
                tokens->pop();
        }
        else {
            print_error("Redefinition of attribute in line %i\n", lookahead.line);
            return false;
        }
    }
    else {
        print_error("Couldn't add an attribute, material ID needs to be defined first\n");
        return false;
    }

    return true;
}

bool resolve_optid(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, mtl_optid oid) {
    mtl_token lookahead = tokens->front();
    if (matlib->size() > 0 && matlib->back().attrs.size() > 0) {
        if (!has_option(&matlib->back().attrs.back(), oid)) {
            auto& mat = matlib->back();
            mat.attrs.back().options.push_back({ oid });
            tokens->pop();
        }
        else {
            print_error("Redefinition of attribute option in line %i\n", lookahead.line);
            return false;
        }
    }
    else {
        print_error("Couldn't add an attribute option, option is defined out of attribute context\n");
        return false;
    }

    return true;
}

bool resolve_newmtlid(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    matlib->push_back({});
    tokens->pop();

    return true;
}

bool resolve_attrfloat(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        auto& attr = matlib->back().attrs.back();

        attr.value.flt = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attrfloat3x(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        auto& attr = matlib->back().attrs.back();

        attr.value.flt3.x = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as first component of '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attrfloat3y(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        auto& attr = matlib->back().attrs.back();

        attr.value.flt3.y = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as second component of '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attrfloat3z(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        auto& attr = matlib->back().attrs.back();

        attr.value.flt3.z = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as third component of '%s' value in line %i\n", attr_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_attrswitch(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* attr_name) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_STR) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);

        if (strcmp(lookahead.value.str, "on") == 0) {
            matlib->back().attrs.back().value.uint = 1;
        }
        else if (strcmp(lookahead.value.str, "off") == 0) {
            matlib->back().attrs.back().value.uint = 0;
        }
        else {
            print_error("Expected 'on' or 'off' as value for "
                "'%s' in line %i\n", attr_name, lookahead.line);
            return false;
        }
        tokens->pop();
    }
    else if (lookahead.type == mtltoken_type::VAL_UINT) {
        if (lookahead.value.uint == 0 ||
            lookahead.value.uint == 1) {
            matlib->back().attrs.back().options.back().value.uint = lookahead.value.uint;
        }
        else {
            print_error("Expected '0', '1' or (preferably) 'on' or 'off' "
                "as value for '%s' in line %i\n", attr_name, lookahead.line);
            return false;
        }
    }
    return true;
}

bool resolve_optswitch(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* opt_name) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_STR) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);

        if (strcmp(lookahead.value.str,"on") == 0) {
            matlib->back().attrs.back().options.back().value.uint = 1;
        }
        else if (strcmp(lookahead.value.str, "off") == 0) {
            matlib->back().attrs.back().options.back().value.uint = 0;
        }
        else {
            print_error("Expected 'on' or 'off' as option argument for "
                        "'%s' in line %i\n", opt_name, lookahead.line);
            return false;
        }
        tokens->pop();
    }
    else if (lookahead.type == mtltoken_type::VAL_UINT) {
        if (lookahead.value.uint == 0 ||
            lookahead.value.uint == 1) {
            matlib->back().attrs.back().options.back().value.uint = lookahead.value.uint;
        }
        else {
            print_error("Expected '0', '1' or (preferably) 'on' or 'off' "
                        "as option argument for '%s' in line %i\n", opt_name, lookahead.line);
            return false;
        }
    }
    return true;
}

bool resolve_channelval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_STR && lookahead.value.str[0] != '\0') {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        switch (lookahead.value.str[0]) {
        case 'r': opt.value.channel = mtl_imfchanid::R; tokens->pop(); break;
        case 'g': opt.value.channel = mtl_imfchanid::G; tokens->pop(); break;
        case 'b': opt.value.channel = mtl_imfchanid::B; tokens->pop(); break;
        case 'l': opt.value.channel = mtl_imfchanid::L; tokens->pop(); break;
        case 'm': opt.value.channel = mtl_imfchanid::M; tokens->pop(); break;
        case 'z': opt.value.channel = mtl_imfchanid::Z; tokens->pop(); break;
        default:
            print_error("Expected r|g|b|l|m|z as 'imfchan' argument in line %i\n", lookahead.line);
            return false;
        }
    } 
    else {
        print_error("Expected r|g|b|l|m|z as 'imfchan' argument in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool resolve_mmgainval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt3.y = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Expected a float literal as 'gain' argument in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool resolve_mmbaseval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();
    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt3.x = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Expected a float literal as 'base' argument in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool resolve_optfloat(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* opt_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as '%s' argument in line %i\n", opt_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_optuint(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib, const char* opt_name) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_UINT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.uint = lookahead.value.uint;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as '%s' argument in line %i\n", opt_name, lookahead.line);
        return false;
    }

    return true;
}

bool resolve_optuval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt3.x = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as u value in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

bool resolve_optvval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt3.y = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as v value in line %i\n", lookahead.line);
        return false;
    }

    return true;
}


bool resolve_optwval(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    mtl_token lookahead = tokens->front();

    if (lookahead.type == mtltoken_type::VAL_FLOAT) {
        assert(matlib->size() > 0);
        assert(matlib->back().attrs.size() > 0);
        assert(matlib->back().attrs.back().options.size() > 0);
        auto& opt = matlib->back().attrs.back().options.back();

        opt.value.flt3.z = lookahead.value.flt;
        tokens->pop();
    }
    else {
        print_error("Unexpected token, expected a float literal as w value in line %i\n", lookahead.line);
        return false;
    }

    return true;
}

void mtl_parse(std::queue<mtl_token>* tokens, std::vector<mtl_material>* matlib) {
    std::stack<mtl_symbol> terminals;
    terminals.push(mtl_symbol::NT_START);

    std::vector<mtl_material> tmp_matlib;

    bool well = true;
    while (!terminals.empty() && well) {
        mtl_token lookahead = tokens->front();
    
        mtl_symbol front = terminals.top();
        terminals.pop();
        switch (front) {
        case mtl_symbol::NT_START:              well = expand_nt_start(tokens, &terminals);         break;
        case mtl_symbol::NT_STATEMENTLIST:      well = expand_nt_statementlist(tokens, &terminals); break;
        case mtl_symbol::NT_STATEMENT:          well = expand_nt_statement(tokens, &terminals);     break;
        case mtl_symbol::NT_BUMPMAP_OPTIONLIST: well = expand_nt_bmoptions(tokens, &terminals);     break;
        case mtl_symbol::NT_KXMAP_OPTIONLIST:   well = expand_nt_kxmapoptions(tokens, &terminals);  break;
        case mtl_symbol::NT_D_OPTIONLIST:       well = expand_nt_doptions(tokens, &terminals);      break;
        case mtl_symbol::NT_DFTMAP_OPTIONLIST:  well = expand_nt_dftoptions(tokens, &terminals);    break;
        case mtl_symbol::NT_OPT_UVWLIST:        well = expand_nt_uvwlist(tokens, &terminals);       break;
        case mtl_symbol::NT_OPT_VWLIST:         well = expand_nt_vwlist(tokens, &terminals);        break;
        case mtl_symbol::NT_OPT_WLIST:          well = expand_nt_wlist(tokens, &terminals);         break;
        case mtl_symbol::T_BUMPMAP_FILENAME: well = resolve_attrstring(tokens, &tmp_matlib, "bump");   break;
        case mtl_symbol::T_DISP_FILENAME:    well = resolve_attrstring(tokens, &tmp_matlib, "bump");   break;
        case mtl_symbol::T_DISSOLVE_VALUE:   well = resolve_attrfloat(tokens, &tmp_matlib, "d");       break;
        case mtl_symbol::T_ILLUM_VALUE:      well = resolve_attruint(tokens, &tmp_matlib, "illum");    break;
        case mtl_symbol::T_KABLUE:           well = resolve_attrfloat3z(tokens, &tmp_matlib, "Ka");    break;
        case mtl_symbol::T_KAGREEN:          well = resolve_attrfloat3y(tokens, &tmp_matlib, "Ka");    break;
        case mtl_symbol::T_KARED:            well = resolve_attrfloat3x(tokens, &tmp_matlib, "Ka");    break;
        case mtl_symbol::T_KDBLUE:           well = resolve_attrfloat3z(tokens, &tmp_matlib, "Kd");    break;
        case mtl_symbol::T_KDGREEN:          well = resolve_attrfloat3y(tokens, &tmp_matlib, "Kd");    break;
        case mtl_symbol::T_KDRED:            well = resolve_attrfloat3x(tokens, &tmp_matlib, "Kd");    break;
        case mtl_symbol::T_KSBLUE:           well = resolve_attrfloat3z(tokens, &tmp_matlib, "Ks");    break;
        case mtl_symbol::T_KSGREEN:          well = resolve_attrfloat3y(tokens, &tmp_matlib, "Ks");    break;
        case mtl_symbol::T_KSRED:            well = resolve_attrfloat3x(tokens, &tmp_matlib, "Ks");    break;
        case mtl_symbol::T_MAPAAT_SWITCH:    well = resolve_attrswitch(tokens, &tmp_matlib, "mapaat"); break;
        case mtl_symbol::T_MAPD_FILENAME:    well = resolve_attrstring(tokens, &tmp_matlib, "map_d");  break;
        case mtl_symbol::T_MAPKA_FILENAME:   well = resolve_attrstring(tokens, &tmp_matlib, "map_Ka"); break;
        case mtl_symbol::T_MAPKD_FILENAME:   well = resolve_attrstring(tokens, &tmp_matlib, "map_Kd"); break;
        case mtl_symbol::T_MAPKS_FILENAME:   well = resolve_attrstring(tokens, &tmp_matlib, "map_Ks"); break;
        case mtl_symbol::T_MAPNS_FILENAME:   well = resolve_attrstring(tokens, &tmp_matlib, "map_Ns"); break;
        case mtl_symbol::T_MTLNAME:          well = resolve_mtlname(tokens, &terminals, &tmp_matlib);  break;
        case mtl_symbol::T_NSEXP:            well = resolve_attruint(tokens, &tmp_matlib, "Ns");    break;
        case mtl_symbol::T_ID_BUMPMAP:       well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::BUMP_MAP);         break;
        case mtl_symbol::T_ID_KS:            well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KS);               break;
        case mtl_symbol::T_ID_NS:            well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::NS);               break;
        case mtl_symbol::T_ID_D:             well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::DISSOLVE);         break;
        case mtl_symbol::T_ID_DISP:          well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::DISPLACEMENT_MAP); break;
        case mtl_symbol::T_ID_ILLUM:         well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KA);               break;
        case mtl_symbol::T_ID_KA:            well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KD);               break;
        case mtl_symbol::T_ID_KD:            well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KS);               break;
        case mtl_symbol::T_ID_MAPAAT:        well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::MAPAAT);           break;
        case mtl_symbol::T_ID_MAPD:          well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::DISSOLVE_MAP);     break;
        case mtl_symbol::T_ID_MAPKA:         well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KA_MAP);           break;
        case mtl_symbol::T_ID_MAPKD:         well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KD_MAP);           break;
        case mtl_symbol::T_ID_MAPKS:         well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::KS_MAP);           break;
        case mtl_symbol::T_ID_MAPNS:         well = resolve_attrid(tokens, &tmp_matlib, mtl_attrid::NS_MAP);           break;
        case mtl_symbol::T_ID_NEWMTL:        well = resolve_newmtlid(tokens, &tmp_matlib);                             break;
        case mtl_symbol::T_OPTID_BM:         well = resolve_optid(tokens, &tmp_matlib, mtl_optid::BM);             break;
        case mtl_symbol::T_OPTID_BLENDU:     well = resolve_optid(tokens, &tmp_matlib, mtl_optid::BLENDU);         break;
        case mtl_symbol::T_OPTID_BLENDV:     well = resolve_optid(tokens, &tmp_matlib, mtl_optid::BLENDV);         break;
        case mtl_symbol::T_OPTID_CLAMP:      well = resolve_optid(tokens, &tmp_matlib, mtl_optid::CLAMP);          break;
        case mtl_symbol::T_OPTID_IMFCHAN:    well = resolve_optid(tokens, &tmp_matlib, mtl_optid::IMFCHAN);        break;
        case mtl_symbol::T_OPTID_MM:         well = resolve_optid(tokens, &tmp_matlib, mtl_optid::MM);             break;
        case mtl_symbol::T_OPTID_TEXRES:     well = resolve_optid(tokens, &tmp_matlib, mtl_optid::TEXRES);         break;
        case mtl_symbol::T_OPTID_O:          well = resolve_optid(tokens, &tmp_matlib, mtl_optid::TEX_OFFSET);     break;
        case mtl_symbol::T_OPTID_S:          well = resolve_optid(tokens, &tmp_matlib, mtl_optid::TEX_SCALE);      break;
        case mtl_symbol::T_OPTID_T:          well = resolve_optid(tokens, &tmp_matlib, mtl_optid::TEX_TURBULENCE); break;
        case mtl_symbol::T_OPTID_HALO:          well = resolve_optid(tokens, &tmp_matlib, mtl_optid::HALO);        break;
        case mtl_symbol::T_OPT_BMVAL:           well = resolve_optfloat(tokens, &tmp_matlib,  "-bm");     break;
        case mtl_symbol::T_OPT_BLENDUSWITCHVAL: well = resolve_optswitch(tokens, &tmp_matlib, "-blendu"); break;
        case mtl_symbol::T_OPT_BLENDVSWITCHVAL: well = resolve_optswitch(tokens, &tmp_matlib, "-blendv"); break;
        case mtl_symbol::T_OPT_CLAMPSWITCHVAL:  well = resolve_optswitch(tokens, &tmp_matlib, "-clamp");  break;
        case mtl_symbol::T_OPT_CCSWITCHVAL:     well = resolve_optswitch(tokens, &tmp_matlib, "-cc");     break;
        case mtl_symbol::T_OPT_CHANNELVAL:      well = resolve_channelval(tokens, &tmp_matlib);           break;
        case mtl_symbol::T_OPT_GAINVAL:         well = resolve_mmgainval(tokens, &tmp_matlib);            break;
        case mtl_symbol::T_OPT_BASEVAL:         well = resolve_mmbaseval(tokens, &tmp_matlib);            break;
        case mtl_symbol::T_OPT_RESVAL:          well = resolve_optuint(tokens, &tmp_matlib, "-texres");   break;
        case mtl_symbol::T_OPT_UVAL:            well = resolve_optuval(tokens, &tmp_matlib);              break;
        case mtl_symbol::T_OPT_VVAL:            well = resolve_optvval(tokens, &tmp_matlib);              break;
        case mtl_symbol::T_OPT_WVAL:            well = resolve_optwval(tokens, &tmp_matlib);              break;
        }
    }

    if (well) {
        for (auto& mat : tmp_matlib) {
            auto iter = std::find_if(matlib->begin(), matlib->end(), [mat](const mtl_material& mtl) {
                return (mtl.id == mat.id); });

            if (iter == matlib->end()) {
                matlib->push_back(mat);
            }
            else {
                print_error("A material was defined in a different mtl file already");
            }
        }
    }
    else {
        print_error("Aborted parsing because file is ill-formed\n");
    }

}