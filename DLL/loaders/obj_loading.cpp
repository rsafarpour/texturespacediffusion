#include "obj_loading.h"
#include "obj_files/obj_lexer.h"
#include "obj_files/obj_parse.h"
#include "obj_files/obj_token.h"
#include "obj_files/obj_postprocess.h"

#include "mtl_files/mtl_lexer.h"
#include "mtl_files/mtl_parse.h"

#include <queue>

obj_data obj_load(const std::string* file_name) {
    obj_data out;

    // Extract obj file location to search for MTL files in 
    // the same directory as the OBJ file
    std::string path_prefix;
    size_t pos = file_name->find_last_of('/');
    if (pos != std::string::npos) {
        path_prefix = file_name->substr(0, pos) + '/';
    }

    std::deque<obj_token> tokens = obj_lex(file_name);
    std::vector<std::string> mtl_files = extract_mtllibs(&tokens);

    for (std::string file_name : mtl_files) {
        file_name = path_prefix + file_name;
        std::queue<mtl_token> mtl_tokens = mtl_lex(&file_name);
        mtl_parse(&mtl_tokens, &out.materials);
    }

    std::vector<obj_object> objects = obj_parse(&tokens, &out.materials);

    obj_generatenormals(&objects);
    // TODO: Make this dependent on parameter
    obj_flipv(&objects);
    out.meshes = obj_postprocess(&objects);


    return out;
}
