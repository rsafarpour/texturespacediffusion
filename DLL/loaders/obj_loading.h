#pragma once

#include <string>
#include "obj_files/obj_output.h"
#include "mtl_files/mtl_output.h"

struct obj_data {
    std::vector<obj_mesh>     meshes;
    std::vector<mtl_material> materials;
};

obj_data obj_load(const std::string* file_name);