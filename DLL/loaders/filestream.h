#pragma once

#include <stdio.h>
#include <inttypes.h>
#include <string>

using filesize_t  = int64_t;
using chunksize_t = int32_t;

class fistream {
public:
    fistream(const char* file_name);
    ~fistream();

    FILE*       handle;
    filesize_t  size;
    fpos_t      position;
};


size_t read_chunk(fistream* f, char* out, chunksize_t size);
size_t read_chunk(fistream* f, std::string* out, chunksize_t size);


