#include "obj_lexer.h"
#include "../../conf.h"
#include "../../output.h"
#include"../filestream.h"
#include <cctype>


#define commit_token(token_id) token_stream.push_back({ token_id, fline }); \
                               state = 0; \
                               if (c == '\n') { fline++; }
#define jump_to_string(str_state) else if (isspace(c)) { state = str_state; continue; }\
                                  else { state = str_state; }

std::deque<obj_token> obj_lex(const std::string* file_name) {
    fistream fin(file_name->c_str());
    std::deque<obj_token> token_stream;
    std::string token_string;

    size_t fline = 1;
    uint3 vertdef;
    char chunks[2][PAGE_BYTES];
    size_t size[2];
    fpos_t chunk_pos = 0;
    int state = 0;
    char pr_chunk = 0;
    char rd_chunk = 0;
    size[rd_chunk] = read_chunk(&fin, chunks[rd_chunk], PAGE_BYTES);
    if (size[rd_chunk] < PAGE_BYTES) {
        chunks[rd_chunk][size[rd_chunk]++] = ' ';
    }

    do {
        rd_chunk = (rd_chunk + 1) % 2;
        pr_chunk = (rd_chunk + 1) % 2;
        size[rd_chunk] = read_chunk(&fin, chunks[rd_chunk], PAGE_BYTES);
        if (size[rd_chunk] < PAGE_BYTES) {
            chunks[rd_chunk][size[rd_chunk]++] = ' ';
        }

        chunk_pos = 0;
        char c = -1;
        while (chunk_pos < size[pr_chunk]) {
            char c = chunks[pr_chunk][chunk_pos];
            token_string += c;
            switch (state) {
            case 0:
                token_string.clear();
                token_string += c;

                if (isspace(c)) { if (c == '\n') { fline++; } }          // Count lines
                else if (c == '#')   { state = 1; }     // Comment
                else if (c == 'v')   { state = 2; }     // v or vn
                else if (c == 'g')   { state = 5; }     // group
                else if (c == 'f')   { state = 6; }     // face
                else if (c == 'm')   { state = 7; }     // mtllib
                else if (c == 'u')   { state = 13; }    // usemtl
                else if (c == '-')   { state = 19; }    // float
                else if (isdigit(c)) { state = 22; }    // float, uint or vert_id
                else if (c == 's')   { state = 28; }    // smoothing
                else if (c == 'o')   { state = 27; }    // object id
                else if (c == 'l')   { state = 29; }    // line
                else { state = 23; }                    // string
                break;

                // Comments
            case 1:
                if (c == '\n') {
                    fline++;
                    state = 0;
                }
                break;

                // 'v', 'vn' or 'vt'
            case 2:
                if (isspace(c)) { commit_token(objtoken_type::ID_POSITION); }
                else if (c == 'n') { state = 3; }
                else if (c == 't') { state = 4; }
                else { state = 23; }
                break;
            case 3:
                if (isspace(c)) { commit_token(objtoken_type::ID_NORMAL); }
                else { state = 23; }
                break;
            case 4:
                if (isspace(c)) { commit_token(objtoken_type::ID_TEXCOORD); }
                else { state = 23; }
                break;

                // 'g'
            case 5:
                if (isspace(c)) { commit_token(objtoken_type::ID_GROUP); }
                else { state = 23; }
                break;

                // 'f'
            case 6:
                if (isspace(c)) { commit_token(objtoken_type::ID_FACE); }
                else { state = 23; }
                break;

                // 'mtllib'
            case 7:
                if (c == 't') { state = 8; }
                jump_to_string(23);
                break;
            case 8:
                if (c == 'l') { state = 9; }
                jump_to_string(23);
                break;
            case 9:
                if (c == 'l') { state = 10; }
                jump_to_string(23);
                break;
            case 10:
                if (c == 'i') { state = 11; }
                else { state = 23; }
                break;
            case 11:
                if (c == 'b') { state = 12; }
                jump_to_string(23);
                break;
            case 12:
                if (isspace(c)) { commit_token(objtoken_type::ID_MTLLIB); }
                else { state = 23; }
                break;

                // 'usemtl'
            case 13:
                if (c == 's') { state = 14; }
                jump_to_string(23);
                break;
            case 14:
                if (c == 'e') { state = 15; }
                jump_to_string(23);
                break;
            case 15:
                if (c == 'm') { state = 16; }
                jump_to_string(23);
                break;
            case 16:
                if (c == 't') { state = 17; }
                jump_to_string(23);
                break;
            case 17:
                if (c == 'l') { state = 18; }
                jump_to_string(23);
                break;
            case 18:
                if (isspace(c)) { commit_token(objtoken_type::ID_USEMTL); } 
                else {
                    state = 23;
                }
                break;

                // float
            case 19:
                if (isdigit(c)) { state = 20; }
                else return {};
                break;
            case 20:
                if (isdigit(c)) { state = 20; }
                else if (c == '.') { state = 21; }
                else return {};
                break;
            case 21:
                if (isdigit(c)) { state = 21; }
                else if (isspace(c)) { commit_token(std::strtof(token_string.c_str(), nullptr)); }
                else return {};
                break;

                // float or face def
            case 22:
                if (isdigit(c)) {
                }
                else if (c == '.') {
                    state = 21; // Process as float
                }
                else if (c == '/') {
                    // Process as face def string
                    token_string.pop_back();
                    vertdef.x = std::strtoul(token_string.c_str(), nullptr, 10);
                    token_string.clear();
                    state = 24;
                }
                else if (isspace(c)) { 
                    commit_token((unsigned)std::strtoul(token_string.c_str(), nullptr, 10)); } 
                break;

                // string
            case 23:
                if (!isspace(c) && token_string.size() < 1024) { state = 23; }
                else if (isspace(c)) {
                    token_string.pop_back();
                    token_stream.push_back({token_string.c_str(), fline });
                    state = 0;
                    if (c == '\n') { fline++; }
                }
                else {
                    print_error("A string literal exceeded the limit of 1024 characters in line %i", fline);
                    return {};
                }
                break;

                // face def
            case 24:
                if (isdigit(c)) {
                }
                else if (c == '/' && token_string == "/") {
                    token_string.clear();
                    state = 25;
                }
                else if (c == '/' && !token_string.empty()) {
                    token_string.pop_back();
                    vertdef.y = std::strtoul(token_string.c_str(), nullptr, 10);
                    token_string.clear();
                    state = 26;
                }
                else if (isspace(c)) {
                    token_string.pop_back();
                    vertdef.y = std::strtoul(token_string.c_str(), nullptr, 10);
                    vertdef.z = ~0;
                    token_stream.push_back({ objtoken_type::VAL_VERTID_PT, vertdef, fline });
                    token_string.clear();
                    state = 0;
                    if (c == '\n') { fline++; }
                }
                break;
                // facedef without texcoord
            case 25:
                if (isspace(c)) {
                    vertdef.y = ~0;
                    vertdef.z = std::strtoul(token_string.c_str(), nullptr, 10);
                    token_stream.push_back({ objtoken_type::VAL_VERTID_PN, vertdef, fline });
                    state = 0;
                    if (c == '\n') { fline++; }
                }
                else if (!isdigit(c)) {
                    print_error("Illegal face definition in line, expected a digit or space %i", fline);
                    return {};
                }
                break;
                // face def with texcoord
            case 26:
                if (isspace(c)) {
                    vertdef.z = std::strtoul(token_string.c_str(), nullptr, 10);
                    token_stream.push_back({ objtoken_type::VAL_VERTID_PTN, vertdef, fline });
                    token_string.clear();
                    state = 0;
                    if (c == '\n') { fline++; }
                }
                else if (!isdigit(c)) {
                    print_error("Illegal face definition in line, expected a digit or space %i", fline);
                }
                break;
            case 27:
                if (isspace(c)) { commit_token(objtoken_type::ID_OBJECTNAME); }
                else { state = 23; }
                break;
            case 28:
                if (isspace(c)) { commit_token(objtoken_type::ID_SMOOTH); }
                else { state = 23; }
                break;
            case 29:
                if (isspace(c)) { commit_token(objtoken_type::ID_LINE); }
                else { state = 23; }
                break;
            default:
                print_error("Error loading OBJ file");
            }

            chunk_pos++;
        }
    } while (size[pr_chunk] == PAGE_BYTES);

    token_stream.push_back({ objtoken_type::END, fline });
    return token_stream;
}