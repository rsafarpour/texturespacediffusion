#pragma once

#include "../../vectortypes.h"
#include "obj_layout.h"
#include <vector>
#include <string>

using vertex_id       = unsigned;
using face_id         = unsigned;
using objsmoothing_id = unsigned;
using objmaterial_id  = unsigned;

struct obj_polygroup {
    std::string           label;                  /* Label defined for polygon group    */
    std::vector<unsigned> face_ids;               /* List of polygon group member faces */
};

struct obj_vertices {
    std::vector<float3> positions;                 /* Vertex position         */
    std::vector<float3> normals;                   /* Normals if defined      */
    std::vector<float2> texcoords;                 /* Texcoords if defined    */
};

struct obj_object {
    std::vector<obj_polygroup> polygroups;

    obj_layout                   layout;           /* Layout of face vertices                        */
    obj_vertices                 vertices;         /* Vertex attributes                              */
    std::string                  label;            /* Label defined for object                       */
    std::vector<size_t>          nverts_face;      /* Vertex count per face                          */
    std::vector<size_t>          start_face;       /* Index of first vertex of a face                */
    std::vector<vertex_id>       position_ids;     /* Position ref of face vertex                    */
    std::vector<vertex_id>       texcoord_ids;     /* Texcoord ref of face vertex                    */
    std::vector<vertex_id>       normal_ids;       /* Normal   ref of face vertex                    */
    std::vector<objsmoothing_id> smoothing_groups; /* Smoothing group membership of each face vertex */
    std::vector<objmaterial_id>  material_ids;     /* Material ref for faces                         */
};

struct obj_mesh {
    obj_layout            layout;
    std::vector<float3>   normals;
    std::vector<float3>   positions;
    std::vector<float2>   texcoords;
    std::vector<unsigned> indices;
};
