#include "obj_parse.h"
#include <stack>
#include "../../output.h"

// TODO: Faces must be planar! Verify this!

enum obj_symbol {
    NT_START,
    NT_STATEMENTLIST,
    NT_STATEMENT,
    NT_LINESTART,
    NT_FACESTART,
    NT_VERTLISTP,
    NT_VERTLISTPT,
    NT_VERTLISTPN,
    NT_VERTLISTPTN,

    T_END,
    T_LINEEND,
    T_FACEEND,
    T_STRING,
    T_POSX,
    T_POSY,
    T_POSZ,
    T_NORMALX,
    T_NORMALY,
    T_NORMALZ,
    T_TEXCOORDU,
    T_TEXCOORDV,
    T_MTLLIBNAME,
    T_MTLNAME,
    T_GROUPID,
    T_VERTDEFP,
    T_VERTDEFPT,
    T_VERTDEFPN,
    T_VERTDEFPTN,
    T_VERTLISTEND,
    T_OBJECTNAME,
    T_SMOOTHGROUP
};

std::vector<std::string> extract_mtllibs(const std::deque<obj_token>* token_stream) {
    std::vector<std::string> mtl_files;

    auto iter = token_stream->begin();
    while (iter != token_stream->end()) {
        if (iter->type != objtoken_type::ID_MTLLIB) {
            ++iter;
            continue;
        }
        else {
            if ((++iter) != token_stream->end() && iter->type == objtoken_type::VAL_STR) {
                mtl_files.push_back(iter->value.str);
            }
        }
    }

    return mtl_files;
}



bool expand_nt_start(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    terminals->push(obj_symbol::T_END);
    terminals->push(obj_symbol::NT_STATEMENTLIST);
    return true;
}

bool expand_nt_statement(std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    switch (lookahead.type)
    {
    case objtoken_type::ID_USEMTL:
        terminals->push(obj_symbol::T_MTLNAME);
        tokens->pop_front();
        break;
    case objtoken_type::ID_MTLLIB:
        terminals->push(obj_symbol::T_MTLLIBNAME);
        tokens->pop_front();
        break;
    case objtoken_type::ID_POSITION:
        terminals->push(obj_symbol::T_POSZ);
        terminals->push(obj_symbol::T_POSY);
        terminals->push(obj_symbol::T_POSX);
        tokens->pop_front();
        break;
    case objtoken_type::ID_NORMAL:
        terminals->push(obj_symbol::T_NORMALZ);
        terminals->push(obj_symbol::T_NORMALY);
        terminals->push(obj_symbol::T_NORMALX);
        tokens->pop_front();
        break;
    case objtoken_type::ID_TEXCOORD:
        terminals->push(obj_symbol::T_TEXCOORDV);
        terminals->push(obj_symbol::T_TEXCOORDU);
        tokens->pop_front();
        break;
    case objtoken_type::ID_FACE:
        terminals->push(obj_symbol::NT_FACESTART);
        tokens->pop_front();
        break;
    case objtoken_type::ID_GROUP:
        terminals->push(obj_symbol::T_GROUPID);
        tokens->pop_front();
        break;
    case objtoken_type::ID_OBJECTNAME:
        terminals->push(obj_symbol::T_OBJECTNAME);
        tokens->pop_front();
        break;
    case objtoken_type::ID_SMOOTH:
        terminals->push(obj_symbol::T_SMOOTHGROUP);
        tokens->pop_front();
        break;
    case objtoken_type::ID_LINE:
        terminals->push(obj_symbol::NT_LINESTART);
        tokens->pop_front();
        break;
    default:
        print_error("Unexpected token, expected beginning of a statement in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool expand_nt_facestart(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals, std::vector<obj_object>* objects, objmaterial_id mat_id, size_t* start_index) {
    if (objects->size() == 0 ||
        objects->back().polygroups.size() == 0) {
        print_error("Couldn't define face because there is no object/poly group. This is a bug.\n", tokens->front().line);
        return false;
    }
    
    obj_object& object = objects->back();
    object.nverts_face.push_back(0);
    object.start_face.push_back(*start_index);

    obj_token lookahead = tokens->front();
    obj_layout layout = obj_layout::UNDEFINED;
    switch (lookahead.type)
    {
    case objtoken_type::VAL_UINT:
        terminals->push(obj_symbol::T_FACEEND);
        terminals->push(obj_symbol::NT_VERTLISTP);
        terminals->push(obj_symbol::T_VERTDEFP);
        terminals->push(obj_symbol::T_VERTDEFP);
        terminals->push(obj_symbol::T_VERTDEFP);
        layout = obj_layout::POSITIONS;
        break;
    case objtoken_type::VAL_VERTID_PN:
        terminals->push(obj_symbol::T_FACEEND);
        terminals->push(obj_symbol::NT_VERTLISTPN);
        terminals->push(obj_symbol::T_VERTDEFPN);
        terminals->push(obj_symbol::T_VERTDEFPN);
        terminals->push(obj_symbol::T_VERTDEFPN);
        layout = obj_layout::POSITIONS_NORMALS;
        break;
    case objtoken_type::VAL_VERTID_PT:
        terminals->push(obj_symbol::T_FACEEND);
        terminals->push(obj_symbol::NT_VERTLISTPT);
        terminals->push(obj_symbol::T_VERTDEFPT);
        terminals->push(obj_symbol::T_VERTDEFPT);
        terminals->push(obj_symbol::T_VERTDEFPT);
        layout = obj_layout::POSITIONS_TEXCOORDS;
        break;
    case objtoken_type::VAL_VERTID_PTN:
        terminals->push(obj_symbol::T_FACEEND);
        terminals->push(obj_symbol::NT_VERTLISTPTN);
        terminals->push(obj_symbol::T_VERTDEFPTN);
        terminals->push(obj_symbol::T_VERTDEFPTN);
        terminals->push(obj_symbol::T_VERTDEFPTN);
        layout = obj_layout::POSITIONS_TEXCOORDS_NORMALS;
        break;
    default:
        print_error("Unexpected token, expected beginning of a face definition in line %i\n", tokens->front().line);
        return false;
    }

    object.material_ids.push_back(mat_id);
    object.layout = (object.layout == obj_layout::UNDEFINED) ? layout : object.layout;

    if (object.layout == layout) {
        return true;
    }
    else {
        print_error("Polygon group has inconsistent vertex layout in line %i\n", lookahead.line);
        return false;
    }
}

bool expand_nt_linestart(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    switch (lookahead.type)
    {
    case objtoken_type::VAL_UINT:
        terminals->push(obj_symbol::T_LINEEND);
        terminals->push(obj_symbol::NT_VERTLISTP);
        terminals->push(obj_symbol::T_VERTDEFP);
        terminals->push(obj_symbol::T_VERTDEFP);
        break;
    case objtoken_type::VAL_VERTID_PT:
        terminals->push(obj_symbol::T_LINEEND);
        terminals->push(obj_symbol::NT_VERTLISTPT);
        terminals->push(obj_symbol::T_VERTDEFPT);
        terminals->push(obj_symbol::T_VERTDEFPT);
        break;
    case objtoken_type::VAL_VERTID_PN:
        terminals->push(obj_symbol::T_LINEEND);
        terminals->push(obj_symbol::NT_VERTLISTPN);
        terminals->push(obj_symbol::T_VERTDEFPN);
        terminals->push(obj_symbol::T_VERTDEFPN);
        break;
    case objtoken_type::VAL_VERTID_PTN:
        terminals->push(obj_symbol::T_LINEEND);
        terminals->push(obj_symbol::NT_VERTLISTPTN);
        terminals->push(obj_symbol::T_VERTDEFPTN);
        terminals->push(obj_symbol::T_VERTDEFPTN);
    default:
        print_error("Unexpected token, expected beginning of a line definition in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool expand_nt_statementlist(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    if (lookahead.type != objtoken_type::END) {
        terminals->push(NT_STATEMENTLIST);
        terminals->push(NT_STATEMENT);
    }

    return true;
}
           

bool expand_nt_vertlistp(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    if (lookahead.type == objtoken_type::VAL_UINT) {
        terminals->push(NT_VERTLISTP);
        terminals->push(T_VERTDEFP);
    }
    else {
        terminals->push(T_VERTLISTEND);
    }

    return true;
}

bool expand_nt_vertlistpt(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    if (lookahead.type == objtoken_type::VAL_VERTID_PT) {
        terminals->push(NT_VERTLISTPT);
        terminals->push(T_VERTDEFPT);
    }
    else {
        terminals->push(T_VERTLISTEND);
    }

    return true;
}

bool expand_nt_vertlistpn(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    if (lookahead.type == objtoken_type::VAL_VERTID_PN) {
        terminals->push(NT_VERTLISTPN);
        terminals->push(T_VERTDEFPN);
    }
    else {
        terminals->push(T_VERTLISTEND);
    }

    return true;
}

bool expand_nt_vertlistptn(const std::deque<obj_token>* tokens, std::stack<obj_symbol>* terminals) {
    obj_token lookahead = tokens->front();
    if (lookahead.type == objtoken_type::VAL_VERTID_PTN) {
        terminals->push(NT_VERTLISTPTN);
        terminals->push(T_VERTDEFPTN);
    }
    else {
        terminals->push(T_VERTLISTEND);
    }

    return true;
}

bool resolve_posx(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->positions.push_back({});
        verts->positions.back().x = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_posy(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->positions.back().y = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_posz(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->positions.back().z = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_normalx(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->normals.push_back({});
        verts->normals.back().x = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_normaly(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->normals.back().y = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_normalz(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->normals.back().z = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_texcoordu(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->texcoords.push_back({});
        verts->texcoords.back().x = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_texcoordv(std::deque<obj_token>* tokens, obj_vertices* verts) {
    if (tokens->front().type == objtoken_type::VAL_FLOAT) {
        verts->texcoords.back().y = tokens->front().value.flt;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a vertex position x-component in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_mtlname(std::deque<obj_token>* tokens, std::vector<mtl_material>* materials, objmaterial_id* new_mtlid) {
    if (tokens->front().type == objtoken_type::VAL_STR) {
        std::string id = tokens->front().value.str;
        auto iter = std::find_if(materials->begin(), materials->end(), [id](const mtl_material& m) {
            return (m.id == id); });
        
        if (iter != materials->end()) {
            *new_mtlid = materials->end() - iter - 1;
            tokens->pop_front();
            return true;
        }
        else {
            print_error("Selected material in line %i has not been found in a mtl file\n", tokens->front().line);
            return false;
        }
    }
    else {
        print_error("Unexpected token, expected a material name in line %i\n", tokens->front().line);
        return false;
    }
}

bool resolve_mtllib(std::deque<obj_token>* tokens) {
    if (tokens->front().type == objtoken_type::VAL_STR) {
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a matlib name string in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_groupid(std::deque<obj_token>* tokens, std::vector<obj_polygroup>* polygroups) {
    if (tokens->front().type == objtoken_type::VAL_STR) {
        std::string groupname = tokens->front().value.str;

        if (polygroups->back().label != "") {
            auto iter = std::find_if(polygroups->begin(), polygroups->end(), [groupname](const obj_polygroup& p)
                {return p.label == groupname; });
            if (iter == polygroups->end()) {
                polygroups->push_back({});
            }
            else {
                std::iter_swap(iter, polygroups->rbegin());
            }
        }

        polygroups->back().label = tokens->front().value.str;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a group name string in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_objname(std::deque<obj_token>* tokens, std::vector<obj_object>* objects, size_t* start_index) {
    if (tokens->front().type == objtoken_type::VAL_STR) {
        std::string objname = tokens->front().value.str;


        if (objects->back().label != "") {
            auto iter = std::find_if(objects->begin(), objects->end(), [objname](const obj_object& ob)
                {return ob.label == objname; });
            if (iter == objects->end()) {
                objects->push_back({});
            }
            else {
                print_error("Duplicate object definition in line %i\n", tokens->front().line);
                return false;
            }
        }

        size_t nverts = 0;
        for (auto& n : objects->back().nverts_face) {
            nverts += n;
        }
        *start_index = nverts;

        objects->back().label = tokens->front().value.str;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected a group name string in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_vertdefp(std::deque<obj_token>* tokens, obj_object* object, obj_vertices* vertices, objsmoothing_id* smoothing_group) {
    if (object->layout != obj_layout::POSITIONS) {
        print_error("Unexpected token, expected a vertex definition with format \"p\"in line %i\n", tokens->front().line);
        return false;
    }
    
    if (tokens->front().type == objtoken_type::VAL_UINT) {
        if (tokens->front().value.uint < vertices->positions.size() + 1) {
            vertex_id vid = object->position_ids.size();
            object->polygroups.back().face_ids.push_back(vid);

            object->nverts_face.back()++;
            object->position_ids.push_back(tokens->front().value.vertdef.x - 1);
            object->smoothing_groups.push_back(*smoothing_group);
            tokens->pop_front();
        }
        else {
            print_error("Vertex definition references an unknown position in line %i\n", tokens->front().line);
            return false;
        }
    }
    else {
        print_error("Unexpected token, expected a string with vertex definition in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_vertdefpt(std::deque<obj_token>* tokens, obj_object* object, obj_vertices* vertices, objsmoothing_id* smoothing_group) {
    if (object->layout != obj_layout::POSITIONS_TEXCOORDS) {
        print_error("Unexpected token, expected a vertex definition with format \"p/t\"in line %i\n", tokens->front().line);
        return false;
    }
    
    if (tokens->front().type == objtoken_type::VAL_VERTID_PT) {
        if (tokens->front().value.vertdef.x < vertices->positions.size() + 1 &&
            tokens->front().value.vertdef.y < vertices->texcoords.size() + 1) {
            vertex_id vid = object->position_ids.size();
            object->polygroups.back().face_ids.push_back(vid);

            object->nverts_face.back()++;
            object->position_ids.push_back(tokens->front().value.vertdef.x - 1);
            object->texcoord_ids.push_back(tokens->front().value.vertdef.y - 1);
            object->smoothing_groups.push_back(*smoothing_group);
            tokens->pop_front();
        }
        else {
            print_error("Vertex definition references an unknown position or texcoord in line %i\n", tokens->front().line);
            return false;
        }
    }
    else {
        print_error("Unexpected token, expected a string with vertex definition in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_vertdefpn(std::deque<obj_token>* tokens, obj_object* object, obj_vertices* vertices, objsmoothing_id* smoothing_group) {
    if (object->layout != obj_layout::POSITIONS_TEXCOORDS) {
        print_error("Unexpected token, expected a vertex definition with format \"p//n\"in line %i\n", tokens->front().line);
        return false;
    }

    if (tokens->front().type == objtoken_type::VAL_VERTID_PN) {
        if (tokens->front().value.vertdef.x < vertices->positions.size() + 1 &&
            tokens->front().value.vertdef.z < vertices->normals.size() + 1) {
            vertex_id vid = object->position_ids.size();
            object->polygroups.back().face_ids.push_back(vid);

            object->nverts_face.back()++;
            object->position_ids.push_back(tokens->front().value.vertdef.x - 1);
            object->normal_ids.push_back(tokens->front().value.vertdef.z - 1);
            object->smoothing_groups.push_back(*smoothing_group);
            tokens->pop_front();
        }
        else {
            print_error("Vertex definition references an unknown position or normal in line %i\n", tokens->front().line);
            return false;
        }
    }
    else {
        print_error("Unexpected token, expected a string with vertex definition in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_vertdefptn(std::deque<obj_token>* tokens, obj_object* object, obj_vertices* vertices, objsmoothing_id* smoothing_group) {
    if (object->layout != obj_layout::POSITIONS_TEXCOORDS) {
        print_error("Unexpected token, expected a vertex definition with format \"p/t/n\"in line %i\n", tokens->front().line);
        return false;
    }

    if (tokens->front().type == objtoken_type::VAL_VERTID_PTN) {
        if (tokens->front().value.vertdef.x < vertices->positions.size() + 1&&
            tokens->front().value.vertdef.y < vertices->normals.size() + 1&&
            tokens->front().value.vertdef.z < vertices->texcoords.size() + 1) {
            vertex_id vid = object->position_ids.size();
            object->polygroups.back().face_ids.push_back(vid);

            object->nverts_face.back()++;
            object->position_ids.push_back(tokens->front().value.vertdef.x - 1);
            object->texcoord_ids.push_back(tokens->front().value.vertdef.y - 1);
            object->normal_ids.push_back(tokens->front().value.vertdef.z - 1);
            object->smoothing_groups.push_back(*smoothing_group);
            tokens->pop_front();
        }
        else {
            print_error("Vertex definition references an unknown position, texcoord or normal in line %i\n", tokens->front().line);
            return false;
        }
    }
    else {
        print_error("Unexpected token, expected a string with vertex definition in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_smoothing(std::deque<obj_token>* tokens, objsmoothing_id* group) {
    if (tokens->front().type == objtoken_type::VAL_UINT &&
        tokens->front().value.uint <= 32) {
        *group = tokens->front().value.uint;
        tokens->pop_front();
    }
    else if (tokens->front().type == objtoken_type::VAL_STR &&
        tokens->front().value.str == "off") {
        *group = 0;
        tokens->pop_front();
    }
    else {
        print_error("Unexpected token, expected an integral number between 0-32 or a literal 'off' in line %i\n", tokens->front().line);
        return false;
    }

    return true;
}

bool resolve_faceend(size_t* start_index, obj_object* object) {
    if (object->nverts_face.size() > 0) {
        *start_index += object->nverts_face.back();
    }
    else {
        print_error("Failed parsing .obj file, missing face definition. This is a bug\n");
        return false;
    }

    return true;
}

std::vector<obj_object> obj_parse(std::deque<obj_token>* tokens, std::vector<mtl_material>* matlib) {
    
    std::vector<obj_object> objects;
    objects.resize(1);
    objects[0].polygroups.resize(1);
    objmaterial_id current_material = 0;
    objsmoothing_id current_smoothgroup = 0;
    size_t nindices_group = 0;

    std::stack<obj_symbol> terminals;
    terminals.push(obj_symbol::NT_START);

    bool well = true;
    while (!terminals.empty() && well) {
        obj_token lookahead = tokens->front();

        obj_symbol front = terminals.top();
        terminals.pop();
        switch (front) {
        case NT_START:         well = expand_nt_start(tokens, &terminals);                                                  break;
        case NT_STATEMENT:     well = expand_nt_statement(tokens, &terminals);                                              break;
        case NT_FACESTART:     well = expand_nt_facestart(tokens, &terminals, &objects, current_material, &nindices_group); break;
        case NT_LINESTART:     well = expand_nt_linestart(tokens, &terminals);                                              break;
        case NT_STATEMENTLIST: well = expand_nt_statementlist(tokens, &terminals);                                          break;
        case NT_VERTLISTP:     well = expand_nt_vertlistp(tokens, &terminals);                                              break;
        case NT_VERTLISTPT:    well = expand_nt_vertlistpt(tokens, &terminals);                                             break;
        case NT_VERTLISTPN:    well = expand_nt_vertlistpn(tokens, &terminals);                                             break;
        case NT_VERTLISTPTN:   well = expand_nt_vertlistptn(tokens, &terminals);                                            break;
        case T_POSX:        well = resolve_posx(tokens, &objects.back().vertices);                       break;
        case T_POSY:        well = resolve_posy(tokens, &objects.back().vertices);                       break;
        case T_POSZ:        well = resolve_posz(tokens, &objects.back().vertices);                       break;
        case T_NORMALX:     well = resolve_normalx(tokens, &objects.back().vertices);                    break;
        case T_NORMALY:     well = resolve_normaly(tokens, &objects.back().vertices);                    break;
        case T_NORMALZ:     well = resolve_normalz(tokens, &objects.back().vertices);                    break;
        case T_TEXCOORDU:   well = resolve_texcoordu(tokens, &objects.back().vertices);                  break;
        case T_TEXCOORDV:   well = resolve_texcoordv(tokens, &objects.back().vertices);                  break;
        case T_MTLNAME:     well = resolve_mtlname(tokens, matlib, &current_material);                   break;
        case T_MTLLIBNAME:  well = resolve_mtllib(tokens);                                               break;
        case T_GROUPID:     well = resolve_groupid(tokens, &objects.back().polygroups);                  break;
        case T_SMOOTHGROUP: well = resolve_smoothing(tokens, &current_smoothgroup);                      break;
        case T_OBJECTNAME:  well = resolve_objname(tokens, &objects, &nindices_group);                   break;
        case T_VERTDEFP:    well = resolve_vertdefp(tokens, &objects.back(), &objects.back().vertices, &current_smoothgroup);   break;
        case T_VERTDEFPT:   well = resolve_vertdefpt(tokens, &objects.back(), &objects.back().vertices, &current_smoothgroup);  break;
        case T_VERTDEFPN:   well = resolve_vertdefpn(tokens, &objects.back(), &objects.back().vertices, &current_smoothgroup);  break;
        case T_VERTDEFPTN:  well = resolve_vertdefptn(tokens, &objects.back(), &objects.back().vertices, &current_smoothgroup); break;
        case T_FACEEND:     well = resolve_faceend(&nindices_group, &objects.back());
        case T_END:
        case T_LINEEND:
            break;
        }
    }

    if (well) {
        return objects;
    }
    else {
        print_error("Aborted .obj file parsing due to ill-formed file\n");
        return {};
    }
}