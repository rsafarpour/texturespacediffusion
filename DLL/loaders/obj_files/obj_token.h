#pragma once

#include "../../vectortypes.h"

enum class objtoken_type {
    ID_POSITION,
    ID_TEXCOORD,
    ID_FACE,
    ID_NORMAL,
    ID_MTLLIB,
    ID_USEMTL,
    ID_GROUP,
    ID_SMOOTH,
    ID_OBJECTNAME,
    ID_LINE,
    VAL_FLOAT,
    VAL_STR,
    VAL_UINT,
    VAL_VERTID_PT,
    VAL_VERTID_PTN,
    VAL_VERTID_PN,
    END
};

union objtoken_val {
    float       flt;
    char*       str;
    uint3       vertdef;
    unsigned    uint;
};

struct obj_token {
    obj_token(objtoken_type ty, size_t line);
    obj_token(objtoken_type ty, uint3 vertdef, size_t line);
    obj_token(float value, size_t line);
    obj_token(unsigned value, size_t line);
    obj_token(const char* value, size_t line);
    obj_token(const obj_token& other);
    obj_token(obj_token&& other);
    ~obj_token();

    obj_token& operator=(const obj_token& other);
    obj_token& operator=(obj_token&& other);

    objtoken_type type;
    objtoken_val  value;
    size_t        line;
};
