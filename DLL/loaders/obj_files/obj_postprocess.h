#pragma once
#include "obj_output.h"

void                  obj_generatenormals(std::vector<obj_object>* objects);
std::vector<obj_mesh> obj_postprocess(std::vector<obj_object>* objects);
void                  obj_flipv(std::vector<obj_object>* objects);
