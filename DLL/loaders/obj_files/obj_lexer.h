#pragma once

#include "obj_token.h"
#include <queue>
#include <string>

std::deque<obj_token> obj_lex(const std::string* file_name);
