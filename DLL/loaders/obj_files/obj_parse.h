#pragma once

#include "../mtl_files/mtl_output.h"
#include "obj_token.h"
#include "obj_output.h"
#include <queue>

std::vector<std::string> extract_mtllibs(const std::deque<obj_token>* token_stream);
std::vector<obj_object>  obj_parse(std::deque<obj_token>* tokens, std::vector<mtl_material>* matlib);
