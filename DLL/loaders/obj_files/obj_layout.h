#pragma once

constexpr unsigned LAYOUTBIT_POSITION = 1;
constexpr unsigned LAYOUTBIT_NORMAL   = 2;
constexpr unsigned LAYOUTBIT_TEXCOORD = 4;

enum class obj_layout : unsigned {
    UNDEFINED                   = 0x00,
    POSITIONS                   = LAYOUTBIT_POSITION,
    POSITIONS_NORMALS           = LAYOUTBIT_POSITION | LAYOUTBIT_NORMAL,
    POSITIONS_TEXCOORDS         = LAYOUTBIT_POSITION | LAYOUTBIT_TEXCOORD,
    POSITIONS_TEXCOORDS_NORMALS = LAYOUTBIT_POSITION | LAYOUTBIT_TEXCOORD | LAYOUTBIT_NORMAL,
};

inline void add_texcoords(obj_layout* l) {
    *l = (obj_layout)(((unsigned)*l) | LAYOUTBIT_TEXCOORD);
}

inline void add_normals(obj_layout* l) {
    *l = (obj_layout)(((unsigned)*l) | LAYOUTBIT_NORMAL);
}

inline bool has_texcoords(obj_layout l) {
    return (unsigned)l & LAYOUTBIT_TEXCOORD;
}

inline bool has_normals(obj_layout l) {
    return (unsigned)l & LAYOUTBIT_NORMAL;
}

