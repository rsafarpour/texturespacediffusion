#include "obj_postprocess.h"
#include "../../output.h"
#include <numeric>

struct composited_group {
    std::string            label;
    obj_layout             layout;
    std::vector<size_t>    nverts_face;
    std::vector<size_t>    start_face;
    std::vector<vertex_id> vert_ids;
    std::vector<float3>    positions;
    std::vector<float3>    normals;
    std::vector<float2>    texcoords;
};

// If there is no common edge: retval.x == retval.y
// returned vertices are counter clockwise for a
uint2 shared_edge(uint3 a, uint3 b) {
    uint2 retval;
    if (a.x == b.x || a.x == b.y || a.x == b.z) {
        retval.x = a.x;
        if (a.y == b.x || a.y == b.y || a.y == b.z) {
            retval.y = a.y;
            return retval;
        }
        else if (a.z == b.x || a.z == b.y || a.z == b.z) {
            retval.y = a.z;
            return retval;
        }
    }
    else if (a.y == b.x || a.y == b.y || a.y == b.z){
        retval.x = a.y;
        if (a.z == b.x || a.z == b.y || a.z == b.z) {
            retval.y = a.z;
            return retval;
        }
    }

    return {};
}

// Assumes that polygons are flat and vertices are defined in
// CW or CCW order
bool delaunay(uint3 a, uint3 b, const composited_group* group) {
    float3 a_A = group->positions[a.x];
    float3 a_B = group->positions[a.y];
    float3 a_C = group->positions[a.z];

    float3 AB = a_B - a_A;
    float3 AC = a_C - a_A;
    float3 p1 = a_A + scale(0.5, AB);
    float3 p2 = a_A + scale(0.5, AC);
    float3 n = normalised(cross(AB, AC));
    float3 d1 = cross(AB, n);
    float3 d2 = cross(AC, n);

    float num = (p1.y - p2.y) * d2.x + (p2.x - p1.x) * d2.y;
    float den = d2.y*d1.x - d1.y*d2.x;

    float3 center_circum;
    if (den == 0.0) { //Report as Delaunay to prevent infinte loops in caller
        return true;
    }

    float k = num / den;
    center_circum = p1 + scale(k, d1);

    float r_circum = mag(center_circum - a_A);
    r_circum = std::max(r_circum, mag(center_circum - a_B));
    r_circum = std::max(r_circum, mag(center_circum - a_C));

    float3 b_A = group->positions[b.x];
    float3 b_B = group->positions[b.y];
    float3 b_C = group->positions[b.z];

    float dist_bA = mag(center_circum - b_A);
    float dist_bB = mag(center_circum - b_B);
    float dist_bC = mag(center_circum - b_C);

    // There's only one vert which can satisfy this
    // as the other's are shared vertices
    return (
        dist_bA >= r_circum ||
        dist_bB >= r_circum ||
        dist_bC >= r_circum);
}

// Naive implementation; shouldn't be too much of a problem 
// since polygons with more verts than a quad should be rare
void delaunay_flip(std::vector<uint3>* tris, const composited_group* group) {
    
    bool flipped = true;
    while (flipped) {
        flipped = false;
        
        for(auto iter = tris->begin(); iter != tris->end(); ++iter)
            for (auto neighbor_iter = iter + 1; neighbor_iter != tris->end(); ++neighbor_iter) {
                
                uint2 e = shared_edge(*iter, *neighbor_iter);
                if (e.x != e.y && !delaunay(*iter, *neighbor_iter, group)) {
                    unsigned unshared1;
                    unsigned unshared2;

                    if (iter->x != e.x && iter->x != e.y) { unshared1 = iter->x; }
                    else if (iter->y != e.x && iter->y != e.y) { unshared1 = iter->y; }
                    else if (iter->z != e.x && iter->z != e.y) { unshared1 = iter->z; }

                    if (neighbor_iter->x != e.x && neighbor_iter->x != e.y) { unshared2 = neighbor_iter->x; }
                    else if (neighbor_iter->y != e.x && neighbor_iter->y != e.y) { unshared2 = neighbor_iter->y; }
                    else if (neighbor_iter->z != e.x && neighbor_iter->z != e.y) { unshared2 = neighbor_iter->z; }

                    *iter = { unshared2, e.x, unshared1 };
                    *neighbor_iter = { unshared1, e.y, unshared2 };
                    flipped = true;
                }
            }
    }
}

std::vector<std::vector<uint3>> naive_triangulate(const composited_group* group) {
    std::vector<std::vector<uint3>> triangulated_faces;

    size_t n_faces = group->start_face.size();
    for (size_t i = 0; i < n_faces; ++i) {

        size_t start = group->start_face[i];
        size_t end = start + group->nverts_face[i];

        uint3 tri;
        tri.x = group->vert_ids[start + 0];
        tri.y = group->vert_ids[start + 1];
        tri.z = group->vert_ids[start + 2];
        triangulated_faces.push_back({});
        triangulated_faces.back().push_back(tri);

        // Works because points are assumed 
        // to be ordered CW or CCW
        for (size_t j = start + 3; j < end; ++j) {
            tri.y = tri.z;
            tri.z = group->vert_ids[j];

            triangulated_faces.back().push_back(tri);
        }
        
    }

    return triangulated_faces;
}

// This function assumes that all polygons are defined in either CW or
// CCW winding order
std::vector<unsigned> delaunay_triangulate(const composited_group* group) {
    std::vector<unsigned> triangulated;

    std::vector<std::vector<uint3>> triangulations = naive_triangulate(group);
    
    for (std::vector<uint3>& t : triangulations) {
        delaunay_flip(&t, group);
        
        for (uint3 indices : t) {
            triangulated.push_back(indices.x);
            triangulated.push_back(indices.y);
            triangulated.push_back(indices.z);
        }
    }

    return triangulated;
}

composited_group join_pt(const obj_object* object) {

    struct composited_verts {
        std::vector<size_t> position_ids;
        std::vector<size_t> texcoord_ids;
    } vert_ids;

    composited_group composite;
    composite.layout = object->layout;

    vertex_id vert_id = 0;
    for (unsigned k = 0; k < object->start_face.size(); ++k) {
        size_t n_faceverts = object->nverts_face[k];
        size_t start_face = object->start_face[k];
        composite.start_face.push_back(vert_id);
        composite.nverts_face.push_back(object->nverts_face[k]);

        for (unsigned i = 0; i < n_faceverts; i++) {
            auto iter = std::find(vert_ids.position_ids.begin(), vert_ids.position_ids.end(), object->position_ids[start_face + i]);
            size_t j = iter - vert_ids.position_ids.begin();
            if (iter != vert_ids.position_ids.end() &&
                vert_ids.texcoord_ids[j] == object->texcoord_ids[start_face + i]) {
                composite.vert_ids.push_back(j);
            }
            else {
                float3 pos = object->vertices.positions[object->position_ids[start_face + i]];
                float2 tex = object->vertices.texcoords[object->texcoord_ids[start_face + i]];
                composite.vert_ids.push_back(vert_id++);
                composite.positions.push_back(pos);
                composite.texcoords.push_back(tex);
                vert_ids.position_ids.push_back(object->position_ids[start_face + i]);
                vert_ids.texcoord_ids.push_back(object->texcoord_ids[start_face + i]);
            }
        }
    }

    return composite;
}

composited_group join_ptn(const obj_object* object) {

    struct composited_verts {
        std::vector<size_t> position_ids;
        std::vector<size_t> texcoord_ids;
        std::vector<size_t> normal_ids;
    } vert_ids;

    composited_group composite;
    composite.layout = object->layout;

    vertex_id vert_id = 0;
    for (unsigned k = 0; k < object->start_face.size(); ++k) {
        size_t n_faceverts = object->nverts_face[k];
        size_t start_face = object->start_face[k];
        composite.start_face.push_back(vert_id);
        composite.nverts_face.push_back(object->nverts_face[k]);

        for (unsigned i = 0; i < n_faceverts; i++) {
            auto iter = std::find(vert_ids.position_ids.begin(), vert_ids.position_ids.end(), object->position_ids[start_face + i]);
            size_t j = iter - vert_ids.position_ids.begin();
            if (iter != vert_ids.position_ids.end() &&
                vert_ids.texcoord_ids[j] == object->texcoord_ids[start_face + i] &&
                vert_ids.texcoord_ids[j] == object->normal_ids[start_face + i]) {
                composite.vert_ids.push_back(j);
            }
            else {
                float3 pos = object->vertices.positions[object->position_ids[start_face + i]];
                float2 tex = object->vertices.texcoords[object->texcoord_ids[start_face + i]];
                float3 nor = object->vertices.normals[object->normal_ids[start_face + i]];
                composite.vert_ids.push_back(vert_id++);
                composite.positions.push_back(pos);
                composite.texcoords.push_back(tex);
                composite.normals.push_back(nor);
                vert_ids.position_ids.push_back(object->position_ids[start_face + i]);
                vert_ids.texcoord_ids.push_back(object->texcoord_ids[start_face + i]);
                vert_ids.normal_ids.push_back(object->normal_ids[start_face + i]);
            }
        }
    }

    return composite;
}

bool materials_consistent(std::vector<obj_object>* objects) {
    for (auto& object : *objects) {
        if (object.material_ids.size() > 0) {
            objmaterial_id id = object.material_ids.front();
            for (auto& mat_id : object.material_ids) {
                if (mat_id != id) { return false; }
            }
        }
        else {
            return false;
        }
    }

    return true;
}

bool sanity_checks(std::vector<obj_object>* objects) {
    for (obj_object& o : *objects) {
        if (o.material_ids.size() == 0) {
            print_error("Loaded object (labeled: \"%s\") contains no face definitions\n", o.label.c_str());
            return false;
        }


        // Ensure that object groups have one common material
        objmaterial_id objmat = o.material_ids.front();
        auto iter = std::find_if(o.material_ids.begin(), o.material_ids.end(), [objmat](const objmaterial_id id) {
            return (id != objmat);
        });

        if (iter != o.material_ids.end()) { 
            print_error("A loaded object contains multiple material definitions\n");
            return false;
        }

        // Ensure that the individual of a n-gon face are defined 
        // consistently in CW or CCW order and that 
        // the faces are perfectly flat
        size_t n_faces = o.nverts_face.size();
        for (size_t i = 0; i < n_faces; ++i) {
            size_t nverts = o.nverts_face[i];
            if (nverts < 3) {
                return false;
            }
            else {
                size_t start = o.start_face[i];
                float3 v0 = o.vertices.positions[o.position_ids[start + 0]];
                float3 v1 = o.vertices.positions[o.position_ids[start + 1]];
                float3 v2 = o.vertices.positions[o.position_ids[start + 2]];
            
                float3 n = normalised(cross(v1 - v0, v2 - v0));
                for (size_t j = 3; j < nverts; ++j) {
                    v2 = o.vertices.positions[o.position_ids[start + j]];
                    float3 comp = normalised(cross(v1 - v0, v2 - v0));

                    float d = dot(n, comp);
                    if (d < 0) {
                        print_error("A polygon is not defined consistently in CW or CCW order, this may cause artifacts\n");
                    }
                    d = dot(n, v2 - v0);
                    if (d > 0.01) {
                        print_error("A polygon is not flat, this may cause artifacts\n");
                    }
                }
            }
        } 
    }


    return true;
}

// TODO: Other functions
composited_group join_vertices(obj_object* object) {
    switch (object->layout) {
    //case obj_layout::POSITIONS:                   return join_p(&g, object);
    //case obj_layout::POSITIONS_NORMALS:           return join_pn(&g, object);
    case obj_layout::POSITIONS_TEXCOORDS:         return join_pt(object);
    case obj_layout::POSITIONS_TEXCOORDS_NORMALS: return join_ptn(object);
    }

    return {};
}

// TODO: Needs to be done over smoothing groups
// TODO: supply expecetd vertex order
void obj_generatenormals(std::vector<obj_object>* objects) {
    for (auto& o : *objects) {
        std::vector<std::vector<face_id>> vertex_faces(o.vertices.positions.size());

        size_t nfaces = o.start_face.size();
        for (size_t i = 0; i < nfaces; ++i) {
            size_t start_face = o.start_face[i];
            size_t end_face   = start_face + o.nverts_face[i];
            for (size_t j = start_face; j < end_face; ++j) {
                vertex_id vid = o.position_ids[j];
                vertex_faces[vid].push_back(i);
            }
        }

        o.vertices.normals.reserve(o.vertices.positions.size());
        for (auto& facelist : vertex_faces) {
            float3 n_face = {};
            for (face_id fid : facelist) {

                size_t vert_start = o.start_face[fid];

                // Loader guarantees at least 3 vertices!
                float3 pos_b = o.vertices.positions[o.position_ids[vert_start + 0]];
                float3 pos_a = o.vertices.positions[o.position_ids[vert_start + 1]];
                float3 pos_c = o.vertices.positions[o.position_ids[vert_start + 2]];

                // Assuming CW or CCW vertex definition order
                float3 ab = pos_b - pos_a;
                float3 ac = pos_c - pos_a;
                float3 n = normalised(cross(ac, ab));
                n_face += n;
            }

            o.vertices.normals.push_back(normalised(n_face));
        }

        o.normal_ids = o.position_ids;
        add_normals(&o.layout);
    }
}


std::vector<obj_mesh> obj_postprocess(std::vector<obj_object>* objects) {
    std::vector<obj_mesh> meshes;

    // Before processing regroup by material
    if (sanity_checks(objects)) {
        for (auto& o : *objects) {
            composited_group composite = join_vertices(&o);
            std::vector<unsigned> group_indices = delaunay_triangulate(&composite);
            meshes.push_back({});
            meshes.back().positions = std::move(composite.positions);
            meshes.back().normals   = std::move(composite.normals);
            meshes.back().texcoords = std::move(composite.texcoords);
            meshes.back().layout    = composite.layout;
            meshes.back().indices   = group_indices;
        }
    }

    return meshes;
}

void obj_flipv(std::vector<obj_object>* objects) {
    for (auto& object : *objects) {
        for (auto& uv : object.vertices.texcoords) {
            uv.y = 1.0f - uv.y;
        }
    }
}
