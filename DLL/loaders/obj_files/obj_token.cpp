#include "obj_token.h"

obj_token::obj_token(objtoken_type ty, size_t line)
    : value({ 0 })
{
    this->type = ty;
    this->line = line;
}

obj_token::obj_token(objtoken_type ty, uint3 vertdef, size_t line) {
    this->type = ty;
    this->line = line;
    this->value.vertdef = vertdef;
}

obj_token::obj_token(float value, size_t line) {
    this->type = objtoken_type::VAL_FLOAT;
    this->value.flt = value;
    this->line = line;
}

obj_token::obj_token(unsigned value, size_t line) {
    this->type = objtoken_type::VAL_UINT;
    this->value.uint = value;
    this->line = line;
}

obj_token::obj_token(const char* value, size_t line) {
    this->type = objtoken_type::VAL_STR;
    this->line = line;
    this->value.str = (char*)malloc(strlen(value) + 1);
    strcpy(this->value.str, value);
}

obj_token::obj_token(const obj_token& other)
    : value({ 0 })
{
    this->type = other.type;
    this->line = other.line;
    if (this->type == objtoken_type::VAL_STR) {
        this->value.str = (char*)malloc(strlen(other.value.str) + 1);
        strcpy(this->value.str, other.value.str);
    }
    else
    {
        this->value = other.value;
    }
}

obj_token::obj_token(obj_token&& other)
    : value({ 0 })
{
    std::swap(this->type, other.type);
    std::swap(this->value, other.value);
    std::swap(this->line, other.line);

}

obj_token& obj_token::operator=(const obj_token& other) {
    obj_token tmp(other);
    std::swap(*this, tmp);
    return *this;
}
obj_token& obj_token::operator=(obj_token&& other) {
    obj_token tmp(other);
    std::swap(*this, tmp);
    return *this;
}

obj_token::~obj_token() {
    if (this->type == objtoken_type::VAL_STR) {
        free(this->value.str);
    }
}