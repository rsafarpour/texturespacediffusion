#include "filestream.h"

fistream::fistream(const char* file_name) {
    this->handle = fopen(file_name, "rb");
    if (this->handle != nullptr) {
        fpos_t endpos = 0;

        fseek(this->handle, 0, SEEK_END);
        fgetpos(this->handle, &endpos);
        fseek(this->handle, 0, SEEK_SET);

        this->size = endpos;
        this->position = 0;
    }
}

fistream::~fistream() {
    if (this->handle != nullptr) {
        fclose(this->handle);
        this->handle = nullptr;
    }
}

size_t read_chunk(fistream* f, char* out, chunksize_t size) {
    size_t read = fread(out, 1, size, f->handle);
    return read;
}

size_t read_chunk(fistream* f, std::string* out, chunksize_t size) {
    size_t read = fread(&(*out)[0], 1, size, f->handle);
    return read;
}