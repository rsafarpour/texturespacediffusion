
#include "conf.h"
#include "loaders/obj_loading.h"
#include "loaders/transform.h"
#include "rendering/dx11profile.h"
#include "rendering/dx11render.h"
#include "shell/window.h"

#include "rendering/camera.h"
#include "rendering/d3dmaterials.h"
#include "rendering/d3dmeshes.h"
#include "rendering/reflist.h"
#include "rendering/scene.h"
#include "rendering/sspace_method.h"
#include "rendering/tspace_method.h"
#include "stb_image.h"
#include "output.h"

constexpr float PI = 3.141592653f;

enum class RenderMethod {
    TEXTURE_SPACE,
    SCREEN_SPACE
};

#define DLLEXPORT __declspec(dllexport)
extern "C" {
    DLLEXPORT void start(HWND hwnd, unsigned width, unsigned height);
    DLLEXPORT void update();
    DLLEXPORT void stop();
    DLLEXPORT void set_camelevation(float elevation);
    DLLEXPORT void set_camazimuth(float azimuth);
    DLLEXPORT void set_lightelevation(float elevation);
    DLLEXPORT void set_lightazimuth(float azimuth);
    DLLEXPORT void set_lightradiance(float L_r, float L_g, float L_b);
    DLLEXPORT void set_blurscale(float scale);
    DLLEXPORT void set_attnscale(float scale);
    DLLEXPORT void set_bumpscale(float scale);
    DLLEXPORT void set_ambientradiance(float L_r, float L_g, float L_b);
    DLLEXPORT void set_profile(unsigned layer, float r, float g, float b);
    DLLEXPORT void set_ssbias(float bias);
    DLLEXPORT void set_ssscale(float scale);
    DLLEXPORT void set_transmscale(float scale);
    DLLEXPORT void set_rendermethod(RenderMethod m);
    DLLEXPORT float get_avgframetime();
}

constexpr unsigned profile_layers = sizeof(scene_t::profile) / (4*sizeof(float));

vertex_desc vdesc = { 
    {
        DXGI_FORMAT_R32G32B32_FLOAT, 
        DXGI_FORMAT_R32G32B32_FLOAT, 
        DXGI_FORMAT_R32G32_FLOAT
    },
    {
        "POSITION", 
        "NORMAL", 
        "TEXCOORD"} 
};

// Globals
RenderMethod  current_method;
dx11renderer  r;
scene_t       scene;
tspace_method tspace;
sspace_method sspace;

void set_camelevation(float elevation) {
    scene.cam_elevation = elevation;
    place_at(&scene.cam, DISTANCE_CAMERA, scene.cam_elevation, scene.cam_azimuth);
    update_camera(&r, &scene.cam);
}

void set_camazimuth(float azimuth) {
    scene.cam_azimuth = azimuth;
    place_at(&scene.cam, DISTANCE_CAMERA, scene.cam_elevation, scene.cam_azimuth);
    update_camera(&r, &scene.cam);
}

void set_lightelevation(float elevation) {
    scene.light_elevation = elevation;
    set_position(&scene.light, DISTANCE_LIGHT, scene.light_elevation, scene.light_azimuth);
    update_light(&r, &scene.light);
}

void set_lightazimuth(float azimuth) {
    scene.light_azimuth = azimuth;
    set_position(&scene.light, DISTANCE_LIGHT, scene.light_elevation, scene.light_azimuth);
    update_light(&r, &scene.light);
}

void set_lightradiance(float L_r, float L_g, float L_b) {
    scene.pointlight_radiance = { L_r, L_g, L_b };
    set_radiance(&scene.light, scene.pointlight_radiance);
    update_light(&r, &scene.light);
}

void set_blurscale(float scale) {
    tspace.scales.blur_scale = scale;
    upload_bufferdata(&r, tspace.scales_ref, &tspace.scales, sizeof(tspace_method::scales));
}

void set_attnscale(float scale) {
    tspace.scales.attenuation_scale = scale;
    upload_bufferdata(&r, tspace.scales_ref, &tspace.scales, sizeof(tspace_method::scales));
}

void set_bumpscale(float scale) {
    scene.params.bump_height = scale;
    upload_bufferdata(&r, scene.param_buffer, &scene.params, sizeof(sparams_t));
}

void set_ambientradiance(float L_r, float L_g, float L_b) {
    scene.ambient_radiance = { L_r, L_g, L_b };
    upload_bufferdata(&r, scene.ambient_buffer, &scene.ambient_radiance, sizeof(scene.ambient_radiance));
}

void set_profile(unsigned layer, float weight_r, float weight_g, float weight_b) {
    if (layer < profile_layers) {
        scene.profile[4*layer + 0] = weight_r;
        scene.profile[4*layer + 1] = weight_g;
        scene.profile[4*layer + 2] = weight_b;
        upload_bufferdata(&r, scene.profile_buffer, scene.profile, sizeof(scene.profile));
    }
}

void set_ssbias(float bias) {
    sspace.params.bias = bias;
    upload_bufferdata(&r, sspace.params_buffer, &sspace.params, sizeof(ssparams_t));
}

void set_ssscale(float scale) {
    sspace.params.scale = scale;
    upload_bufferdata(&r, sspace.params_buffer, &sspace.params, sizeof(ssparams_t));
}

void set_transmscale(float scale) {
    sspace.params.transmscale = std::max(0.00001f, scale);
    upload_bufferdata(&r, sspace.params_buffer, &sspace.params, sizeof(ssparams_t));
}

void set_rendermethod(RenderMethod method) {
    if (current_method != method) {
        if (method == RenderMethod::TEXTURE_SPACE) {
            destroy_sspacemethod(&r, &sspace);
            create_tspacemethod(&r, r.backbuffer_dims, &scene, &vdesc, &tspace);
        }
        else {
            destroy_tspacemethod(&r, &tspace);
            create_sspacemethod(&r, r.backbuffer_dims, &scene, &vdesc, &sspace);
        }

        current_method = method;
    }
}

float get_avgframetime() {
    if (current_method == RenderMethod::TEXTURE_SPACE) {
        return tspace.avg_frametime;
    }
    else {
        return sspace.avg_frametime;
    }
}


void start(HWND hwnd, unsigned width, unsigned height) {

    r = create_renderer(hwnd, width, height);

    std::string file_name = "./_data/lpshead/head.obj";

    obj_data data;
    data = obj_load(&file_name);
    scale_mesh(&data.meshes[0], 60.0f);
    
    scene.mesh_refs = submit_meshes(&r, &data);
    scene.mparams = gen_materials(&r, &data);
    create_scene(&r, r.backbuffer_dims, &scene);

    current_method = RenderMethod::TEXTURE_SPACE;
    create_tspacemethod(&r, r.backbuffer_dims, &scene, &vdesc, &tspace);
}

void update() {
    if (current_method == RenderMethod::TEXTURE_SPACE) {
        render_tspacemethod(&r, &tspace);
    }
    else {
        render_sspacemethod(&r, &sspace);
    }
}

void stop() {
    if (current_method == RenderMethod::TEXTURE_SPACE) {
        destroy_tspacemethod(&r, &tspace);
    }
    else {
        destroy_sspacemethod(&r, &sspace);
    }
    destroy_scene(&r, &scene);
    destroy_renderer(&r);
}