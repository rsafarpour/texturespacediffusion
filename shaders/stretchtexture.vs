cbuffer transforms : register(b0) {
    float4x4 _view;
    float4x4 _proj;
}

struct vs_in {
    float3 position : POSITION;
    float3 normal   : NORMAL;
    float2 uv       : TEXCOORD;
};

struct vs_out {
    float3 positionVS  : POSITION_VS;
    float2 uv          : TEXCOORD;
    float4 positionNDC : SV_POSITION;
};

vs_out main(vs_in i) {

    vs_out o;
    o.positionVS  = mul(_view, float4(i.position, 1.0));
    o.uv          = i.uv;
    o.positionNDC = float4(2*i.uv.x - 1.0, -(2*i.uv.y - 1.0), 0.0, 1.0);
    return o;
}