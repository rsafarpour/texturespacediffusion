This repo implements texture space diffusion as described in 
GPU Gems 3 chapter 14. Dependencies to build this project are
D3D11 and WPF, so a default Visual Studio installation + Windows
SDK should suffice.

More details at:
https://ramins.dev/?p=1452
