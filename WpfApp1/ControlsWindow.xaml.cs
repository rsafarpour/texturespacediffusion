﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ControlsWindow.xaml
    /// </summary>
    public partial class ControlsWindow : Window
    {
        enum RenderMethod
        {
            TEXTURE_SPACE,
            SCREEN_SPACE
        }

        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_camelevation(float elevation);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_camazimuth(float azimuth);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_lightelevation(float elevation);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_lightazimuth(float azimuth);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_blurscale(float scale);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_attnscale(float scale);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_bumpscale(float scale);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_lightradiance(float L_r, float L_g, float L_b);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_ambientradiance(float L_r, float L_g, float L_b);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_profile(UInt32 layer, float weight_r, float weight_g, float weight_b);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_ssbias(float bias);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_ssscale(float scale);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_transmscale(float scale);
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void set_rendermethod(RenderMethod method);

        public WeightPicker wp;
        public float[] profile_weights;
        public uint profile_current = 0;

        public ControlsWindow()
        {
            profile_weights = new float[18] {
                0.233f, 0.455f, 0.649f,
                0.100f, 0.336f, 0.344f,
                0.118f, 0.198f, 0.000f,
                0.113f, 0.007f, 0.007f,
                0.358f, 0.004f, 0.000f,
                0.078f, 0.000f, 0.000f
            };

            InitializeComponent();
            wp = new WeightPicker(this);
            wp.Topmost = true;

            Loaded += OnFinished;
        }

        public void OnFinished(object sender, EventArgs e)
        {
            this.Rect_DiffusionWeight0.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[0] * 255.0f), (byte)(profile_weights[1] * 255.0f), (byte)(profile_weights[2] * 255.0f)));
            this.Rect_DiffusionWeight1.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[3] * 255.0f), (byte)(profile_weights[4] * 255.0f), (byte)(profile_weights[5] * 255.0f)));
            this.Rect_DiffusionWeight2.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[6] * 255.0f), (byte)(profile_weights[7] * 255.0f), (byte)(profile_weights[8] * 255.0f)));
            this.Rect_DiffusionWeight3.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[9] * 255.0f), (byte)(profile_weights[10] * 255.0f), (byte)(profile_weights[11] * 255.0f)));
            this.Rect_DiffusionWeight4.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[12] * 255.0f), (byte)(profile_weights[13] * 255.0f), (byte)(profile_weights[14] * 255.0f)));
            this.Rect_DiffusionWeight5.Fill = new SolidColorBrush(Color.FromRgb((byte)(profile_weights[15] * 255.0f), (byte)(profile_weights[16] * 255.0f), (byte)(profile_weights[17] * 255.0f)));
            Loaded -= OnFinished;
            this.Slider_AmbientLightRadianceR.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
            this.Slider_AmbientLightRadianceG.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
            this.Slider_AmbientLightRadianceB.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
            this.Slider_PointLightRadianceR.ValueChanged += Slider_PointLightRadiance_ValueChanged;
            this.Slider_PointLightRadianceG.ValueChanged += Slider_PointLightRadiance_ValueChanged;
            this.Slider_PointLightRadianceB.ValueChanged += Slider_PointLightRadiance_ValueChanged;

            this.ComboBox_SelectionChanged(null, null);
        }

        private float DegToRad(float deg)
        {
            return deg/ 180.0f * (float)Math.PI;
        }

        private void Slider_AmbientLightRadiance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.Slider_AmbientLightRadianceR != null &&
                this.Slider_AmbientLightRadianceG != null &&
                this.Slider_AmbientLightRadianceB != null)
            {
                if (this.CheckBox_AmbientRatioLock.IsChecked == true)
                {
                    this.Slider_AmbientLightRadianceR.ValueChanged -= Slider_AmbientLightRadiance_ValueChanged;
                    this.Slider_AmbientLightRadianceG.ValueChanged -= Slider_AmbientLightRadiance_ValueChanged;
                    this.Slider_AmbientLightRadianceB.ValueChanged -= Slider_AmbientLightRadiance_ValueChanged;

                    float change = (float)(e.NewValue - e.OldValue);
                    if (sender != this.Slider_AmbientLightRadianceR) this.Slider_AmbientLightRadianceR.Value += change;
                    if (sender != this.Slider_AmbientLightRadianceG) this.Slider_AmbientLightRadianceG.Value += change;
                    if (sender != this.Slider_AmbientLightRadianceB) this.Slider_AmbientLightRadianceB.Value += change;

                    this.Slider_AmbientLightRadianceR.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
                    this.Slider_AmbientLightRadianceG.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
                    this.Slider_AmbientLightRadianceB.ValueChanged += Slider_AmbientLightRadiance_ValueChanged;
                }

                float r = (float)this.Slider_AmbientLightRadianceR.Value;
                float g = (float)this.Slider_AmbientLightRadianceG.Value;
                float b = (float)this.Slider_AmbientLightRadianceB.Value;
                set_ambientradiance(r, g, b);
            }
        }

        private void Slider_PointLightRadiance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.Slider_PointLightRadianceR != null &&
                this.Slider_PointLightRadianceG != null &&
                this.Slider_PointLightRadianceB != null)
            {
                if (this.CheckBox_PointRatioLock.IsChecked == true)
                {
                    this.Slider_PointLightRadianceR.ValueChanged -= Slider_PointLightRadiance_ValueChanged;
                    this.Slider_PointLightRadianceG.ValueChanged -= Slider_PointLightRadiance_ValueChanged;
                    this.Slider_PointLightRadianceB.ValueChanged -= Slider_PointLightRadiance_ValueChanged;

                    float change = (float)(e.NewValue - e.OldValue);
                    if (sender != this.Slider_PointLightRadianceR) this.Slider_PointLightRadianceR.Value += change;
                    if (sender != this.Slider_PointLightRadianceG) this.Slider_PointLightRadianceG.Value += change;
                    if (sender != this.Slider_PointLightRadianceB) this.Slider_PointLightRadianceB.Value += change;

                    this.Slider_PointLightRadianceR.ValueChanged += Slider_PointLightRadiance_ValueChanged;
                    this.Slider_PointLightRadianceG.ValueChanged += Slider_PointLightRadiance_ValueChanged;
                    this.Slider_PointLightRadianceB.ValueChanged += Slider_PointLightRadiance_ValueChanged;
                }

                float r = (float)this.Slider_PointLightRadianceR.Value;
                float g = (float)this.Slider_PointLightRadianceG.Value;
                float b = (float)this.Slider_PointLightRadianceB.Value;
                set_lightradiance(r, g, b);
            }
        }

        private void Slider_PointLightElevation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_lightelevation(DegToRad((float)e.NewValue));
        }

        private void Slider_PointLightAzimuth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_lightazimuth(DegToRad((float)e.NewValue));
        }

        private void Slider_CameraElevation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_camelevation(DegToRad((float)e.NewValue));
        }

        private void Slider_CameraAzimuth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_camazimuth(DegToRad((float)e.NewValue));
        }

        private void Slider_BumpScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_bumpscale((float)e.NewValue);
        }

        private void Slider_AttenuationScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_attnscale((float)e.NewValue);
        }

        private void Slider_BlurScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_blurscale((float)e.NewValue);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Rect_DiffusionWeight_MouseEnter(object sender, MouseEventArgs e)
        {
            Rectangle obj = sender as Rectangle;
            obj.StrokeThickness = 1;
        }

        private void Rect_DiffusionWeight_MouseLeave(object sender, MouseEventArgs e)
        {
            Rectangle obj = sender as Rectangle;
            obj.StrokeThickness = 0;
        }

        private void Rect_DiffusionWeight_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Rectangle obj = sender as Rectangle;
            switch (obj.Name)
            {
                case "Rect_DiffusionWeight0":
                    this.profile_current = 0;
                    break;
                case "Rect_DiffusionWeight1":
                    this.profile_current = 1;
                    break;
                case "Rect_DiffusionWeight2":
                    this.profile_current = 2;
                    break;
                case "Rect_DiffusionWeight3":
                    this.profile_current = 3;
                    break;
                case "Rect_DiffusionWeight4":
                    this.profile_current = 4;
                    break;
                case "Rect_DiffusionWeight5":
                    this.profile_current = 5;
                    break;
                default:
                    return;
            }

            wp.old_r = profile_weights[3*this.profile_current + 0];
            wp.old_g = profile_weights[3*this.profile_current + 1];
            wp.old_b = profile_weights[3*this.profile_current + 2];
            wp.UpdatePreview();
            wp.Show();
        }

        public void UpdateProfile(float r, float g, float b)
        {
            Rectangle obj;
            switch (this.profile_current)
            {
                case 0: obj = Rect_DiffusionWeight0; break;
                case 1: obj = Rect_DiffusionWeight1; break;
                case 2: obj = Rect_DiffusionWeight2; break;
                case 3: obj = Rect_DiffusionWeight3; break;
                case 4: obj = Rect_DiffusionWeight4; break;
                case 5: obj = Rect_DiffusionWeight5; break;
                default:
                    return;
            }

            this.profile_weights[3 * this.profile_current + 0] = r;
            this.profile_weights[3 * this.profile_current + 1] = g;
            this.profile_weights[3 * this.profile_current + 2] = b;
            obj.Fill = new SolidColorBrush(Color.FromRgb((byte)(r*255.0f), (byte)(g*255.0f), (byte)(b*255.0f)));

            set_profile(this.profile_current, r, g, b);
        }

        private void Slider_SSBlurScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_ssscale((float)e.NewValue);
        }

        private void Slider_SSBlurBias_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_ssbias((float)e.NewValue);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.Grid_TSDControls != null && this.Grid_SSSSSControls != null)
            {
                string value = this.ComboBox_MethodSelector.SelectedValue.ToString();
                if (value.Equals("Texture Space Diffusion"))
                {
                    set_rendermethod(RenderMethod.TEXTURE_SPACE);
                    this.Grid_TSDControls.Visibility = Visibility.Visible;
                    this.Grid_SSSSSControls.Visibility = Visibility.Collapsed;
                }
                else
                {
                    set_rendermethod(RenderMethod.SCREEN_SPACE);
                    this.Grid_TSDControls.Visibility = Visibility.Collapsed;
                    this.Grid_SSSSSControls.Visibility = Visibility.Visible;
                }
            }
        }

        private void Slider_SSTranslucency_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            set_transmscale((float)e.NewValue);
        }
    }
}
