﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WeightPicker.xaml
    /// </summary>
    public partial class WeightPicker : Window
    {
        private ControlsWindow parent;
        public float old_r;
        public float old_g;
        public float old_b;

        public void UpdatePreview()
        {
            Color c = Color.FromRgb(
               (byte)(this.Slider_ProfileWeightR.Value * 255.0f),
               (byte)(this.Slider_ProfileWeightG.Value * 255.0f),
               (byte)(this.Slider_ProfileWeightB.Value * 255.0f));
            SolidColorBrush brush = new SolidColorBrush(c);

            this.Rect_WeightPreview.Fill = brush;
        }

        public WeightPicker(ControlsWindow parent)
        {
            this.parent = parent;

            InitializeComponent();
        }

        private void ProfileWeightsChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            UpdatePreview();
            parent.UpdateProfile(
                (float)this.Slider_ProfileWeightR.Value,
                (float)this.Slider_ProfileWeightG.Value,
                (float)this.Slider_ProfileWeightB.Value);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parent.UpdateProfile(
                (float)this.Slider_ProfileWeightR.Value, 
                (float)this.Slider_ProfileWeightG.Value, 
                (float)this.Slider_ProfileWeightB.Value);

            this.Hide();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.IsVisible) {
                parent.UpdateProfile(old_r, old_g, old_b);
            }
            e.Cancel = true;
            this.Hide();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((sender as Window).IsVisible == true)
            {
                this.Slider_ProfileWeightR.Value = old_r;
                this.Slider_ProfileWeightG.Value = old_g;
                this.Slider_ProfileWeightB.Value = old_b;

                this.UpdatePreview();
            }
        }
    }
}
