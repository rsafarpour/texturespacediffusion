﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ControlsWindow controls;
        
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void start(IntPtr hwnd, UInt32 width, UInt32 height);
        
        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void stop(IntPtr hwnd);

        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern void update();

        [DllImport(@"./shared/TextureSpaceDiffusion.dll")]
        static extern float get_avgframetime();

        public MainWindow()
        {
            Console.WriteLine(Directory.GetCurrentDirectory());
            InitializeComponent();
            controls = new ControlsWindow();
            controls.Show();
        
            IntPtr hwnd = new WindowInteropHelper(this).EnsureHandle();
            start(hwnd, (UInt32)this.Width, (UInt32)this.Height);
            
            CompositionTarget.Rendering += Render;
        }
        
        public void Render(object sender, EventArgs e)
        {
            update();
            float avg_frametime = get_avgframetime();

            controls.Label_AvgFrameTimeValue.Content = avg_frametime.ToString() + "ms";
        }
        
        private void Window_Activated(object sender, EventArgs e)
        {
            controls.Topmost = true;
            controls.Topmost = false;
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IntPtr hwnd = new WindowInteropHelper(this).Handle;
            stop(hwnd);

            Application.Current.Shutdown();
        }
    }
}
